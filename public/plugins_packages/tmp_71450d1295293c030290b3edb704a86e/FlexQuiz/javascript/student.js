
$(document).ready(function() {
    $(document).on("click", '#boolean-option', function(event) {

        var numItems = $('.chosen').length

        if(numItems >= 1 && $('.qt').text() == 'Single-Choice'){
            $( ".chosen" ).each(function( index ) {
                $(this).find('#bool').val('0');
                $(this).find('#boolean-option').prop('checked', false);
                $(this).removeClass('chosen');
              });

        } 
       if($(this).is(':checked')){
            $(this).parent().parent().parent().parent().parent().addClass('chosen');
        } else {
            $(this).parent().parent().parent().parent().parent().removeClass('chosen');
        }
    });

    // Get the modal
    var modal = document.getElementById('myModal');
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");

    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        modalImg.alt = this.alt;
        captionText.innerHTML = this.alt;
    }

    var span = document.getElementsByClassName("close")[0];
    span.onclick = function() {
    modal.style.display = "none";
    }

});

$(document).on("click", '.show-img', function(event) {
    var modal = document.getElementById("myModal");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    modal.style.display = "block";

    var new_img = $(this).find('.pop').attr('src');
    var new_caption = $(this).find('.pop').attr('alt');
    modalImg.src = new_img;
    captionText.innerHTML = new_caption;
});
