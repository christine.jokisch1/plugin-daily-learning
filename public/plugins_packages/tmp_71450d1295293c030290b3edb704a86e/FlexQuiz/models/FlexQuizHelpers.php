
    <?php
    
    class FlexQuizHelpers {

    /**
     * Returns the id of the currently selected seminar or false, if no seminar
     * is selected
     *
     * @return mixed  seminar_id or false
     */
    public static function getSeminarId()
    {
        return Context::getId();
    }

}
?>