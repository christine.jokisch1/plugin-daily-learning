
<form class="default" id="create" autocomplete="off" name="add" method="post" action="<?= PluginEngine::getLink('flexquiz/database/add') ?>" enctype="multipart/form-data">
 <?= CSRFProtection::tokenTag() ?>
      <fieldset class='pt-0'>
          <legend>Pflichtfelder</legend>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                        <label for="title" class="form-label m-2"><b>Name</b></label>
                            <input type="text" name="title" class="form-control m-2 mw-100" placeholder="Name" required>
                    </div>
                    <div class="col">
                        <label for="Fragenkategorie" class="form-label m-2"><b>Fragenkategorie</b></label>
                        <select name='category' class="form-control m-2 mw-100" aria-label="category" id='category' required>
                            <option value='flexible' selected>Flexibel</option>
                            <option value='daily'>Täglich</option>
                        </select>
                    </div>
                </div>
            </div>

        <!-- Kategorie -->
        <div class="mb-3">
            <div class="row">
                <div class="col">
                    <label for="category1" class="form-label m-2"><b>Oberkategorie</b>
                        <input list="categories" name="supercategory" class="form-control m-2 mw-100" required/>
                    </label>
                    <datalist id="categories" required>
                        <?php foreach($categories as $category){ ?>
                            <option value="<?php echo $category['title'] ?>">
                        <?php } ?>
                    </datalist>
                </div>

                <div class="col">
                    <label for="type" class="form-label m-2"><b>Fragen-Typ</b></label>
                    <select class="form-control m-2 mw-100" name='question_type' id="question_type" required>
                        <option>Single-Choice</option>
                        <option>Multiple-Choice</option>
                        <option>Text</option>
                    </select>
                </div>
            </div>
        </div>

        <!-- Daily: Erscheinungsdatum -->
        <div id='erscheinung-daily' style='display:none'>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                    <label for="frage_start_daily" class="form-label m-2"><b>Erscheinungsdatum</b></label>
                        <input type="text" name="release_date_daily" id="question_start_daily" class="form-control m-2 datetimepicker qs mw-100" placeholder="Erscheinung">
                    </div>
                </div>
            </div>
        </div>

      </fieldset>
      <fieldset class="collapsable collapsed pt-0">
          <legend>Optionale Felder - Inhalt</legend>
        <!-- Datum von bis -->
         <!-- Daily: Erscheinungsdatum -->
         <div id='erscheinung-flexible'>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                    <label for="frage_start" class="form-label m-2"><b>Fragen-Erscheinung</b></label>
                        <input type="text" name="release_date" id="question_start" class="form-control m-2 mw-100 datetimepicker qs" placeholder="Erscheinung">
                    </div>
                    <div class="col">
                        <label for="quiz_ende" class="form-label m-2"><b>Beantwortungszeit</b></label>
                        <input type="text" name="response_date" id="question_ende" class="form-control m-2 mw-100 datetimepicker qe" placeholder="Fristende">
                    </div>
                </div>
            </div>
        </div>
        <!-- Kategorie -->
        <div class="mb-3">
            <div class="row">
                <div class="col">
                    <label for="subcategory" class="form-label m-2"><b>Unterkategorie</b></label>
                    <input type="text" name="subcategory" class="form-control m-2 mw-100" placeholder="Unterkategorie">
                </div>
                <div class="col">
                    <label for="difficulty" class="form-label m-2"><b>Schwierigkeitsgrad</b></label>
                    <select class="form-control m-2 mw-100" name='type' id="type">
                        <option>Leicht</option>
                        <option>Mittel</option>
                        <option>Schwer</option>
                    </select>
                </div>
            </div>
            <!-- File Uploads -->
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                        <label for="subcategory" class="form-label m-2"><b>Hinweistext</b>
                            <?= tooltipIcon(_('Nachdem der Studierende erstmals die Frage falsch beantworten hat, kann ein Hinweistext als Hilfestellung angezeigt werden.')) ?>
                            <?= Icon::create('info-circle') ?>
                        </label>
                        <textarea class="form-control mw-100" name="hint"></textarea>
                    </div>
                    <div class="col">
                    <label for="difficulty" class="form-label m-2"><b>Bild zur Fragestellung</b></label>
                        <input type="hidden" name="subcmd" value="upload">
                        <input class="form-control attach" type="file" name="import_file" accept=".png" >
                    </div>

                </div>
            </div>
      </fieldset>
      <fieldset class='pt-0'>
          <legend>Fragestellung</legend> <br>
                <div class="col-12">
                    <div class="mb-3 mt-4">
                        <textarea class="form-control add_toolbar wysiwyg" data-textarea="new_entry" name="question_content" required tabindex="1"></textarea>
                    </div>
                </div>

                <div class='col'>
                    <div class="text-answer" style='display:none'>
                        <div class="mb-3">
                        <label for="text-answer" class="form-label mb-0"><b>Korrekte Antwort</b></label>
                            <div class='row frame'>
                                <div class='col-sm-1 frame-box'>
                                    <input type='hidden' name='text-boolean[]' value='1' id='bool' checked>
                                    <input class="form-check-input input-box" id="boolean" type="checkbox" checked onclick="return false;" value='1'/>
                                </div>
                                <div class='col-sm-11'>
                                    <input type="text" class="form-control mw-100" name="content[]">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="answers">
                        <div class="mb-3">
                            <b>Antwortmöglichkeiten</b>
                            <!-- Single Choice -->
                            <div id='single'>
                                <div class='row frame'>
                                    <div class='col-sm-1 frame-box'>
                                        <input type='hidden' name='boolean[]' value='0' id='bool'>
                                        <input class="form-check-input input-box" id="boolean" type="checkbox" onclick="$(this).parent().find('#bool').attr('value', this.checked ? '1' : '0')">
                                    </div>
                                    <div class='col-sm-4'>
                                        <input type="text" class="form-control" name="content[]" >
                                    </div>
                                    <div class='col-sm-4'>
                                        <input type="hidden" name="subcmd" value="upload">
                                        <input class="form-control attach" type="file" name="answer_file[]" accept=".png,.jpg,.jpeg">
                                    </div>
                                    <div class='col-sm-1'>
                                    </div>
                                </div>
                            </div>

                            <div id='single'>
                                <div class='row frame'>
                                    <div class='col-sm-1 frame-box'>
                                        <input type='hidden' name='boolean[]' value='0' id='bool'>
                                        <input class="form-check-input input-box" id="boolean" type="checkbox" onclick="$(this).parent().find('#bool').attr('value', this.checked ? '1' : '0')">
                                    </div>
                                    <div class='col-sm-4'>
                                        <input type="text" class="form-control" name="content[]" >
                                    </div>
                                    <div class='col-sm-4'>
                                        <input type="hidden" name="subcmd" value="upload">
                                        <input class="form-control attach" type="file" name="answer_file[]" accept=".png,.jpg,.jpeg">
                                    </div>
                                    <div class='col-sm-1'>
                                        <button type='button' class='btn button addbtn' id='addbtn'><span aria-hidden='true'><i class='far fa-plus-square'></i></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="messagebox messagebox_error m-2 error-show hidden">
                            <div class="messagebox_buttons">
                                <a class="close" href="#" title="Nachrichtenbox schließen">
                                    <span>Nachrichtenbox schließen</span>
                                </a>
                            </div>
                            <span class='error-result'> </span>
                        </div>
                    </div>
                </div>
        </fieldset>
      <button type="submit" id='create_question' name="add_question" class="button">Frage erstellen</button>
    </form>

<?= $this->s_cats->render() ?>
<?= $this->s_views->render() ?>

<script>$('.datetimepicker').datetimepicker();</script>

