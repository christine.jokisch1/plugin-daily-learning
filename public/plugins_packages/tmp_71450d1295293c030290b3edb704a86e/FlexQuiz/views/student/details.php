<div class='row justify-content-center'>
    <div class='col-sm-10'>
        <h2 class='mb-3 mt-4'>Quiz: <?php echo $question['title'] ?></h2>
        <div class='flexquiz-quiz-question'>
            <div class="card-title question-head">
                <div class='question-title'>Fragestellung</div>
            </div>
            <div class='flexquiz-quiz-body'>
                <?php if(!empty($question['file'])){ ?>
                    <a href="#" class="pop">
                        <!-- Trigger the Modal -->
                        <img class='question-preview' id="myImg" src="<?php echo $controller->getQuestionLink($question['file'], $cid); ?>" alt="Bild zur Fragestellung: <?php echo $question['title'] ?>" width="250">
                    </a>
                <?php } ?>
                <?php echo $question['content'] ?>
            </div>
        </div>
            <?php foreach($options as $option){ ?>
                <div class='flexquiz-quiz-answers <?php echo ($option['answered_right'] == $option['is_right']) ? 'ans-green' : 'ans-red'; ?>'>
                    <div class='row'>
                        <div class='col-sm-10'>
                            <? if($option['file']){ ?>
                                <div class="img-box-options">
                                    <div class='show-img'>
                                        <a href="#" class="pop" alt="<?php echo $option['option_content'] ?>" src='<?php echo $controller->getOptionLink( $option['file'], $cid); ?>'>
                                            <i class="far fa-file-image"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="img-box-options box-none"></div>
                            <?php } ?>
                            <label style='display:block' class='mb-0'>
                                <input type='hidden' name='answers[]' value='<?php echo $option['answer_id']; ?>'>
                                <input type='hidden' name='boolean[]' value='' id='bool'>
                                <input  <?php echo ($option['answered_right'] == '1') ? 'disabled="disabled" checked' : 'disabled="disabled"'; ?>
                                    class="form-check-input mt-0 option mr-4 mb-0" name='answer_option' id="boolean-option" type="checkbox" />
                                <?php echo $option['option_content'] ?>
                            </label>
                        </div>
                    </div>
                </div>
            <?php } ?>

        <?php if($question['result'] == '0' && !empty($question['hint'])) { ?>
            <div class='flexquiz-quiz-question'>
                <div class='flexquiz-quiz-body'>
                    <b>Hinweis:</b> <?php echo $question['hint']; ?>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>


<!-- Modal -->
<div id="myModal" class="modal">
    <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>

<?= $this->sidebar ?>
