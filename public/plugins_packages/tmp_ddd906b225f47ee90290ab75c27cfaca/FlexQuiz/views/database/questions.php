<div class='row d-flex justify-content-center align-self-stretch'>
  <div class='col-sm-11 m-4'>
    <div class='box-info'>
      <i class="icon icon-info fas fa-question"></i>
      <h3 class='mb-2 p-0'>
          <?php echo $cat['title'] ?>
      </h3>
      <div class='mb-4'>
        <h3>Übersicht der Fragen</h3><br>
          Folgende Funktionen stehen zur Verfügung:<br>
        <ul>
          <li>Erstellte Fragen können aus der Datenbank <b>entfernt</b> werden.<br></li>
          <li>Erstellte Fragen können beliebig <b>editiert</b> werden.</li>
        </ul>

      </div>
    </div>
  </div>

  <div class='col-sm-11 m-4'>
    <table class="default sortable-table">
        <colgroup>
          <col width="600"></col>
          <col width="250"></col>
          <col width="250"></col>
          <col width="250"></col>
          <col width="250"></col>
        </colgroup>
        <thead>
          <tr class="sortable">
            <th data-sort="text">Titel</th>
            <th data-sort="text">Kategorie</th>
            <th data-sort="text">Subkategorie</th>
            <th data-sort="text">Erscheinungsdatum</th>
            <th data-sort="text">Beantwortungsfrist</th>
            <th style="text-align: right">Funktionen</th>
          </tr>
        </thead>
        <?php foreach($questions as $key => $question){?>
          <tr>
            <td>
              <? echo ( ($question['title'] == '' || empty($question['title'])) ? 'Kein Titel vorhanden' : ' '. $question['title'].' ')  ?>
              <?php echo '<span class="badge rounded-pill b_'.$question['category'].'">'. $question['category'] .'</span>' ?>
            </td>
            <td><?php echo $question['type'] ?></td>
            <td>
              <? echo ( ($question['subcategory'] == '' || $question['subcategory'] == ' ' || empty($question['subcategory']) ) ? '-' : ' '. $question['subcategory'].' ')  ?>
            </td>
            <td><?php echo date("d.m.Y, H:i", strtotime($question['release_date'])); ?></td>
            <td><?php echo date("d.m.Y, H:i", strtotime($question['response_date'])); ?></td>
            <td class="actions" style="text-align: right">
            <?php
              $menu = ActionMenu::get();
              $menu->addLink(
                PluginEngine::getURL('flexquiz/database/edit', array('cat' => $question['supercategory_id'], 'qid' => $question['question_id'])),
                  _('Bearbeiten'),
                  Icon::create('edit'),
                  ['href' => '#']
              );
              $menu->addButton(
                  '',
                  ('Löschen'),
                  Icon::create('trash'),
                  [
                      'data-confirm' => _('Wollen Sie wirklich löschen?'),
                      'class' => 'delete-cat',
                  ]
              );
              echo $menu->render();
            ?>
            </td>
          </tr>
        <?php } ?>
        <tfoot></tfoot>
      </table>
    </div>
  </div>
  </div>


<?= $this->s_cats->render() ?>
<?= $this->s_views->render() ?>
