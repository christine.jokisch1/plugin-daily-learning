<?php foreach( $category as $key => $cat) { ?>    
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kategorie editieren</h5>
        <button type="button" class="btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-3">
            <label for="supercategory" class="form-label">Bezeichnung *</label>
            <input type="title" class="form-control" id="supercategory" value='<?php echo $cat['title'] ?>' placeholder="<?php echo $cat['title'] ?>">
        </div>
        <div class="mb-3">
            <label for="icon" class="form-label">Icon *</label>
            <select class="form-control" id="select-icon" name="vehicle">
                <option value="fas fa-atom" data-icon="fas fa-atom">fa-atom</option>
                <option value="fas fa-asterisk" data-icon="fas fa-asterisk">fa-asterisk</option>
                <option value="fas fa-bacterium" data-icon="fas fa-bacterium">fa-bacterium</option>
                <option value="fas fa-bezier-curve" data-icon="fas fa-bezier-curve">fa-bezier-curve</option>
                <option value="fas fa-brain" data-icon="fab fa-brain">fa-brain</option>
                <option value="fab fa-btc" data-icon="fas fa-book">fa-book</option>
                <option value="fab fa-buffer" data-icon="fas fa-car-side">fa-car-side</option>
                <option value="fas fa-chart-pie" data-icon="fas fa-chart-pie">fa-chart-pie</option>
                <option value="fas fa-chart-bar" data-icon="fas fa-chart-bar">fa-chart-bar</option>
                <option value="fab fa-certificate" data-icon="fab fa-certificate">fa-certificate</option>
            </select>
            <br>
            <center>Eine Kategorie kann nachträglich hinsichtlich der Bezeichnung und ihrer visuellen Darstellung (Icon) geändert werden.</center>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
        <button type="button" class="btn btn-primary" id='catedit_send' data-id='<?php echo $catid; ?>'>Änderungen übernehmen</button>
    </div>  
<?php } ?>
