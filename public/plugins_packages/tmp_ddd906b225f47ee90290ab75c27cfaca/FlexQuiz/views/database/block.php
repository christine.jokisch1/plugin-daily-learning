
  <div class='row d-flex justify-content-center align-self-stretch'>
    <?php foreach($categories as $key => $category){?>
      <div class='col-sm-12 col-md-12 col-lg-4 col-xl-3'>
        <div class="card3 cat_<?php echo $category['supercategory_id'] ?>" href="#">
          <h3 id='cat-title-wrapper'><i class="icon <?php echo $category['icon'] ?>"></i> <div class='cat-title'> <?php echo $category['title']; ?> </div></h3>
          <p class="small">
            <center>
              Es stehen aktuell <b><?php echo $category['count']; ?></b> Frage(n) zur Verfügung.<br>
              <span><h5 class='mt-2'><?php echo $category['flexCount']  ?> Flexible | <?php echo $category['dailyCount'] ?> Tägliche </h5></span>
            </center>
          </p>
          <p class="small">
            <center>
              <a href='<?= PluginEngine::getLink('flexquiz/database/questions', array('cat' => $category['supercategory_id'])) ?>'>
                <i class="fas fa-chevron-right"></i> Zu den Fragen
              </a>
            </center>
          </p>
          <div class="dimmer"></div>
            <div class='config'>
              <div class="go-corner" href="#">
                <div class="go-arrow">
                  <i class="fas fa-tools"></i>
              </div>
            </div>
            <div class='config-list'>
              <a data-target="#editCategory" id='cedit' class='config-edit' data-toggle="tooltip" data-id="<?php echo $category['supercategory_id'] ?>" data-placement="right" title="Kategorie editieren"><i class="far fa-edit"></i></a>
              <a class='config-delete' data-toggle="tooltip" data-placement="right" title="Kategorie löschen" href="<?= PluginEngine::getLink('flexquiz/database/', array('action' => 'delete','cat' => $category['supercategory_id'])) ?>"><i class="far fa-trash-alt"></i></a>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
