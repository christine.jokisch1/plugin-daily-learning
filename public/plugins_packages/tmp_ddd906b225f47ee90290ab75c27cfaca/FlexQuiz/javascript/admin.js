
$(document).ready(function() {
    $('#select-icon').select2({
        width:'50%',
        templateSelection: formatText,
        templateResult: formatText
    });
});

$(document).ready(function() {
    $('.stud-list').click(function(e){
        let searchParams = new URLSearchParams(window.location.search);
        let cid = searchParams.get('cid');
        $.ajax({
            type: 'POST',
            url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/list",
            data: {cid: cid, view:'list'},
            cache: false,
            success: function(response) {
                $('#list').html(response);
            }
        });
    });

    $('.stud-block').click(function(e){
        let searchParams = new URLSearchParams(window.location.search);
        let cid = searchParams.get('cid');

        $.ajax({
            type: 'POST',
            url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/block",
            data: {cid: cid, view:'block'},
            cache: false,
            success: function(response) {
                $('#list').html(response);
            }
        });
    });

    $('#remove').click(function(e){
        let id = $(this).data("id");
        let searchParams = new URLSearchParams(window.location.search);
        let cid = searchParams.get('cid');

        $.ajax({
            type: 'GET',
            url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/edit",
            dataType: "html",
            data: {id: id, action:'removeIMG', cid:cid},
            success: function(response) {
               $('#img-view').remove();
            }
        });
    });
});

$(document).ready(function(){
    $('.removeOptionIMG').click(function(e){
        let id = $(this).data("id");
        let searchParams = new URLSearchParams(window.location.search);
        let cid = searchParams.get('cid');
        let current = $(this);
        $.ajax({
            type: 'GET',
            url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/edit",
            dataType: "html",
            data: {id: id, action:'removeOptionIMG', cid:cid},
            success: function(response) {
                current.closest('#img-view').remove();
            }
        });
    });
});

$(document).ready(function(){
    $('[rel="tooltip"]').tooltip({trigger: "hover"});
});

$(document).on("click", '#cedit', function(event) {
    $('#editCategory').modal('show');

    let searchParams = new URLSearchParams(window.location.search);
    let cid = searchParams.get('cid');
    let cat = $(this).data('id');

    $.ajax({
        type: "GET",
        url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/editCat",
        dataType: "html",
        data: {cid: cid, cat:cat},
        success: function(html){
           $('.editInput').html(html)

           $('#select-icon').select2({
            width:'50%',
            templateSelection: formatText,
            templateResult: formatText
        });

        }
   });
});

$(document).on("click", '.delete-cat', function(event) {
    let searchParams = new URLSearchParams(window.location.search);
    let cid = searchParams.get('cid');
    $(this).closest('tr').remove();
    var id = $(this).attr('id');
    $('.fq-sidebar').find('#cat_'+id).remove();
    $.ajax({
        type: 'POST',
        url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/list",
        data: {cid: cid, view:'list', action:'delete', id: id},
        cache: false,
        success: function(response) {

        }
    });
});

$(document).on("click", '.config', function(event) {
    if($(this).find('.config-list').hasClass('visible')){
        $(this).find('.config-list').removeClass('visible')
    } else {
        $(this).find('.config-list').addClass('visible');
    }
    $('[data-toggle="tooltip"]').tooltip();
});

var i = 1;

$(document).on("click", '#addbtn', function(event) {
    $('#single:last-child').after(`
    <div id='single'>
        <div class='row frame'>
            <div class='col-sm-1 frame-box'>
                <input type='hidden' name='boolean[]' value='0' id='bool'>
                <input class="form-check-input input-box" id="boolean" type="checkbox" onclick="$(this).parent().find('#bool').attr('value', this.checked ? '1' : '0')">
            </div>
            <div class='col-sm-4'>
                <input type="text" class="form-control" name="content[]" >
            </div>
            <div class='col-sm-4'>
                <input type="hidden" name="subcmd" value="upload">
                <input class="form-control attach" type="file" name="answer_file[]" accept=".png,.jpg,.jpeg">
            </div>
            <div class='col-sm-1'>
                <button type='button' class='btn button remove'><span aria-hidden='true'><i class='far fa-trash-alt'></i></span></button>
            </div>
        </div>
    </div>
    `);

});

$(document).on("click", '.remove', function(event) {
   $(this).closest("#single").remove();
});

function formatText (icon) {
    return $('<span><i class="fas '+ $(icon.element).data('icon') + ' icon-select"></i> ' + icon.text + '</span>');
};

$(document).on("click", '#catedit_send', function(event) {
    let bezeichnung = $('#supercategory').val();
    let icon = $('#select-icon').val();
    let catid = $(this).data('id');

    $.ajax({
        type: 'POST',
        url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/editcat",
        data: {bezeichnung:bezeichnung, icon:icon, catid:catid, action:'editsend'},
        cache: false,
        success: function(response) {
            $('#editCategory').modal('toggle');
            $('.cat_'+catid).find('.icon').attr('class', 'icon '+icon);
            $('.cat_'+catid).find('.cat-title').html(bezeichnung);
        }
    });
});

window.addEventListener("load", function(){

    var item = $("#flexquiz").clone();
      $(item).insertAfter('.sidebar-widget').show();
  });

  window.addEventListener("load", function(){
    var item = $("#flexquiz-views").clone().css("display", "block");
    $('#layout-sidebar').find('section:last').after(item);

  });

  function RemoveBaseUrl(url) {
    /*
     * Replace base URL in given string, if it exists, and return the result.
     *
     * e.g. "http://localhost:8000/api/v1/blah/" becomes "/api/v1/blah/"
     *      "/api/v1/blah/" stays "/api/v1/blah/"
     */
    var baseUrlPattern = /^https?:\/\/[a-z\:0-9.]+/;
    var result = "";

    var match = baseUrlPattern.exec(url);
    if (match != null) {
        result = match[0];
    }

    if (result.length > 0) {
        url = url.replace(result, "");
    }

    return url;
    }

  $(function(){
    var current = window.location.href;
    $('.fq-sidebar .chapters a').each(function(){
        var catp = $(this);

        var page = window.location.pathname.split("/").pop();  // Wenn es sich z. B. um Edit Page handelt, muss die Kategorie ja auch gehighlighted werden
            if(document.referrer.indexOf(window.location.hostname) != -1 && page == 'edit'){
               var referrer =  document.referrer;
               var ref =  referrer;
               var UrlPattern = RemoveBaseUrl(referrer);
               $('.chapters').find('a[href$="'+UrlPattern+'"]:first').closest('li').addClass('active');
            } else {
                if (this.href === current) {
                    $(this).closest('li').addClass('active');
                }
            }
    })
})
$(document).ready(function() {
    document.getElementById('category').addEventListener('change', function (e) {
        if (e.target.value === "daily") {
            $('#erscheinung-daily').slideDown();
            $('#erscheinung-flexible').slideUp();
            document.getElementById("question_start_daily").required = true;
        } else {
            $('#erscheinung-daily').slideUp();
            $('#erscheinung-flexible').slideDown();
            document.getElementById("question_start_daily").required = false;
        }
    })

    document.getElementById('question_type').addEventListener('change', function (e) {
        if (e.target.value === "Text") {
            $('.answers').slideUp();
            $('.text-answer').slideDown();
        } else if(e.target.value === "Single-Choice" || e.target.value === 'Multiple-Choice') {
            $('.text-answer').slideUp();
            $('.answers').slideDown();


        }
    });

        $("#create_question").closest("form").on('submit', function(event) {

            var type= $('#question_type').val();
            var count = filled = 0;
            var leng =  $('.answers #bool').length;

            if(type !== 'Text'){

                if(type == 'Single-Choice' || type == 'Multiple-Choice'){

                    $.each($('.answers #bool'), function(index, item) {

                        if ($(item).parent().next().find('input[type=text]').val().length > 0) {
                            filled++;
                           }

                        if ($(item).attr('value') == '1') {
                            count++;
                        }

                    });

                } else if(type == 'Multiple-Choice') {
                 /* Hier fortsetzen*/
                }

                if(( (count < 1) || (count >= 2)) && (type === 'Single-Choice')){
                    $('.error-show').show();
                    $('.error-result').html('Es muss genau eine richtige Antwort vorhanden sein.');

                } else if((count < 2) && (type === 'Multiple-Choice')){
                    $('.error-show').show();
                    $('.error-result').html('Es müssen mindestens zwei richtige eeAntwort vorhanden sein.');
                } else {
                    if(leng === filled){
                        $('.error-show').hide();
                        $("#create").submit();
                    } else {
                        $('.error-show').show();
                        $('.error-result').html('Es müssen alle erzeugten Inputfelder ausgefüllt werden.');
                    }

                }

            } else {
                $("#create").submit();
            }

            event.preventDefault();
        });

});
