
<?php

class FlexQuizHistory {

    public static function getQuestion($quiz_id, $user_id){
        $stmt = DBManager::get()->prepare(
            "SELECT * FROM flexquiz_quiz, flexquiz_questions
            WHERE flexquiz_quiz.question_id = flexquiz_questions.question_id
            AND flexquiz_quiz.quiz_id = :quiz_id
            AND flexquiz_quiz.user_id = :user_id
            ");
        $stmt->bindValue(':quiz_id', $quiz_id);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetch();
    }
     public static function getQuizResults($quiz_id){
         $stmt = DBManager::get()->prepare(
             "SELECT *, flexquiz_quiz_results.is_right as answered_right FROM flexquiz_quiz_results, flexquiz_answers
            WHERE flexquiz_quiz_results.answer_id = flexquiz_answers.answer_id
            AND flexquiz_quiz_results.quiz_id = :quiz_id
            ");
         $stmt->bindValue(':quiz_id', $quiz_id);
         $stmt->execute();
         return $stmt;

     }
}
?>
