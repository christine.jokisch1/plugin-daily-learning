<?php

class FlexQuizSupercategories
{
    public static function delete($supercategory_id)
    {
        $stmt = DBManager::get()->prepare("DELETE FROM `flexquiz_supercategories`
                                                WHERE flexquiz_supercategories.supercategory_id = :supercategory_id
                                                ");
        $stmt->bindValue(':supercategory_id', $supercategory_id);
        $stmt->execute();
     return true; 
    }

    public static function getCategoryTitle($supercategory_id)
    {
        $stmt = DBManager::get()->prepare("SELECT title FROM `flexquiz_supercategories`
                                                WHERE flexquiz_supercategories.supercategory_id = :supercategory_id
                                                ");
        $stmt->bindValue(':supercategory_id', $supercategory_id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result['title'];
    }

    public static function getCountQuestionsByCategory($supercategory_id)
    {
        $stmt = DBManager::get()->prepare("SELECT count(*) as count FROM `flexquiz_supercategories`, `flexquiz_questions`
                                                WHERE flexquiz_supercategories.supercategory_id = flexquiz_questions.supercategory_id
                                                AND flexquiz_supercategories.supercategory_id = :supercategory_id
                                                ");
        $stmt->bindValue(':supercategory_id', $supercategory_id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result['count'];
    }

    public static function getSupercategory($supercategory_id){
        $stmt = DBManager::get()->prepare("SELECT * FROM `flexquiz_supercategories` WHERE  supercategory_id = :supercategory_id");
        $stmt->bindValue(':supercategory_id', $supercategory_id);
        $stmt->execute();
        return $stmt;
    } 

    public static function getSupercategorybyCourse($course_id){
        $stmt = DBManager::get()->prepare("SELECT * FROM `flexquiz_supercategories`, `flexquiz_questions`
                                            WHERE flexquiz_supercategories.supercategory_id = flexquiz_questions.supercategory_id
                                            AND flexquiz_questions.seminar_id = :course_id
                                            ");
        $stmt->bindValue(':course_id', $course_id);
        $stmt->execute();
        return $stmt;
    } 

    public static function editCat($data)
    {
        $stmt = DBManager::get()->prepare(
            "UPDATE flexquiz_supercategories
            SET title = ?, icon = ?
            WHERE supercategory_id = ?");
        $stmt->execute([$data['title'], $data['icon'], $data['supercategory_id']]);
        return true; 
    }
}

?>