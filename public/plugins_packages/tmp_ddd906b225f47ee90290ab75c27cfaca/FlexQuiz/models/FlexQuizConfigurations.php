<?php

class FlexQuizConfigurations {

    public static function editconfigcheck($cid)
    {

        $stmt = DBManager::get()->prepare(
            "SELECT * FROM flexquiz_configurations
           WHERE flexquiz_configurations.seminar_id = :seminar_id
           ");
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function insertconfig($cid, $pluginname, $welcome_dozent, $welcome_student){

        $stmt = DBManager::get()->prepare(
            "INSERT INTO flexquiz_configurations (seminar_id, plugin_name, welcome_dozent, welcome_student)
            VALUES (?,?,?,?)");

            $stmt->execute([
                $cid,
                $pluginname,
                $welcome_dozent, 
                $welcome_student
            ]);
            return;
    }

    public static function editconfig($cid, $pluginname, $welcome_dozent, $welcome_student){

        $stmt = DBManager::get()->prepare(
            "UPDATE flexquiz_configurations
                 SET plugin_name = ?, welcome_dozent = ?, welcome_student= ?
                 WHERE seminar_id = ?");

            $stmt->execute([
                $pluginname,
                $welcome_dozent, 
                $welcome_student,
                $cid
            ]);

            return;
    }

    public static function getconfig($cid){
        $stmt = DBManager::get()->prepare(
            "SELECT * FROM flexquiz_configurations
           WHERE flexquiz_configurations.seminar_id = :seminar_id
           ");
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->execute();
        return $stmt; 
    }

    public static function getPluginname($config){
        $pluginname_query = $config->fetch(); 

        if($pluginname_query['plugin_name']){
            return $plugin_name = $pluginname_query['plugin_name']; 
        } else {
            return $plugin_name = 'FlexQuiz'; 
        }
    }
}
?>
