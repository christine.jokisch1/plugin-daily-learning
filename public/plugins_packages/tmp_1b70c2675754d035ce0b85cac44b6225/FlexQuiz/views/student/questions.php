<div class='row' style='width:99%'>
    <div class='col-sm-12 m-4'>
        <div class='box-info'>
        <i class="icon icon-info <?php echo $cat['icon'] ?>"></i>
        <h3 class='mb-2 p-0'>
            <?php echo $cat['title'] ?>
        </h3>
        <div class='mb-4'>
            In der Kategorie <?php echo  $info['title'] ?> existieren insgesamt <b><?php echo $countAll['count'] ?></b> Fragestellungen. <br>
            <ul>
                <li>Fragen mit der Kategorie <i>flexibel</i> sind beliebig wiederholbar. <br></li>
                <li>Tägliche Fragen können nur einmal an einem festgelegten Tag beantwortet werden.</li>
            </ul>
        </div>

        <span>
            <h5>
                <?php echo $info['answered'] ?> Beantwortet | <?php echo ($countAll['count'] - $info['answered']) ?> Unbeantwortet
                <br>
                <?php if( ($getCountTimeRestricted['count'] - $info['answered']) > 0) { ?>
                    <a class="button button-open mt-4" href='<?= PluginEngine::getLink('flexquiz/student/quiz', array('cat' => $cat['supercategory_id']) ) ?>'>
                        Offene Fragen beantworten ( <?php echo $countAll['count'] - $info['answered']; ?> )
                    </a>
                <?php } ?>
            </h5>
        </span>
        </div>
    </div>
    <div class='row' style='width:99%'>
        <div class='col-sm-12 m-4'>
            <h2 class='mb-3'>Aktuelle Fragestellungen</h2>
                <table class="default sortable-table" data-sortlist="[[0]]" id='entries'>
                    <colgroup>
                        <col width="30"></col>
                        <col width="250"></col>
                        <col width="20"></col>
                        <col width="70"></col>
                        <col width="50"></col>
                        <col width="20"></col>
                        <col width="110"></col>
                        <col width="50"></col>
                    </colgroup>
                    <thead>
                    <tr class="sortable">
                        <th data-sort="text">#</th>
                        <th data-sort="text">Titel</th>
                        <th></th>
                        <th data-sort="text">Schwierigkeit</th>
                        <th data-sort="text">Erscheinungsdatum</th>
                        <th data-sort="text">Beantwortungsfrist</th>
                        <th>Restliche Zeit</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                <tbody>
                <?php foreach($questions as $key => $question){?>
                <tr>
                    <td><?php echo $key+1 ?></td>
                    <td>
                        <?php if(  date($question['response_date']) > date("Y-m-d") || $question['category'] == 'flexible') {  ?>
                            <a href='<?= PluginEngine::getLink('flexquiz/student/quiz', array('id' => $question['question_id'], 'cat' => $cat['supercategory_id'] )) ?>'>
                                <? echo ( ($question['title'] == '') ? 'Kein Titel vorhanden' : ' '. $question['title'].' ')  ?>
                            </a>
                        <?php } else { ?>
                            <? echo ( ($question['title'] == '') ? 'Kein Title vorhanden' : ' '. $question['title'].' ')  ?>
                        <?php } ?>

                         <?php echo '<span class="badge rounded-pill b_'.$question['category'].'">'. ($question['category'] == 'flexible' ? 'Flexible': 'Täglich') .'</span>' ?>
                    </td>
                    <td>
                        <?php if($question['file'] || $question['file_check'] == '1'){ ?>
                            <?= tooltipIcon(_('Diese Frage beinhaltet ein Bild.')) ?>
                            <div class="img-box"><i class="far fa-file-image"></i></div>
                        <?php } ?>
                    </td>
                    <td><?php echo $question['type'] ?></td>
                    <td><?php echo date("d.m.Y, H:i", strtotime($question['release_date'])); ?></td>
                    <td><div class='response' id='<?php echo $question['category'] ?>'><?php echo date("d.m.Y, H:i", strtotime($question['response_date'])); ?></div></td>
                    <td><div></div></td>
                    <td>
                        <?php echo ($question['answered'] == 'Beantwortet' ? Icon::create('accept') : Icon::create('decline')); ?>
                        <?php echo $question['answered'] ?>
                    </td>

                    </tr>
                    <?php } ?>
                    <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<?= $this->sidebar ?>

<script>
$(document).ready(function() {
    setInterval(function() {
        $('#entries').find('.response').each(function(){
            var date_unformatted = $(this).html();

            var splitted = date_unformatted.split(',');
            var date = splitted[0];
            var time = splitted[1].split(':');
            var parts = date.split('.');
            var mydate = new Date(parts[2], parts[1]-1, parts[0],time[0],time[1]);

            var now = new Date().getTime();
            var distance = mydate - now;
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                if($(this).attr('id') == 'flexible'){
                    var res = "Unbegrenzt";
                } else {
                    var res =  days + " Tage " + hours + " Std " + minutes + " Min.";

                    if(distance < 0){
                        var res = 'Abgelaufen';
                    }
                }
            $(this).closest('td').next('td').html(res);

        });
        }, 1000);
});


</script>





