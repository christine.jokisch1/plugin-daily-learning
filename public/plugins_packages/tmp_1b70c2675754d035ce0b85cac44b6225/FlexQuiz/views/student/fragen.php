<h3 class='mb-3'>Offene Fragen</h3>
  <table class="default sortable-table">
  <colgroup>
    <col width="30"></col>
    <col width="300"></col>
    <col width="30"></col>
    <col width="100"></col>
    <col width="50"></col>
    <col width="50"></col>
    <col width="50"></col>
    <col width="30"></col>
  </colgroup>
    <thead>
      <tr class="sortable">
        <th data-sort="text">#</th>
        <th data-sort="text">Titel</th>
        <th></th>
        <th data-sort="text">Schwierigkeit</th>
        <th data-sort="text">Überkategorie</th>
        <th data-sort="text">Erscheinungsdatum</th>
        <th data-sort="text">Beantwortungsfrist</th>
        <th data-sort="text">Status</th>
      </tr>
    </thead>
  <tbody>
  <?php foreach($questions as $key => $question){?>
  <tr>
    <td><?php echo $key ?></td>
      <td>
          <a href='<?= PluginEngine::getLink('flexquiz/student/quiz', array('id' => $question['question_id'])) ?>'>
            <? echo ( ($question['title'] == '') ? 'Kein Titel vorhanden' : ' '. $question['title'].' ')  ?>
          </a>
          <?php echo '<span class="badge rounded-pill b_'.$question['category'].'">'. ($question['category'] == 'flexible' ? 'Flexible': 'Täglich') .'</span>' ?>
        </td>
      <td> <?php echo ( ($question['file'] || $question['file_check'] == '1') ? '<div class="img-box"><i class="far fa-file-image"></i></div>' : '') ?> </td>
      <td><?php echo $question['type'] ?></td>
      <td><?php echo $question['stitle'] ?></td>
      <td><?php echo date("d.m.Y, H:i", strtotime($question['release_date'])); ?></td>
      <td><?php echo date("d.m.Y, H:i", strtotime($question['response_date'])); ?></td>
      <td><?php echo $question['answered'] ?></td>
    </tr>
    <?php } ?>
    <tfoot>
  </tfoot>
</table>

<?= $this->sidebar ?>
