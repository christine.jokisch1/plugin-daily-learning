<?php foreach($question as $key => $question){?>
    <div class='row justify-content-center'>
        <div class='col-sm-10'>
        <h2 class='mb-3 mt-4'>Quiz: <?php echo $question['title'] ?></h2>
            <div class='flexquiz-quiz-question'>
                <div class="card-title question-head">
                    <div class='question-title'>Fragestellung</div>

                        <?php if($open_question_next['question_id']){ ?>
                                <a class='button quiz-button-next' href='<?= PluginEngine::getLink('flexquiz/student/quiz', $next_array) ?>'>
                                    Nächste
                                </a>
                        <?php } ?>

                        <?php if($open_question_previous['question_id']){ ?>
                            <a class='button quiz-button-previous' href='<?= PluginEngine::getLink('flexquiz/student/quiz', $previous_array) ?>'>
                                Vorherige
                            </a>
                        <?php } ?>
                    </div>

                <div class='flexquiz-quiz-body'>
                    <div class="block_container">
                        <div class="">
                            <?php if(!empty($question['file'])){ ?>
                                <a href="#" class="pop">
                                    <!-- Trigger the Modal -->
                                    <img class='question-preview' id="myImg" src="<?php echo $controller->getQuestionLink($question['file'], $cid); ?>" alt="Bild zur Fragestellung: <?php echo $question['title'] ?>" width="250">
                                </a>
                            <?php } ?>
                        </div>
                        <div class="block-inner">
                            <?php echo $question['content'] ?>
                        </div>
                    </div>
                    <div class="pl-2 info-question" style='display:inline'>
                        <small><span class="qt"><?php echo $question['question_type'] ?></span> |  <?php echo $question['type'] ?></small>
                    </div>
                </div>
            </div>
        </div>

        <?php if($question['question_type'] == 'Text') { ?>
            <div class='col-sm-10'>
                <form class="default" autocomplete="off" name="send_answer" method="post" action="<?= PluginEngine::getLink('flexquiz/student/quiz', $subarray) ?>">
                <?= CSRFProtection::tokenTag() ?>
                <input type='hidden' name='answer' class="form-control">
                    <?php foreach($options as $key => $option){?>

                        <div class='flexquiz-quiz-answer-text <?php echo $results[0]['color']; ?>'>
                            <div class='row'>
                                <div class='col-sm-1'>
                                    <label style='display:block' class='mb-0'>
                                        <b>
                                            Antwort:
                                        </b>
                                    </label>
                                </div>
                                <div class='col-sm-10'>
                                    <input name='answer-text' class="form-control" value='<?php echo $user_answer; ?>'  <?php echo ($user_answer != '' ? 'readonly' : '')?>>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                        <div class='question-bottom'>
                            <?php if($pageState == '0'){ ?>
                                <button type="submit" name="answer" class="button quiz-button-bottom m-2">Frage beantworten</button>
                            <?php } else {  ?>
                                <a href="<?= PluginEngine::getLink('flexquiz/student/index', array()) ?>" class="button quiz-button-bottom m-2">Zurück zur Übersicht</a>
                            <?php } ?>
                            <!-- Nächste Frage wenn vorhanden -->
                        </div>
                </form>
            </div>
        <?php } else { ?>
            <div class='col-sm-10'>
                <form class="default" autocomplete="off" name="send_answer" method="post" action="<?= PluginEngine::getLink('flexquiz/student/quiz', $subarray) ?>">
                <?= CSRFProtection::tokenTag() ?>

                <div id="hint" style="display:none;">
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='hint-inner'>
                                <i class="far fa-lightbulb font-color-blue"></i> <b>Hinweistext zur Lösung</b><br>
                                <hr>
                                <?php echo $question['hint']; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php foreach($options as $key => $option){?>
                    <div class='flexquiz-quiz-answers <?php echo ($selected[$key] == '1') ? ' chosen ' : ''; ?><?php echo $results[$key]['color']; ?>'>
                        <div class='row'>
                            <div class='col-sm-10'>
                                <? if($option['file']){ ?>
                                    <div class="img-box-options">
                                        <div class='show-img'>
                                            <a href="#" class="pop" alt="<?php echo $option['option_content'] ?>" src='<?php echo $controller->getOptionLink( $option['file'], $cid); ?>'>
                                                <i class="far fa-file-image"></i>
                                            </a>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="img-box-options box-none"></div>
                            <?php } ?>

                                <label style='display:block' class='mb-0'>
                                    <input type='hidden' name='answers[]' value='<?php echo $option['answer_id']; ?>'>
                                    <input type='hidden' name='boolean[]' value='<?php echo ($selected[$key] == '1') ? '1' : '0'; ?>' id='bool'>
                                    <input <?php echo ($pageState == '1') ? 'disabled="disabled"' : ''; ?>
                                    <?php echo ($selected[$key] == '1') ? 'checked' : ''; ?> class="form-check-input mt-0 option mr-4 mb-0" name='answer_option' onclick="$(this).parent().find('#bool').attr('value', this.checked ? '1' : '0')" id="boolean-option" type="checkbox" />
                                    <?php echo $option['option_content'] ?>
                                    <div style='float:right;' class='<?php echo 'icon-'.$results[$key]['color']; ?>'></div>
                                </label>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                    <div class='question-bottom'>
                        <?php if($pageState == '0'){ ?>
                            <button type="submit" name="answer" class="button quiz-button-bottom m-2">Frage beantworten</button>
                        <?php } else {  ?>
                            <a href="<?= PluginEngine::getLink('flexquiz/student/index', array()) ?>" class="button quiz-button-bottom m-2">Zurück zur Übersicht</a>
                        <?php } ?>
                        <!-- Hint -->

                        <?php if($pageState == '1' && !empty($question['hint'])){ ?>
                            <a style="float:right" id="show-hint" class="button quiz-button-bottom m-2">
                                Hinweis anzeigen
                            </a>
                       <?php } ?>

                    </div>
                </form>
            </div>
        </div>
    <?php } ?>
<?php  } ?>

<!-- Modal -->
<div id="myModal" class="modal">
  <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<?= $this->sidebar ?>

<script>
    $(document).on("click", '.show-img', function(event) {
        var modal = document.getElementById("myModal");
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        modal.style.display = "block";

        var new_img = $(this).find('.pop').attr('src');
        var new_caption = $(this).find('.pop').attr('alt');
        modalImg.src = new_img;
        captionText.innerHTML = new_caption;
    });


    $(document).on("click", '#show-hint', function(event) {

        if ($('#hint').is(':visible')){
            $('#hint').slideUp();
            $(this).text('Hinweistext einblenden');
        }else {
            $('#hint').slideDown();
            $(this).text('Hinweistext ausblenden');
        }

    });
</script>


