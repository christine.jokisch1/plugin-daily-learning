<div class='row d-flex justify-content-center align-self-stretch'>
  <div class='col-sm-10'>
    <h3 class='mt-4'>Herzlich Willkommen</h3>
  </div>
  <div class='col-sm-10 m-4'>
    <div class='box-info'>
      <i class="icon icon-info fas fa-question"></i>
      <h3 class='mb-2 p-0'>
          <?php echo $cat['title'] ?>
      </h3>
      <div class='mb-4'>
      <h3>FlexQuiz</h3><br>
        In diesem Lernmodul werden Fragen spezifisch zur Lehrveranstaltung gestellt.<br>
        Die Fragen werden zwischen flexiblen und täglichen Fragen unterschieden.
        <ul>
          <li>Fragen mit der Kategorie <i>flexibel</i> sind beliebig wiederholbar. <br></li>
          <li>Tägliche Fragen können nur einmal an einem festgelegten Tag beantwortet werden.</li>
        </ul>
      </div>

      <? if($dcount == 1){  ?>
        <b> Es steht eine neue tägliche Frage zur Verfügung.</b>  <br>

        <a href='<?= PluginEngine::getLink('flexquiz/student/quiz', array('type' => 'daily') ) ?>' type="submit" name="answer" class="button button-open mt-4">
          Tägliche Frage beantworten
        </a>

      <? }elseif($dcount > 1){  ?>
        <b>Es stehen <b><?= $dcount ?> neue</b> tägliche Fragen zur Verfügung.</b>  <br>
        <a href='<?= PluginEngine::getLink('flexquiz/student/quiz', array('type' => 'daily') ) ?>' type="submit" name="answer" class="button button-open mt-4">
          Tägliche Fragen beantworten
        </a>
        <? } ?>
        <?php if($fcount >= 1) { ?>
        <a href='<?= PluginEngine::getLink('flexquiz/student/quiz', array('type' => 'flexible') ) ?>'
          type="submit" name="answer" class="button button-open mt-4">
          Flexible Fragen beantworten
        </a>
        <? } ?>
    </div>
  </div>

    <?php if(count($categories) == 0){ ?>
        <div class='col-sm-10 m-4'>
            <div class='box-info'>
                <center><b>  Es stehen keine Quizfragen zum aktuellen Zeitpunkt bereit.</b></center>
            </div>
        </div>
    <?php } ?>

  <div class='row d-flex justify-content-center align-self-stretch'>
    <?php foreach($categories as $key => $category){?>
      <div class='col-sm-12 col-md-12 col-lg-5 col-xl-3'>
        <div class="card3 cat_<?php echo $category['supercategory_id'] ?>" href="#">
          <h3 id='cat-title-wrapper'><i class="icon <?php echo $category['icon'] ?>"></i> <div class='cat-title'> <?php echo $category['title']; ?> </div></h3>
          <p class="small">
            <center>
              Es stehen aktuell <b><?php echo $category['count']; ?></b> Frage(n) zur Verfügung.<br>
              <span>
                  <h5 class='mt-2'> Fragen  <?php echo $category['count'] ?>  | <?php echo $category['answered'] ?>   Beantwortet </h5>
            </span>
            </center>
          </p>
          <p class="small">
            <center>
              <a href='<?= PluginEngine::getLink('flexquiz/student/questions', array('cat' => $category['supercategory_id'])) ?>'>
                <i class="fas fa-chevron-right"></i> Zu den Fragen
              </a>
            </center>
          </p>

        </div>
      </div>
    <?php } ?>
  </div>
</div>

<?= $this->sidebar ?>
