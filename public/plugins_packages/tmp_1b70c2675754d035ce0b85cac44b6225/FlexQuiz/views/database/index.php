<div class='row d-flex justify-content-center align-self-stretch'>
    <div class='col-sm-10'>
      <h3 class='mt-4'>Quiz-Verwaltung</h3>
      </div>
      <div class='col-sm-10 m-4'>
        <div class='box-info'>
          <i class="icon icon-info fas fa-question"></i>
          <h3 class='mb-2 p-0'>
              <?php echo $cat['title'] ?>
          </h3>
          <div class='mb-4'>
          <h3>Administration von FlexQuiz</h3><br>
              Mithilfe von FlexQuiz können Sie flexible <b>Wissensfragen</b> erstellen.<br>
              Dabei können sowohl mehrere Fragen innerhalb eines Zeitraumes beantwortet werden oder als tägliche Wissensabfrage fungieren. <br>
            <ul>
              <li>Fragen mit der Kategorie <i>flexibel</i> sind beliebig wiederholbar. <br></li>
              <li>Tägliche Fragen können nur einmal an einem festgelegten Tag beantwortet werden.</li>
            </ul>
          </div>
          <span>
            <h5>
              <a class="button button-open mb-3" href="<?php echo PluginEngine::getURL('flexquiz/database/create'); ?>">Fragen erstellen</a>
            </h5>
          </span>
        </div>
     </div>
      <?php if($catcount  == 0){ ?>
      <div class='col-sm-10 m-4'>
        <div class='box-info'>
             <center><b>  Es wurden noch keine Fragen zum aktuellen Zeitpunkt erstellt.</b></center>
        </div>
      <?php } ?>
     </div>

    <div id="list">
      <div class='row d-flex justify-content-center align-self-stretch'>
        <?php foreach($categories as $key => $category){?>
          <div class='col-sm-12 col-md-12 col-lg-5 col-xl-3'>
            <div class="card3 cat_<?php echo $category['supercategory_id'] ?>" href="#">
              <h3 id='cat-title-wrapper'><i class="icon <?php echo $category['icon'] ?>"></i> <div class='cat-title'> <?php echo $category['title']; ?> </div></h3>
              <p class="small">
                <center>
                  Es stehen aktuell <b><?php echo $category['count']; ?></b> Frage(n) zur Verfügung.<br>
                  <span><h5 class='mt-2'><?php echo $category['flexCount']  ?> Flexible | <?php echo $category['dailyCount'] ?> Tägliche </h5></span>
                </center>
              </p>
              <p class="small">
                <center>
                  <a href='<?= PluginEngine::getLink('flexquiz/database/questions', array('cat' => $category['supercategory_id'])) ?>'>
                    <i class="fas fa-chevron-right"></i> Zu den Fragen
                  </a>
                </center>
              </p>
              <div class="dimmer"></div>
                <div class='config'>
                  <div class="go-corner" href="#">
                    <div class="go-arrow">
                      <i class="fas fa-tools"></i>
                  </div>
                </div>
                <div class='config-list'>
                  <a data-target="#editCategory" id='cedit' class='config-edit' data-toggle="tooltip" data-id="<?php echo $category['supercategory_id'] ?>" data-placement="right" title="Kategorie editieren"><i class="far fa-edit"></i></a>
                  <a class='config-delete' data-toggle="tooltip" data-placement="right" title="Kategorie löschen" href="<?= PluginEngine::getLink('flexquiz/database/', array('action' => 'delete','cat' => $category['supercategory_id'])) ?>"><i class="far fa-trash-alt"></i></a>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="editCategory" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="editInput">
          </div>
        </div>
      </div>
    </div>

<?= $this->s_views->render() ?>
<?= $this->s_cats->render() ?>

