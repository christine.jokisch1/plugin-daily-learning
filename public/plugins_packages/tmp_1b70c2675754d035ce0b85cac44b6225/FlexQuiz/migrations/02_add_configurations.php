<?php

class AddFlexquizConfigurations extends Migration
{

    public function description()
    {
        return 'Add additional table configurations for the flexquiz plugin.';
    }
    
    public function up () {
        DBManager::get()->exec("
            CREATE TABLE IF NOT EXISTS `flexquiz_configurations` (
        `configuration_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `seminar_id` varchar(32) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
        `welcome_dozent` varchar(2000) NOT NULL,
        `welcome_student` varchar(2000) NOT NULL,
        `headline_dozent` varchar(50) NOT NULL,
        `headline_student` varchar(50) NOT NULL,
        `icon_dozent` varchar(200) NOT NULL,
        `icon_student` varchar(200) NOT NULL,
        `plugin_name` varchar(20) NOT NULL,
        `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
        ");
    }

    public function down() {
        DBManager::get()->exec("DROP TABLE IF EXISTS `flexquiz_configurations`");

      }
}

?>