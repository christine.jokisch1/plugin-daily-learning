
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `flexquiz_answers`
--

CREATE TABLE `flexquiz_answers` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_content` varchar(100) NOT NULL,
  `file` varchar(999) NOT NULL,
  `is_right` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `flexquiz_questions`
--

CREATE TABLE `flexquiz_questions` (
  `question_id` int(11) NOT NULL,
  `seminar_id` varchar(32) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `supercategory_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `type` varchar(50) NOT NULL,
  `question_type` varchar(100) NOT NULL,
  `category` varchar(150) NOT NULL,
  `release_date` datetime NOT NULL,
  `response_date` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `subcategory` varchar(200) NOT NULL,
  `content` varchar(999) NOT NULL,
  `hint` varchar(999) DEFAULT NULL,
  `file` varchar(999) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `flexquiz_quiz`
--

CREATE TABLE `flexquiz_quiz` (
  `quiz_id` int(10) NOT NULL,
  `user_id` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `question_id` int(11) NOT NULL,
  `result` varchar(100) NOT NULL,
  `response_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `flexquiz_quiz_results`
--

CREATE TABLE `flexquiz_quiz_results` (
  `result_id` int(11) NOT NULL,
  `quiz_id` int(10) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `is_right` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `flexquiz_supercategories`
--

CREATE TABLE `flexquiz_supercategories` (
  `supercategory_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `icon` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------