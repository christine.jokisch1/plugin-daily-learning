
    <section id='flexquiz' style='display:none'>
        <aside class='fq-sidebar sticky'>
            <ol class='chapters'>
                <p class='chapters-header'> Kategorien </p>
                <?php $i = 0; ?>
                <? if (sizeof((array)$cats)) : ?>
                    <? foreach ($cats as $cat) : ?>
                        <?php $i++; ?>
                        <li class='chapters cat_<?= $cat['supercategory_id']?>' id='cat_<?= $cat['supercategory_id']?>'>
                            <a href='<?= PluginEngine::getURL('flexquiz/database/questions', array('cat' => $cat['supercategory_id'])) ?>' class="navigate">
                                <span id='cat-title-wrapper'>
                                    <div class='cat-title'><?= $cat['title']?>  </div>
                                    <span style='float: right;' class='badge rounded-pill b_flexible'><?= $cat['count']?></span>
                                </span>
                            </a>
                        </li>
                    <? endforeach ?>
                <? endif ?>
                <?php if($i == 0 ){ ?>
                    <li class='chapters'>
                        Keine Kategorien vorhanden.
                    </li>
                <?php } ?>
            </ol>
        </aside>
    </section>

