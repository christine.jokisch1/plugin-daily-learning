<form class="default participantsattendanceplugin-lnw" method="post" action="">
    <?= CSRFProtection::tokenTag() ?>
    <div style="padding:5px;background-color:white">
        <? if (!$print_view): ?>
            <div class="actions" style="margin-bottom:10px;">
                <a>
                    <?= Icon::create('print', 'clickable')->asInput(
                        '20px',
                        [
                            'class' => 'text-bottom',
                            'name' => 'clear_print_view'
                        ]
                    ) ?>
                   <?= _("Druckansicht für handschriftliche Eintragungen") ?>
                </a>
                <a>
                    <?= Icon::create('print', 'clickable')->asInput(
                        '20px',
                        [
                            'class' => 'text-bottom',
                            'name' => 'print_view'
                        ]
                    ) ?>
                    <?= _("Druckansicht") ?>
                </a>
                <a>
                    <?= Icon::create('file-excel', 'clickable')->asInput(
                        '20px',
                        [
                            'class' => 'text-bottom',
                            'name' => 'table_export'
                        ]
                    ) ?>
                    <?=  _("Excel Download") ?>
                </a>
            </div>
        <? endif ?>
        <h2><?=  _("Leistungsnachweisliste für Studienleistungen") ?></h2>
        <div class="course-name">
            <?= htmlReady($seminar->getName()) ?>
            (<?= htmlReady($seminar->seminar_number) ?>)
            -
            <?= htmlReady($seminar_semester['name']) ?>
        </div>
        <br>
        <div>
            <span class="title"><?= _("DozentInnen") ?></span>
            <ul>
                <? foreach ($seminar->getMembers('dozent') as $dozent): ?>
                    <li><?= htmlReady($dozent['fullname']) ?></li>
                <? endforeach ?>
            </ul>
        </div>

        <div class="ects">
            <?= _('ECTS-Kreditpunkte') ?>:
            <?= htmlReady($seminar->ects) ?>
        </div>
        <div class="courses-of-study">
            <span class="title"><?=  _('Studienbereich(e) der Veranstaltung') ?></span>
            <ul>
                <? foreach ($study_areas as $study_area): ?>
                    <li><?= htmlReady($study_area) ?></li>
                <? endforeach ?>
            </ul>
        </div>
        <div class="note">
            <div class="title"><?= _("Hinweis") ?></div>
            <?= _("Diese Liste ist ausschlie&szlig;lich zur Weitergabe an das Pr&uuml;fungsamt bestimmt und darf aus Datenschutzgr&uuml;nden nicht ver&ouml;ffentlicht werden.") ?>
        </div>
        <? if (!$print_view): ?>
            <p class="save-note">
                <?= _("Die hier eingegebenen Noten werden nicht dauerhaft gespeichert! Die Eingabe kann nur zur Erzeugung einer ausgefüllten Liste benutzt werden.") ?>
            </p>
        <? endif ?>

        <table <?= $print_view ? 'class="print-view"' : '' ?>
               cellpadding="2" cellspacing="0">
            <thead>
                <tr>
                    <th class="name"><?= _('Nachname') ?></th>
                    <th class="name"><?= _('Vorname') ?></th>
                    <th class="matrikelnr"><?= _('Matrikelnummer') ?></th>
                    <th class="course-of-study"><?= _('Studiengang') ?></th>
                    <th class="participated"><?= _('Teilnahme bestanden') ?></th>
                    <th class="lnw"><?= _('Leistungsnachweis') ?></th>
                    <th class="degree"><?= _('Note') ?></th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($participants as $participant): ?>
                    <tr>
                        <td class="name"><?= htmlReady($participant->Nachname) ?></td>
                        <td class="name"><?= htmlReady($participant->Vorname) ?></td>
                        <td class="matrikelnr">
                            <?= htmlReady($matrikelnr[$participant->user_id]) ?>
                        </td>
                        <td class="course-of-study">
                            <?= htmlReady($course_of_study[$participant->user_id]) ?>
                        </td>
                        <td class="participated">
                            <? if ($print_view): ?>
                                <? if (!$clear_print_view): ?>
                                    <?= $participated[$participant->user_id]
                                      ? Icon::create('accept', 'info')->asImg('16px')
                                      : '' ?>
                                <? endif ?>
                            <? else: ?>
                                <input type="checkbox" class="studip-checkbox"
                                       name="participated[<?= htmlReady($participant->user_id) ?>]"
                                       id="checkbox_participated_<?= htmlReady($participant->user_id) ?>"
                                       value="1"
                                       <?= $participated[$participant->user_id]
                                         ? 'checked="checked"'
                                         : '' ?>>
                                <label for="checkbox_participated_<?= htmlReady($participant->user_id) ?>"></label>
                            <? endif ?>
                        </td>
                        <td class="lnw">
                            <? if ($print_view): ?>
                                <? if (!$clear_print_view): ?>
                                    <?= htmlReady($lnw[$participant->user_id]) ?>
                                <? endif ?>
                            <? else: ?>
                                <input type="text"
                                       name="lnw[<?= htmlReady($participant->user_id) ?>]"
                                       value="<?= htmlReady($lnw[$participant->user_id]) ?>">
                            <? endif ?>
                        </td>
                        <td class="grade">
                            <? if ($print_view): ?>
                                <? if (!$clear_print_view): ?>
                                    <?= htmlReady($grade[$participant->user_id]) ?>
                                <? endif ?>
                            <? else: ?>
                                <input type="text"
                                       name="grade[<?= htmlReady($participant->user_id) ?>]"
                                       value="<?= htmlReady($grade[$participant->user_id]) ?>">
                            <? endif ?>
                        </td>
                    </tr>
                <? endforeach ?>
            </tbody>
        </table>
    </div>
    <div style="font-size:100%;margin-top: 2em;">
        <?= _("Datum und Unterschrift der/s Lehrenden") ?>
        <hr style="width:350px;height:3px;margin-left:0;margin-top: 4em; text-align:left;"></hr>
    </div>
</form>
