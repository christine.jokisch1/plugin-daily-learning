<?php
class InitPlugin extends Migration
{
	function up()
    {

    Config::get()->create('PARTICIPANTS_ATTENDANCE_PLUGIN_AUTO_ENABLE',
            [   'range' => 'global',
                'type' => 'boolean',
                'value' => false,
                'section' => 'plugins',
                'description' => 'Schaltet das Teilnehmeranwesenheitsplugin automatisch ein, wenn das Teilnehmermodul aktiviert ist'
            ]);

    DBManager::get()->exec("
    CREATE TABLE IF NOT EXISTS `termine_anwesenheit` (
     `termin_id` char(32) COLLATE latin1_bin NOT NULL,
     `user_id` char(32) COLLATE latin1_bin NOT NULL,
     `last_changed_user_id` char(32) COLLATE latin1_bin NOT NULL,
     `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
     PRIMARY KEY (`termin_id`,`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC");

	}

	function down()
    {
        Config::get()->delete('PARTICIPANTS_ATTENDANCE_PLUGIN_AUTO_ENABLE');
        DBManager::get()->exec("DROP TABLE IF EXISTS `termine_anwesenheit`");
    }

}
