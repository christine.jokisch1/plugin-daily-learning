<?php
class AddConfigForLnwAction extends Migration
{
    public function up()
    {
        $c = Config::get();

        $c->create(
            'PARTICIPANTS_ATTENDANCE_PLUGIN_ENABLE_LNW_ACTION',
            [
                'type' => 'boolean',
                'default' => '0',
                'section' => 'plugins'
            ]
        );
    }


    public function down()
    {
        $c = Config::get();
        $c->delete('PARTICIPANTS_ATTENDANCE_PLUGIN_ENABLE_LNW_ACTION');
    }
}
