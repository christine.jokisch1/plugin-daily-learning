<?php
/**
 * ParticipantsAttendancePlugin.class.php
 *
 *
 *
 *
 * @author        André Noack <noack@data-quest.de>, Suchi & Berg GmbH <info@data-quest.de>
 * @version        0.1
 */
// +---------------------------------------------------------------------------+
// This file is part of Stud.IP
// ParticipantsAttendancePlugin
//
// Copyright (C) 2011 André Noack <noack@data-quest.de>
// Suchi & Berg GmbH <info@data-quest.de>
// +---------------------------------------------------------------------------+
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or any later version.
// +---------------------------------------------------------------------------+
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// +---------------------------------------------------------------------------+

require_once "vendor/write_excel/OLEwriter.php";
require_once "vendor/write_excel/BIFFwriter.php";
require_once "vendor/write_excel/Worksheet.php";
require_once "vendor/write_excel/Workbook.php";

class ParticipantsAttendancePlugin extends StudipPlugin implements StandardPlugin, SystemPlugin
{
    public static $datafield_id_matrikel = 'ce73a10d07b3bb13c0132d363549efda';
    public static $dates_per_page = 5;

    /**
     * In case the "Matrikelnummer" datafield existed before the TIC
     * that added that datafield by default, we must search the field's ID
     * in the set of available datafields for users.
     */
    protected function getMatrikelnrFieldId()
    {
        $default_datafield = DataField::exists(self::$datafield_id_matrikel);
        if (!$default_datafield) {
            //The "Matrikelnummer" datafield existed before that field was added
            //as default datafield in Stud.IP. We must look for other datafield
            //entries with the name "Matrikelnummer" that exist for users.
            $other_datafield = DataField::findOneBySql(
                "name = 'Matrikelnummer' AND object_type = 'user'"
            );
            if ($other_datafield) {
                self::$datafield_id_matrikel = $other_datafield->id;
            }
        }
    }


    public function __construct()
    {
        parent::__construct();
        $this->getMatrikelnrFieldId();
        $this->me = get_class($this);
        if (Config::get()->PARTICIPANTS_ATTENDANCE_PLUGIN_AUTO_ENABLE
            && Context::getId()
            && get_object_type(Context::getId(), ['sem']) === 'sem'
        ) {
            $modules = new Modules();
            $enabled = $modules->getLocalModules(Context::getId(), 'sem');
            PluginManager::getInstance()->setPluginActivated(
                $this->getPluginId(),
                Context::getId(),
                $enabled['participants']
            );
        }

        if ($this->isActivated(Context::getId())
            && Navigation::hasItem("/course/members")
            && $GLOBALS['perm']->have_studip_perm('tutor', Context::getId())
        ) {
            Navigation::addItem(
                '/course/members/' . $this->me,
                new Navigation(
                    _("Anwesenheitsliste"),
                    PluginEngine::getUrl($this)
                )
            );
            if (Config::get()->PARTICIPANTS_ATTENDANCE_PLUGIN_ENABLE_LNW_ACTION) {
                Navigation::addItem(
                    '/course/members/' . $this->me . 'lnw',
                    new Navigation(
                        _("Leistungsnachweisliste"),
                        PluginEngine::getUrl($this, [], 'lnw')
                    )
                );
            }
        }

    }


    public function getIconNavigation($course_id, $last_visit, $user_id)
    {
    }


    public function getTabNavigation($course_id)
    {
    }


    public function getNotificationObjects($course_id, $since, $user_id)
    {
    }


    public function getInfoTemplate($course_id)
    {
    }


    public function getPluginName()
    {
        return _("Anwesenheitsliste");
    }


    protected function getSelectedSemester(Seminar $seminar)
    {
        $semester_id = Request::get('semester_id');
        $selected_semester = null;
        if ($semester_id) {
            $selected_semester = Semester::find($semester_id);
        } else {
            //Check if the current semester is in the range of the
            //course's semesters. If so, use the current semester.
            $current_semester = Semester::findCurrent();
            if ($current_semester['beginn'] >= $seminar->getStartSemester()) {
                //The current semester is newer than the start semester
                //or it is the start semester.
                $seminar_end_semester = $seminar->getEndSemester();
                if ($seminar_end_semester > 0) {
                    //Check if the current semester is newer than the
                    //end semester of the course:
                    if ($current_semester['beginn'] > $seminar_end_semester) {
                        //We must use the end semester of the course:
                        $selected_semester = Semester::findByTimestamp(
                            $seminar_end_semester
                        );
                    } else {
                        //The current semester is not newer than the end semester
                        //which means we can use it:
                        $selected_semester = $current_semester;
                    }
                } elseif ($seminar_end_semester == 0) {
                    //The course's end semester is the course's start semester.
                    //We can only use that semester.
                    $selected_semester = Semester::findByTimestamp(
                        $seminar->getStartSemester()
                    );
                } else {
                    //The Course's end semester is not specified.
                    //Use the current semester:
                    $selected_semester = $current_semester;
                }
            } else {
                //The current semester is older than the start semester.
                //We must use the start semester:
                $selected_semester = Semester::findByTimestamp(
                    $seminar->getStartSemester()
                );
            }
        }
        return $selected_semester;
    }


    public function show_action()
    {
        if (!$GLOBALS['perm']->have_studip_perm('tutor', Context::getId())) {
            throw new AccessDeniedException();
        }

        Navigation::activateItem('/course/members/' . $this->me);
        PageLayout::setHelpKeyword("Basis.VeranstaltungenVerwaltenTeilnehmer");
        PageLayout::setTitle(Course::findCurrent()->getFullname() . " - " . _("Anwesenheitsliste"));
        ob_start();
        $db = DbManager::get();
        self::$datafield_id_matrikel = $db->fetchColumn(
            "SELECT datafield_id FROM datafields WHERE object_type='user' AND view_perms =? AND name LIKE 'Matrikelnummer'",
            [
                $GLOBALS['perm']->have_studip_perm('dozent', Context::getId())
                ? 'dozent'
                : 'tutor'
            ]
        );
        $header = '';
        $table = [];
        $termine = [];
        $termine_anwesenheit = [];
        $seminar = Seminar::GetInstance(Context::getId());

        $selected_semester = $this->getSelectedSemester($seminar);

        if (Request::submitted('set_time_filter')) {
            $time_filter_start = strtotime(Request::get('set_time_filter_start'));
            $time_filter_end = strtotime(Request::get('set_time_filter_end'));
        } else {
            $time_filter_start = Request::get('time_filter_start') ?: $selected_semester->beginn;
            $time_filter_end = Request::get('time_filter_end') ?: $selected_semester->ende;
        }
        if ($time_filter_start < $selected_semester->beginn || $time_filter_start > $selected_semester->ende) {
            $time_filter_start = $selected_semester->beginn;
        }
        if ($time_filter_end > $selected_semester->ende || $time_filter_end < $time_filter_start) {
            $time_filter_end = $selected_semester->ende;
        }
        $time_filter_end = strtotime('23:59:59', $time_filter_end);
        URLHelper::addLinkParam('semester_id', $selected_semester['semester_id']);
        URLHelper::addLinkParam('time_filter_start', $time_filter_start);
        URLHelper::addLinkParam('time_filter_end', $time_filter_end);

        if (Request::get('print') && Request::get('page')) {
            $start = (Request::int('page') - 1) * self::$dates_per_page;
            $limit = "LIMIT $start," . self::$dates_per_page;
        }
        $termine = $db->fetchAll(
            "SELECT termin_id, date, end_time FROM termine
            WHERE range_id=? AND date_typ IN (?) AND date BETWEEN ? AND ?
            ORDER BY date $limit",
            [
                $seminar->getId(),
                getPresenceTypes(),
                (int)$time_filter_start,
                (int)$time_filter_end
            ]
        );
        if (count($termine)) {
            $db->fetchAll(
                "SELECT * FROM termine_anwesenheit WHERE termin_id IN(?)",
                [
                    array_map(
                        function ($t) {
                            return $t["termin_id"];
                        },
                        $termine
                    )
                ],
                function ($r) use (&$termine_anwesenheit) {
                    $termine_anwesenheit[$r['termin_id']][$r['user_id']] = true;
                });
        }
        $dozenten = join(', ', array_map(function ($a) {
            return $a["Nachname"];
        }, $seminar->getMembers('dozent')));

        $header = sprintf(
            _('Anwesenheitsliste: %1$s - %2$s (%3$s)'),
            $seminar->getName(),
            $selected_semester['name'],
            $dozenten
        );
        $r = 0;
        $table[$r][0] = '';
        if (self::$datafield_id_matrikel) {
            $table[$r][1] = 'Matrikelnr';
        }
        $col_responsive = 2;
        foreach ($termine as $i => $termin) {
            $table[$r][] = strftime('%a %x', $termin['date']);
            if (strftime('%x', $termin['date']) == strftime('%x', time())) {
                $col_responsive = $i + 2;
            }
        }
        $col_responsive = Request::int('show_col', $col_responsive);
        $participants = SimpleCollection::createFromArray(
            $seminar->getMembersWithStatus('autor')
        )->orderBy('nachname,vorname');
        foreach ($participants as $autor) {
            ++$r;
            $table[$r][0] = $autor['Nachname'] . ', ' . $autor['Vorname'];
            if (self::$datafield_id_matrikel) {
                $table[$r][1] = self::getDatafieldValue(
                    self::$datafield_id_matrikel,
                    $autor['user_id']
                );
            }
            if (count($termine)) {
                foreach ($termine as $termin) {
                    $table[$r][] = [
                        $termin['termin_id'] . '-' . $autor['user_id']
                        => $termine_anwesenheit[$termin['termin_id']][$autor['user_id']]
                    ];
                }
            }
        }

        if (Request::submitted('send_xls')) {
            while (ob_get_level()) {
                ob_end_clean();
            }
            $tmpfile = basename($this->create_result_xls($header, $table));
            if ($tmpfile) {
                header(
                    'Location: '
                    . FileManager::getDownloadURLForTemporaryFile(
                        $tmpfile,
                        dgettext(
                            'ParticipantsAttendancePlugin',
                            'Anwesenheitsliste'
                        )
                        . '.xls'
                    )
                );
                page_close();
                die;
            }
        }

        if (Request::isXhr()) {
            while (ob_get_level()) {
                ob_end_clean();
            }
            if (isset($termine_anwesenheit[Request::option('termin_id')][Request::option('user_id')])) {
                $ok = $db->execute(
                    "DELETE FROM termine_anwesenheit WHERE termin_id=? AND user_id=? LIMIT 1",
                    [
                        Request::option('termin_id'),
                        Request::option('user_id')
                    ]
                );
                echo $ok != 1;
            } else {
                $ok = $db->execute(
                    "INSERT IGNORE INTO termine_anwesenheit SET termin_id=?, user_id=?, last_changed_user_id=? ",
                    [
                        Request::option('termin_id'),
                        Request::option('user_id'),
                        $GLOBALS['user']->id
                    ]
                );
                echo $ok;
            }
            page_close();
            die();
        }

        $actions = new ActionsWidget();
        $actions->addLink(
            _('Download als Excel Datei'),
            PluginEngine::getLink(
                $this,
                [
                    'send_xls' => '1',
                    'semester_id' => $selected_semester->id
                ]
            ),
            Icon::create('file-excel', 'clickable')
        );

        $actions->addLink(
            _('Druckansicht'),
            PluginEngine::getLink(
                $this,
                [
                    'print' => 1,
                    'semester_id' => $selected_semester->id
                ]
            ),
            Icon::create('print', 'clickable'),
            [
                'class' => 'print_action',
                'target' => '_blank'
            ]
        );
        if (count($termine) > self::$dates_per_page) {
            foreach (range(1, ceil(count($termine) / self::$dates_per_page)) as $p) {
                $actions->addLink(
                    '[Seite ' . $p . ']',
                    PluginEngine::getLink(
                        $this,
                        [
                            'print' => 1,
                            'page' => $p
                        ]
                    ),
                    null,
                    [
                        'class' => 'print_action',
                        'target' => '_blank'
                    ]
                );
            }
        }
        $actions->addLink(
            _('Druckansicht für handschriftliche Eintragungen'),
            PluginEngine::getLink(
                $this,
                [
                    'print' => 1,
                    'print_clear' => 1,
                    'semester_id' => $selected_semester->id
                ]
            ),
            Icon::create('print', 'clickable'),
            [
                'class' => 'print_action',
                'target' => '_blank'
            ]
        );
        if (count($termine) > self::$dates_per_page) {
            foreach (range(1, ceil(count($termine) / self::$dates_per_page)) as $p) {
                $actions->addLink(
                    '[Seite ' . $p . ']',
                    PluginEngine::getLink(
                        $this,
                        [
                            'print' => 1,
                            'print_clear' => 1,
                            'page' => $p,
                            'semester_id' => $selected_semester->id
                        ]
                    ),
                    null,
                    [
                        'class' => 'print_action',
                        'target' => '_blank'
                    ]
                );
            }
        }

        //Build the semester selector widget:

        //Assume a course without end semester by default:
        $seminar_search_end_timestamp = PHP_INT_MAX;

        if ($seminar->getEndSemester() == 0) {
            //The start semester is the only semester.
            $seminar_search_end_timestamp = $seminar->getStartSemester();
        } elseif ($seminar->getEndSemester() > 0) {
            //The course has an end semester different than the start semester.
            $seminar_search_end_timestamp = $seminar->getEndSemester();
        }
        $selectable_semesters = Semester::findBySql(
            'beginn >= :begin AND beginn <= :end
            ORDER BY beginn DESC',
            [
                'begin' => $seminar->getStartSemester(),
                'end' => $seminar_search_end_timestamp
            ]
        );

        $semester_widget = new SelectWidget(
            _('Semesterfilter'),
            PluginEngine::getURL($this, [], 'show'),
            'semester_id'
        );

        if ($selectable_semesters) {
            foreach ($selectable_semesters as $selectable_semester) {
                $semester_widget->setOptions(
                    [
                        $selectable_semester->id => $selectable_semester->name
                    ],
                    $selected_semester['id']
                );
            }
        }

        $info_widget = new SidebarWidget();
        $info_widget->setTitle(_('Info'));
        $info_txt = sprintf(_("Die Anwesenheitsliste zeigt alle Sitzungstermine (%s) des Ablaufplans.")
            , htmlReady(join(', '
                , array_filter(array_map(function ($a) {
                    return $a["sitzung"] ? $a["name"] : false;
                }, $GLOBALS['TERMIN_TYP'])))));
        $info_widget->addElement(new WidgetElement($info_txt));
        $sidebar = Sidebar::get();
        $sidebar->addWidget($info_widget);
        $sidebar->addWidget($actions);
        $sidebar->addWidget($semester_widget);

        echo '<div>';


        if (!$_GET['print']) {
            echo '<div class="responsive-hidden" style="margin-bottom:10px;">';
            echo '<form class="default" action="' . URLHelper::getLink() . '" method="post">';
            echo '<section class="hgroup size-s">';
            echo '<input type="text" data-date-picker class="hasDatePicker" id="time_filter_start" name="set_time_filter_start" value="'.strftime('%x', $time_filter_start).'">';
            echo _('bis');
            echo '<input type="text" data-date-picker="'. jsReady('{">":"#time_filter_start"}', 'inline-single'). '" class="hasDatePicker" name="set_time_filter_end" value="'.strftime('%x', $time_filter_end).'">';
            echo Studip\Button::create(_('Zeitfilter'), 'set_time_filter', ['style' => 'margin: 0']);
            echo '</section>';
            echo '</form>';
            echo '</div>';
        } else {
            echo '<div style="font-size:150%;font-face:bold;margin-bottom:10px;">' . htmlReady($header) . '</div>';
        }
        echo '<table class="none responsive-hidden" border="0" style="font-size:70%;border-collapse:collapse;" cellpadding="2" cellspacing="0" width="100%">';

        foreach ($table as $r => $row) {
            echo '<tr>';
            if ($r == 0) {
                foreach ($row as $col) {
                    echo chr(10) . '<td style="border: 1px solid black;white-space:nowrap;text-align:center;">' . htmlReady($col) . '</td>';
                }
            } else {
                foreach ($row as $c => $col) {
                    if ($c == 0 || ($c == 1 && self::$datafield_id_matrikel)) {
                        echo chr(10) . '<td style="border: 1px solid black;white-space:nowrap;">';
                        echo htmlReady($col);
                    } else {
                        $key = key($col);
                        $value = current($col);
                        echo chr(10) . '<td style="border: 1px solid black;text-align:center;">';
                        if (!$_GET['print']) {
                            echo Icon::create(
                                $value ? 'checkbox-checked' : 'checkbox-unchecked',
                                'clickable'
                            )->asImg(
                                '16px',
                                [
                                    'class' => 'anwesenheit',
                                    'style' => 'cursor: pointer;',
                                    'id' => $key
                                ]
                            );
                        } else if (!$_GET['print_clear']) {
                            echo Icon::create(
                                $value ? 'checkbox-checked' : 'checkbox-unchecked',
                                'info'
                            )->asImg(
                                '16px'
                            );
                        } else {
                            echo '&nbsp;';
                        }
                    }
                    echo '</td>';
                }
            }
            echo '</tr>';
        }

        echo '</table>';
        if (!Request::get('print')) {
            echo '<table class="none responsive-visible" border="0" style="border-collapse:collapse;" cellpadding="2" cellspacing="0" width="100%">';
            echo '<tr>';
            echo '<td style="text-align: left">';
            if ($col_responsive >= (1 + (int)isset(self::$datafield_id_matrikel))) {
                echo '<a href="' . URLHelper::getLink('', ['show_col' => $col_responsive - 1]) . '">';
                echo Icon::create('arr_1left', Icon::DEFAULT_ROLE)->asImg('32px');
                echo '</a>';
            }
            echo '</td>';
            if (self::$datafield_id_matrikel) echo '<td></td>';
            echo '<td style="text-align: right">';
            if ($col_responsive < count($table[0]) - 1) {
                echo '<a href="' . URLHelper::getLink('', ['show_col' => $col_responsive + 1]) . '">';
                echo Icon::create('arr_1right', Icon::DEFAULT_ROLE)->asImg('32px');
                echo '</a>';
            }
            echo '</td>';
            echo '</tr>';
            foreach ($table as $r => $row) {
                echo '<tr>';
                if ($r == 0) {
                    foreach ($row as $c => $col) {
                        if ($c == 0 || ($c == 1 && self::$datafield_id_matrikel) || ($c == $col_responsive)) {

                            echo chr(10) . '<td style="border: 1px solid black;white-space:nowrap;text-align:center;">' . htmlReady($col) . '</td>';
                        }
                    }
                } else {
                    foreach ($row as $c => $col) {
                        if ($c == 0 || ($c == 1 && self::$datafield_id_matrikel)) {
                            echo chr(10) . '<td style="border: 1px solid black;white-space:nowrap;">';
                            echo htmlReady($col);
                        } else {
                            if ($c != $col_responsive) continue;
                            $key = key($col);
                            $value = current($col);
                            echo chr(10) . '<td style="border: 1px solid black;text-align:center;">';
                            echo Icon::create(
                                $value ? 'checkbox-checked' : 'checkbox-unchecked',
                                'clickable'
                            )->asImg(
                                '24px',
                                [
                                    'class' => 'anwesenheit',
                                    'style' => 'cursor: pointer;vertical-align: middle;padding: 5px',
                                    'id'    => $key . '-responsive'
                                ]
                            );
                        }
                        echo '</td>';
                    }
                }
                echo '</tr>';
            }

            echo '</table>';
        }
        echo '</div>';
        ?>
        <script type="text/javascript">

            jQuery('img.anwesenheit').on('click', function () {
                var clicked = this.id;
                jQuery.ajax({
                    url: '<?=UrlHelper::getURL()?>',
                    data: {
                        termin_id: clicked.split('-')[0],
                        user_id: clicked.split('-')[1]
                    },
                    success: function (ok) {
                        if (ok == '1') {
                            jQuery('#' + clicked).attr('src', jQuery('#' + clicked).attr('src').replace('unchecked', 'checked'));
                        } else {
                            jQuery('#' + clicked).attr('src', jQuery('#' + clicked).attr('src').replace('checked', 'unchecked'));
                        }
                    }
                });
            });
        </script>
        <?php
        $layout = $GLOBALS['template_factory']->open('layouts/base.php');

        $layout->content_for_layout = ob_get_clean();

        echo $layout->render();
    }


    public function lnw_action()
    {
        if (!Config::get()->PARTICIPANTS_ATTENDANCE_PLUGIN_ENABLE_LNW_ACTION) {
            throw new AccessDeniedException(
                _('Diese Aktion ist nicht aktiviert!')
            );
        }
        if (!$GLOBALS['perm']->have_studip_perm('tutor', Context::getId())) {
            throw new AccessDeniedException(_("Zugriff verweigert"));
        }
        PageLayout::setHelpKeyword("Basis.VeranstaltungenVerwaltenTeilnehmer");
        PageLayout::setTitle(Context::getHeaderLine() . ' - ' . _("Leistungsnachweisliste"));
        if (Navigation::hasItem('/course/members/' . $this->me . 'lnw')) {
            Navigation::activateItem('/course/members/' . $this->me . 'lnw');
        }
        $this->addStylesheet('assets/ParticipantsAttendancePlugin.less');
        PageLayout::addHeadElement(
            'script',
            [
                'type' => 'text/javascript',
                'src' => $this->getPluginURL() . '/assets/ParticipantsAttendancePlugin.js'
            ]
        );

        ob_start();
        $db = new DB_Seminar();
        $header = '';
        $table = [];
        $termine = [];
        $termine_anwesenheit =& $_SESSION['ParticipantsAttendancePlugin'][Context::getId()];
        if (!isset($termine_anwesenheit)) {
            $termine_anwesenheit = [];
        }
        $seminar = Seminar::GetInstance(Context::getId());
        $semester = SemesterData::GetInstance();
        $seminar_semester = $semester->getSemesterDataByDate($seminar->getSemesterStartTime());
        $study_areas = [];
        $sem_path = get_sem_tree_path($seminar->getId());
        if (is_array($sem_path)) {
            foreach ($sem_path as $sem_tree_id => $path_name) {
                $study_areas[] = $path_name;
            }
        }

        //$dozenten = join(', ', array_map(create_function('$a', 'return $a["Nachname"];'), $seminar->getMembers('dozent')));



        $participants = User::findBySql(
            "INNER JOIN seminar_user
            USING (user_id)
            WHERE
            seminar_user.seminar_id = :course_id
            AND
            seminar_user.status = 'autor'
            ORDER BY auth_user_md5.nachname ASC,
            auth_user_md5.vorname ASC",
            [
                'course_id' => $seminar->getId()
            ]
        );

        $this->matrikelnr = [];
        $this->course_of_study = [];
        $this->participated = Request::getArray('participated', []);
        $this->lnw = Request::getArray('lnw', []);
        $this->grade = Request::getArray('grade', []);
        foreach ($participants as $user) {
            if (self::$datafield_id_matrikel) {
                $this->matrikelnr[$user->id] = self::getDatafieldValue(
                    self::$datafield_id_matrikel,
                    $user->id
                );
            }
            $first_course_of_study = $user->studycourses[0]->studycourse;
            if ($first_course_of_study) {
                $this->course_of_study[$user->id] = $first_course_of_study->name;
            }
        }

        $print_view = false;
        $clear_print_view = false;

        if (Request::submitted('print_view')) {
            $print_view = true;
        } elseif (Request::submitted('clear_print_view')) {
            $print_view = true;
            $clear_print_view = true;
            $this->participated = [];
            $this->lnw = [];
            $this->grade = [];
        } elseif (Request::submitted('table_export')) {
            $header = sprintf(
                _('Leistungsnachweisliste: %1$s (%2$s) - %3$s'),
                $seminar->getName(),
                $seminar->seminar_number,
                $seminar_semester['name']
            );
            $lecturers = [];
            foreach ($seminar->getMembers('dozent') as $lecturer) {
                $lecturers[] = $lecturer['fullname'];
            }

            $table_data = [
                [
                    _('Nachname'),
                    _('Vorname'),
                    _('Matrikelnummer'),
                    _('Studiengang'),
                    _('Teilnahme bestanden'),
                    _('Leistungsnachweis'),
                    _('Note')
                ]
            ];

            foreach ($participants as $participant) {
                $row = [
                    $participant->Nachname,
                    $participant->Vorname,
                    $this->matrikelnr[$participant->user_id],
                    $this->course_of_study[$participant->user_id],
                    $this->participated[$participant->user_id],
                    $this->lnw[$participant->user_id],
                    $this->grade[$participant->user_id]
                ];

                $table_data[] = $row;
            }

            $tmpfile = basename(
                $this->create_result_xls_lnw(
                    $header,
                    $study_areas,
                    $lecturers,
                    $seminar->ects,
                    $table_data
                )
            );

            if ($tmpfile) {
                header(
                    'Location: '
                    . FileManager::getDownloadURLForTemporaryFile(
                        $tmpfile,
                        dgettext(
                            'ParticipantsAttendancePlugin',
                            'Leistungsnachweisliste'
                        )
                        . '.xls'
                    )
                );
                page_close();
                die;
            }
        }

        $factory = new Flexi_TemplateFactory(__DIR__ . '/templates');
        $template = $factory->open('lnw.php');

        $html = $template->render(
            [
                'print_view' => $print_view,
                'clear_print_view' => $clear_print_view,
                'seminar' => $seminar,
                'seminar_semester' => $seminar_semester,
                'study_areas' => $study_areas,
                'participants' => $participants,
                'matrikelnr' => $this->matrikelnr,
                'course_of_study' => $this->course_of_study,
                'participated' => $this->participated,
                'lnw' => $this->lnw,
                'grade' => $this->grade
            ]
        );

        $layout = $GLOBALS['template_factory']->open('layouts/base.php');
        $layout->content_for_layout = $html;
        echo $layout->render();
    }


    function create_result_xls($header, $table)
    {

        $tempfile = null;
        if (count($table)) {
            $tmpfile = $GLOBALS['TMP_PATH'] . '/' . md5(uniqid('write_excel', 1));
            // Creating a workbook
            $workbook = new Workbook($tmpfile);
            $head_format = $workbook->addformat();
            $head_format->set_size(12);
            $head_format->set_bold();
            $head_format->set_align("left");
            $head_format->set_align("vcenter");

            $head_format_merged = $workbook->addformat();
            $head_format_merged->set_size(12);
            $head_format_merged->set_bold();
            $head_format_merged->set_align("left");
            $head_format_merged->set_align("vcenter");
            $head_format_merged->set_merge();
            $head_format_merged->set_text_wrap();

            $caption_format = $workbook->addformat();
            $caption_format->set_size(10);
            $caption_format->set_align("left");
            $caption_format->set_align("vcenter");
            $caption_format->set_bold();
            //$caption_format->set_text_wrap();

            $data_format = $workbook->addformat();
            $data_format->set_size(10);
            $data_format->set_align("left");
            $data_format->set_align("vcenter");

            $caption_format_merged = $workbook->addformat();
            $caption_format_merged->set_size(10);
            $caption_format_merged->set_merge();
            $caption_format_merged->set_align("left");
            $caption_format_merged->set_align("vcenter");
            $caption_format_merged->set_bold();

            // Creating the first worksheet
            $worksheet1 = $workbook->addworksheet(_("Anwesenheitsliste"));
            $worksheet1->set_row(0, 20);
            $worksheet1->write_string(0, 0, utf8_decode($header), $head_format);
            $worksheet1->set_row(1, 20);

            foreach (range(1, 10) as $c) $worksheet1->write_blank(0, $c, $head_format);
            foreach (range(1, 10) as $c) $worksheet1->write_blank(1, $c, $head_format);
            foreach ($table as $r => $row) {
                if ($r == 0) {
                    foreach ($row as $c => $col) {
                        $worksheet1->write_string($r + 2, $c, utf8_decode($col), $caption_format);
                    }
                } else {
                    foreach ($row as $c => $col) {
                        if ($c == 0 || ($c == 1 && self::$datafield_id_matrikel)) {
                            $max_len[] = strlen($col);
                        } else {
                            list($key, $value) = each($col);
                            $col = $value ? 'x' : '';
                        }
                        $worksheet1->write_string($r + 2, $c, utf8_decode($col), $data_format);
                    }
                }
            }
            $worksheet1->set_column(0, 0, max($max_len));
            foreach (range(1, count($table[0]) - 1) as $c) $worksheet1->set_column(0, $c, 15);
            $workbook->close();

        }
        return $tmpfile;
    }


    public function create_result_xls_lnw($header, $arStudienbereiche, $arDozentInnen, $sECTS, $table)
    {
        if (!Config::get()->PARTICIPANTS_ATTENDANCE_PLUGIN_ENABLE_LNW_ACTION) {
            throw new AccessDeniedException(
                _('Diese Aktion ist nicht aktiviert!')
            );
        }
        $tempfile = null;

        //The maximum amount of columns a single-cell-row shall span:
        $colspan = 6;

        if (count($table)) {
            $tmpfile = $GLOBALS['TMP_PATH'] . '/' . md5(uniqid('write_excel', 1));
            // Creating a workbook
            $workbook = new Workbook($tmpfile);

            // titelzeile
            $head_format = $workbook->addformat();
            $head_format->set_size(12);
            $head_format->set_bold();
            $head_format->set_align("left");
            $head_format->set_align("vcenter");
            $head_format->set_text_wrap();
            // spaltenüberschriften
            $caption_format = $workbook->addformat();
            $caption_format->set_size(10);
            $caption_format->set_align("left");
            $caption_format->set_align("vcenter");
            $caption_format->set_bold();
            $caption_format->set_text_wrap();
            // datenzeilen
            $data_format = $workbook->addformat();
            $data_format->set_size(10);
            $data_format->set_align("left");
            $data_format->set_align("vcenter");
            $data_format->set_text_wrap();
            // formatierung unterschriftsfeld
            $sign_format = $workbook->addformat();
            $sign_format->set_size(10);
            $sign_format->set_align("left");
            $sign_format->set_align("top");
            $sign_format->set_bold();

            // Creating the first worksheet
            $worksheet1 = $workbook->addworksheet(_("Leistungsnachweisliste"));
            $worksheet1->set_margin_right(0.5);

            // höhe der ersten 4 zeilen erhöhen
            foreach (range(0, 3) as $r) $worksheet1->set_row($r, 20);
            // spaltenbreite der ersten 5 spalten setzen
            $worksheet1->set_column(0, 4, 12);
            //The lnw column:
            $worksheet1->set_column(4, 5, 17);
            //The degree column:
            $worksheet1->set_column(5, 6, 8);

            // titel setzen
            $sText = iconv('UTF-8', 'CP1252', "Leistungsnachweisliste für Studienleistungen");
            $worksheet1->write_string(0, 0, $sText, $head_format);
            // gruppierung der ersten 4 spalten der ersten zeile
            // syntax: merge_cells(zeile_von,spalte_von,zeile_bis,spalte_bis)
            $worksheet1->merge_cells(0, 0, 0, $colspan);
            // veranstaltungsinformationen
            $worksheet1->write_string(
                1,
                0,
                iconv('UTF-8', 'CP1252', $header),
                $caption_format
            );
            // gruppierung der zeilen 2 bis 4
            $worksheet1->merge_cells(1, 0, 3, $colspan);
            // dozentInnen
            $sText = "DozentInnen";
            $worksheet1->write_string(4, 0, $sText, $caption_format);
            $worksheet1->merge_cells(4, 0, 4, $colspan);
            $i = 5;
            foreach ($arDozentInnen as $sDozentIn) {
                $worksheet1->write_string(
                    $i,
                    0,
                    iconv('UTF-8', 'CP1252', "- " . $sDozentIn),
                    $data_format
                );
                $worksheet1->merge_cells($i, 0, $i, $colspan);
                $i++;
            }
            // leerzeile
            $worksheet1->merge_cells($i, 0, $i, $colspan);
            $i++;
            //
            $sText = "ECTS-Kreditpunkte : " . $sECTS;
            $worksheet1->write_string($i, 0, $sText, $caption_format);
            $worksheet1->merge_cells($i, 0, $i, $colspan);
            $i++;
            // leerzeile
            $worksheet1->merge_cells($i, 0, $i, $colspan);
            $i++;
            // studienbereiche
            $sText = "Studienbereich(e) der Veranstaltung";
            $worksheet1->write_string($i, 0, $sText, $caption_format);
            $worksheet1->merge_cells($i, 0, $i, $colspan);
            // studienbereiche anzeigen + spalten gruppieren
            $i++;
            foreach ($arStudienbereiche as $sStudienbereich) {
                if (strlen($sStudienbereich) > 85) {
                    // zeilenhöhe wg. text_wrap erhöhen
                    $worksheet1->set_row($i, 25);
                }
                $worksheet1->write_string(
                    $i,
                    0,
                    iconv('UTF-8', 'CP1252', "- " . $sStudienbereich),
                    $data_format
                );
                $worksheet1->merge_cells($i, 0, $i, $colspan);
                $i++;
            }
            // leerzeile
            $worksheet1->merge_cells($i, 0, $i, $colspan);
            $i++;

            $worksheet1->write_string($i, 0, "Hinweis", $head_format);
            $worksheet1->merge_cells($i, 0, $i, $colspan);
            $i++;
            $sText = iconv(
                'UTF-8',
                'CP1252',
                "Diese Liste ist ausschließlich zur Weitergabe an das Prüfungsamt bestimmt und darf aus Datenschutzgründen nicht veröffentlicht werden"
            );
            $worksheet1->write_string($i, 0, $sText, $data_format);
            // zwei verbundene zeilen für den hinweistext
            $worksheet1->merge_cells($i, 0, $i + 1, $colspan);
            // leerzeile
            $worksheet1->merge_cells($i + 2, 0, $i + 2, $colspan);
            $i += 3;

            // die eigentliche tabelle
            foreach ($table as $r => $row) {
                // spaltenköpfe
                if ($r == 0) {
                    foreach ($row as $c => $col) {
                        // zeilenhöhe wg. text_wrap erhöhen
                        $worksheet1->set_row($r + $i, 25, $caption_format);
                        $worksheet1->write_string(
                            $r + $i,
                            $c,
                            iconv('UTF-8', 'CP1252', $col),
                            $caption_format
                        );
                    }
                } else {
                    // datenzeilen
                    foreach ($row as $c => $col) {
                        $value = iconv('UTF-8', 'CP1252', $col);
                        if ($c == 4) {
                            $value = ($col ? 'x' : '');
                        }
                        $worksheet1->write_string($r + $i, $c, $value, $data_format);
                    }
                }
            }
            // nächste zeile
            $i = $i + $r + 1;
            // leerzeile
            $worksheet1->merge_cells($i, 0, $i, $colspan);
            $i++;
            // zwei verbundene zeilen für das unterschriftsfeld
            $sText = iconv('UTF-8', 'CP1252', "Datum und Unterschrift der/s Lehrenden");
            $worksheet1->write_string($i, 0, $sText, $sign_format);
            $worksheet1->merge_cells($i, 0, $i + 2, $colspan);
            $workbook->close();
        }
        return $tmpfile;
    }


    public static function getDatafieldValue($datafield_id, $range_id, $sec_range_id = '')
    {
        $db = DBManager::get();
        $p = $db->prepare("SELECT content FROM datafields_entries WHERE datafield_id=? AND range_id=? AND sec_range_id=?");
        if ($p->execute([$datafield_id, $range_id, $sec_range_id])) {
            return $p->fetchColumn();
        }
    }
}

?>
