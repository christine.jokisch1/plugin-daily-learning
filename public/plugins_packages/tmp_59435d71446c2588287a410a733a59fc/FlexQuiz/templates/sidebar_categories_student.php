<section id='flexquiz' style='display:none'>
        <aside class='fq-sidebar'>
        <ol class='chapters'>
            <p class='chapters-header'> Kategorien</p>
            <? if (sizeof((array)$cats)) : ?>
                <? foreach ($cats as $cat) : ?>
                    <li class='chapters cat_<?= $cat['supercategory_id']?>' id='cat_<?= $cat['supercategory_id']?>'>
                        <a href='<?= PluginEngine::getLink('flexquiz/student/questions', array('cat' => $cat['supercategory_id'])) ?>' class="navigate">

                        <span id='cat-title-wrapper'>
                            <div class='cat-title'><?= $cat['title']?>  </div>
                            <span style='float: right;' class='badge rounded-pill b_flexible'><?= $cat['answered']?> | <?= $cat['count']?></span>
                        </span>

                        </a>
                    </li>
                <? endforeach ?>
            <? endif ?>
            <?php if(count($cats) == 0){ ?>
            <li class='chapters'>
                Keine Kategorien vorhanden.
            </li>
            <?php } ?>
        </ol>
    </aside>
</section>
