<?php

class AddTriesToAnswers extends Migration
{

    public function description()
    {
        return 'Add additional row tries to existing table flexquiz_quiz for the flexquiz plugin.';
    }

       
    public function up () {
        $db = DBManager::get();

        $db->exec("
            ALTER TABLE `flexquiz_quiz` ADD `tries` int(30) DEFAULT NULL AFTER `result`;
        ");

        $db->exec("
            ALTER TABLE `flexquiz_quiz` ADD `final` int(30) DEFAULT NULL AFTER `tries`;
        ");

        SimpleORMap::expireTableScheme();
    }

    public function down() {
       

    }

}
