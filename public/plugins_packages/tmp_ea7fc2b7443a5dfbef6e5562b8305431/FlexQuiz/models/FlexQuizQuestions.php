<?php

class FlexQuizQuestions extends SimpleORMap
{

    public static function insert($data)
    {
        /* Prüfung ob bereits eine gleichnamige Oberkategorie in diesem Kurs existiert */
      $supercategory = $data['supercategory'];
      $subcategory = $data['subcategory'];

      $stmt1 = DBManager::get()->prepare("SELECT *, COUNT(*) as count FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND flexquiz_questions.seminar_id = :seminar_id
                                            AND flexquiz_supercategories.title = :title
                                            ");
        $stmt1->bindValue(':seminar_id', $data['seminar_id']);
        $stmt1->bindValue(':title', $supercategory);
        $stmt1->execute();
        $result = $stmt1->fetch();

        if($result['count'] >= 1){                  /* Wenn ja, wird die ID übernommen */
            $id['id'] = $result['supercategory_id'];
        } else {                                    /* Wenn nein, wird die kategorie eingetragen */
            $stmt2 = DBManager::get()->prepare("INSERT INTO flexquiz_supercategories(title, icon) VALUES (?,?)");
            $stmt2->execute([$supercategory, 'fab fa-buffer']);
            $stmt3 = DBManager::get()->prepare("SELECT max(supercategory_id) as id FROM flexquiz_supercategories");
            $stmt3->execute();
            $id = $stmt3->fetch();
        }


        $stmt4 = DBManager::get()->prepare(
            "INSERT INTO flexquiz_questions (seminar_id, title, category, type, question_type, supercategory_id, release_date, response_date, subcategory, content, hint)
            VALUES (?,?,?,?,?,?,?,?,?,?,?)");

            $stmt4->execute([
                $data['seminar_id'],
                $data['title'],
                $data['category'],
                $data['type'],
                $data['question_type'],
                $id['id'],
                $data['release_date'],
                $data['response_date'],
                $data['subcategory'],
                $data['question_content'],
                $data['hint'],
            ]);

            $stmt5 = DBManager::get()->prepare("SELECT max(question_id) as id FROM flexquiz_questions WHERE seminar_id = :seminar_id");
            $stmt5->bindValue(':seminar_id', $data['seminar_id']);
            $stmt5->execute();
            $id = $stmt5->fetch();

            $upload_file_answers = $data['file_answers'];

            if(!empty($data['file'])){
                $stmt = DBManager::get()->prepare(
                    "UPDATE flexquiz_questions
                    SET file = ?
                    WHERE question_id = ?");
                $stmt->execute(['frage_'.$id['id'].'.'.$data['extension'], $id['id']]);
            }

            foreach((object) $data['content'] as $key => $content){
                $key = $key-1;

                if($upload_file_answers[$key]['extension'] == ''){
                    $name = '';
                } else {
                    $name = 'antwort_'.$id['id'].'_'.$key.'.'.$upload_file_answers[$key]['extension'];
                }

                if($data['question_type'] == 'Text'){
                    $bool =  '1';
                } else {
                    $bool =  $data['boolean'][$key];
                }

                if($content != NULL){
                    $stmt6 = DBManager::get()->prepare("INSERT INTO flexquiz_answers (question_id, option_content, file, is_right) VALUES (?,?,?,?)");
                    $stmt6->execute([
                        $id['id'],
                        $content,
                        $name,
                        $bool,
                    ]);

                    if (!file_exists($data['tempDirAnswers'])) {
                        mkdir($data['tempDirAnswers']);
                    }

                }
            }

     return $id['id'];
    }

    public static function edit($data){

        $result = FlexQuizQuestions::checkCountSupercategories($data['supercategory']);

        if($result['scount'] > 0){
            $data['supercategory_id'] = $result['supercategory_id'];
        } else {
            $data['supercategory_id'] = FlexQuizQuestions::createNewSupercategory($data['supercategory']);
        }

        $stmt = DBManager::get()->prepare(
            "UPDATE flexquiz_questions
            SET title = ?, category = ?, question_type = ?, supercategory_id = ?, release_date = ?, response_date = ?,
                subcategory = ?, content = ?, type = ?, hint = ?, file = ?
            WHERE question_id = ?");
        $stmt->execute([$data['title'], $data['category'], $data['question_type'],
        $data['supercategory_id'], $data['release_date'], $data['response_date'],
        $data['subcategory'], $data['content'], $data['type'], $data['hint'], $data['file'],
        $data['questionID']]);
        FlexQuizQuestions::checkUnusedSupercategories();
        return $data['supercategory_id'];
    }

    public static function checkUnusedSupercategories(){
        $stmt = DBManager::get()->prepare(
            "DELETE FROM flexquiz_supercategories
            WHERE NOT EXISTS ( SELECT 1
                              FROM flexquiz_questions
                              WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                           )
            ");
        $stmt->execute();
    }


    public static function checkCountSupercategories($supercategory){
        /* Überprüfung ob anzahl tite > 0 oder > 1 ist, wenn > 1 dann keine Löschung; ansonsten löschung */
        $stmt = DBManager::get()->prepare(
            "SELECT *, count(*) as scount
                FROM flexquiz_supercategories, flexquiz_questions
                WHERE flexquiz_supercategories.supercategory_id = flexquiz_questions.supercategory_id
                AND flexquiz_supercategories.title = :supercategory
            ");
        $stmt->bindValue(':supercategory', $supercategory);
        $stmt->execute();

        return $stmt->fetch();
    }

    public static function createNewSupercategory($supercategory){
        $stmt2 = DBManager::get()->prepare("INSERT INTO flexquiz_supercategories(title, icon) VALUES (?,?)");
        $stmt2->execute([$supercategory, 'fab fa-buffer']);
        $stmt3 = DBManager::get()->prepare("SELECT max(supercategory_id) as id FROM flexquiz_supercategories");
        $stmt3->execute();
        $result = $stmt3->fetch();
        return $result['id'];
    }

    public static function getQuestions($course_id){

        $stmt = DBManager::get()->prepare("SELECT flexquiz_questions.supercategory_id FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND flexquiz_questions.seminar_id = :course_id
                                        ");
        $stmt->bindValue(':course_id', $course_id);
        $stmt->execute();

        return $stmt;
    }

    public static function getQuestionByID($question_id){
        $stmt = DBManager::get()->prepare("SELECT *, flexquiz_supercategories.title as stitle, flexquiz_questions.title as qtitle
                                            FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND flexquiz_questions.question_id = :question_id
                                        ");
        $stmt->bindValue(':question_id', $question_id);
        $stmt->execute();
        return $stmt;
    }


    public static function getQuestion($question_id, $cat){
        $stmt = DBManager::get()->prepare("SELECT *, flexquiz_supercategories.title as stitle, flexquiz_questions.title as qtitle
                                            FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND flexquiz_questions.question_id = :question_id
                                            AND flexquiz_questions.supercategory_id = :cat
                                        ");
        $stmt->bindValue(':question_id', $question_id);
        $stmt->bindValue(':cat', $cat);
        $stmt->execute();
        return $stmt;
    }

    public static function getOpenQuestion($user_id,$cid){
        $stmt = DBManager::get()->prepare("SELECT *
                                            FROM  `flexquiz_questions`
                                            WHERE flexquiz_questions.seminar_id = :cid
                                            AND NOW()) > DATE(flexquiz_questions.release_date)
                                            AND NOW() < DATE(flexquiz_questions.response_date)
                                            AND NOT EXISTS
                                                (SELECT *
                                                FROM flexquiz_quiz
                                                WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                                                AND flexquiz_quiz.user_id = :user_id)
                                        ");
        $stmt->bindValue(':cid', $cid);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt;
    }

    public static function getFirstOpenQuestion($user_id,$cid,$cat){
            $stmt = DBManager::get()->prepare("SELECT *
                                            FROM flexquiz_questions
                                            WHERE flexquiz_questions.seminar_id = :cid
                                            AND  flexquiz_questions.supercategory_id = :cat
                                            AND NOW() > DATE(flexquiz_questions.release_date)
                                            AND DATE(flexquiz_questions.response_date) > NOW()
                                            AND NOT EXISTS
                                                (SELECT *
                                                FROM flexquiz_quiz
                                                WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                                                AND flexquiz_quiz.user_id = :user_id)
                                            LIMIT 1
                                        ");
        $stmt->bindValue(':cid', $cid);
        $stmt->bindValue(':cat', $cat);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function getNextOpenQuestion($user_id,$cid,$current_id,$cat){

        $stmt = DBManager::get()->prepare("SELECT *
                FROM flexquiz_questions
                WHERE flexquiz_questions.seminar_id = :cid
                AND flexquiz_questions.supercategory_id = :cat
                AND flexquiz_questions.question_id > :current_id

                AND NOT EXISTS
                    (SELECT *
                    FROM flexquiz_quiz
                    WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                    AND flexquiz_quiz.user_id = :user_id
                    )
                LIMIT 1
            ");
        $stmt->bindValue(':current_id', $current_id);
        $stmt->bindValue(':cat', $cat);
        $stmt->bindValue(':cid', $cid);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function getPreviousOpenQuestion($user_id,$cid,$current_id, $cat){
        $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_questions
        WHERE flexquiz_questions.question_id = (
            select max(flexquiz_questions.question_id)
            from flexquiz_questions
            where flexquiz_questions.question_id < :current_id
            AND flexquiz_questions.seminar_id = :cid
            AND flexquiz_questions.supercategory_id = :cat
            AND NOT EXISTS
            (SELECT *
            FROM flexquiz_quiz
            WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
            AND flexquiz_quiz.user_id = :user_id)
            )
            AND DATE(NOW()) >= DATE(flexquiz_questions.release_date)
            AND DATE(NOW()) <= DATE(flexquiz_questions.response_date)
            LIMIT 1
            ");
        $stmt->bindValue(':current_id', $current_id);
        $stmt->bindValue(':cid', $cid);
        $stmt->bindValue(':cat', $cat);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function getNextOpenQuestionByID($user_id,$cid,$current_id,$category){

        $stmt = DBManager::get()->prepare("SELECT *
                FROM flexquiz_questions
                WHERE flexquiz_questions.seminar_id = :cid
                AND flexquiz_questions.question_id > :current_id
                AND flexquiz_questions.category = :category
                AND NOT EXISTS
                    (SELECT *
                    FROM flexquiz_quiz
                    WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                    AND flexquiz_quiz.user_id = :user_id
                    )
                AND DATE(NOW()) >= DATE(flexquiz_questions.release_date)
                AND DATE(NOW()) <= DATE(flexquiz_questions.response_date)
                LIMIT 1
            ");
        $stmt->bindValue(':current_id', $current_id);
        $stmt->bindValue(':cid', $cid);
        $stmt->bindValue(':category', $category);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function getPreviousOpenQuestionByID($user_id,$cid,$current_id,$category){
        $stmt = DBManager::get()->prepare(
            "SELECT * FROM flexquiz_questions
            WHERE flexquiz_questions.question_id = (
                select max(flexquiz_questions.question_id)
                from flexquiz_questions
                where flexquiz_questions.question_id < :current_id
                AND flexquiz_questions.seminar_id = :cid
                AND flexquiz_questions.category = :category
                AND DATE(NOW()) >= DATE(flexquiz_questions.release_date)
                AND DATE(NOW()) <= DATE(flexquiz_questions.response_date)
                AND NOT EXISTS
                (SELECT *
                FROM flexquiz_quiz
                WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                AND flexquiz_quiz.user_id = :user_id)
                )
            LIMIT 1
        ");
        $stmt->bindValue(':current_id', $current_id);
        $stmt->bindValue(':cid', $cid);
        $stmt->bindValue(':category', $category);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function getQuestionsUp($course_id, $user_id){

        $stmt = DBManager::get()->prepare("SELECT flexquiz_questions.supercategory_id, flexquiz_questions.question_id,
                                                flexquiz_questions.file, flexquiz_questions.response_date,
                                                flexquiz_questions.release_date,
                                                flexquiz_questions.title, flexquiz_questions.type,
                                                flexquiz_questions.category, flexquiz_supercategories.title as stitle
                                            FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND flexquiz_questions.seminar_id = :course_id
                                            AND DATE(NOW()) >= DATE(flexquiz_questions.release_date)
                                        ");
        $stmt->bindValue(':course_id', $course_id);
        $stmt->execute();
        $entries = array();
        $entries = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($entries as $key => $question){

            $status = DBManager::get()->prepare("SELECT * FROM `flexquiz_quiz`, `flexquiz_quiz_results`, `flexquiz_questions`
                                                WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                                                AND flexquiz_quiz.question_id = :question_id
                                                AND flexquiz_quiz.user_id = :user
                                                GROUP BY flexquiz_quiz.question_id
            ");
            $status->bindValue(':question_id', $entries[$key]['question_id']);
            $status->bindValue(':user', $user_id);
            $status->execute();

            if($status->rowCount() > 0){
                $entries[$key]['answered'] = 'Beantwortet';
            } else{
                $entries[$key]['answered'] = 'Ausstehend';
            }

            $stmt = DBManager::get()->prepare("SELECT * FROM `flexquiz_answers` WHERE question_id = :question_id");
            $stmt->bindValue(':question_id',  $entries[$key]['question_id']);
            $stmt->execute();

            foreach($stmt as $key1 => $answers){
                if(!empty($answers['file'])){
                    $entries[$key]['file_check'] = '1';
                }else{
                    $entries[$key]['file_check'] = '0';
                }
            }
        }

        return $entries;
    }

    public static function getQuestionsUpByCat($course_id, $user_id, $cat){

        $stmt = DBManager::get()->prepare("SELECT flexquiz_questions.supercategory_id, flexquiz_questions.question_id,
                                                flexquiz_questions.file, flexquiz_questions.response_date,
                                                flexquiz_questions.release_date,
                                                flexquiz_questions.title, flexquiz_questions.type,
                                                flexquiz_questions.category, flexquiz_supercategories.title as stitle
                                            FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND flexquiz_questions.seminar_id = :course_id
                                            AND DATE(NOW()) >= DATE(flexquiz_questions.release_date)
                                            AND flexquiz_questions.supercategory_id = :cat
                                        ");
        $stmt->bindValue(':course_id', $course_id);
        $stmt->bindValue(':cat', $cat);
        $stmt->execute();
        $entries = array();
        $entries = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($entries as $key => $question){

            $status = DBManager::get()->prepare("SELECT * FROM `flexquiz_quiz`, `flexquiz_quiz_results`, `flexquiz_questions`
                                                WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                                                AND flexquiz_quiz.question_id = :question_id
                                                AND flexquiz_quiz.user_id = :user
                                                GROUP BY flexquiz_quiz.question_id
            ");
            $status->bindValue(':question_id', $entries[$key]['question_id']);
            $status->bindValue(':user', $user_id);
            $status->execute();

            if($status->rowCount() > 0){
                $entries[$key]['answered'] = 'Beantwortet';
            } else{
                $entries[$key]['answered'] = 'Ausstehend';
            }

            $stmt = DBManager::get()->prepare("SELECT * FROM `flexquiz_answers` WHERE question_id = :question_id");
            $stmt->bindValue(':question_id',  $entries[$key]['question_id']);
            $stmt->execute();

            foreach($stmt as $key1 => $answers){
                if(!empty($answers['file'])){
                    $entries[$key]['file_check'] = '1';
                }else{
                    $entries[$key]['file_check'] = '0';
                }
            }
        }

        return $entries;
    }

    public static function getQuestionsByCat($supercategory_id){
        $stmt = DBManager::get()->prepare("SELECT * FROM `flexquiz_questions` WHERE `supercategory_id` = :supercategory_id");
        $stmt->bindValue(':supercategory_id', $supercategory_id);
        $stmt->execute();
        return $stmt;
    }

    public static function getCategories($course_id){
        $stmt = DBManager::get()->prepare("SELECT *, COUNT(flexquiz_questions.question_id) as count,
                                            sum(case when flexquiz_questions.category = 'flexible' then 1 else 0 end) AS flexCount,
                                            sum(case when flexquiz_questions.category = 'daily' then 1 else 0 end) AS dailyCount
                                            FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND `seminar_id` = :course_id
                                            GROUP BY flexquiz_questions.supercategory_id
                                            ");
        $stmt->bindValue(':course_id', $course_id);
        $stmt->execute();
        return $stmt;
    }

    public static function getCategoriesCount($course_id){
        $stmt = DBManager::get()->prepare("SELECT COUNT(flexquiz_questions.question_id) as count
                                            FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND `seminar_id` = :course_id
                                            GROUP BY flexquiz_questions.supercategory_id
                                            ");
        $stmt->bindValue(':course_id', $course_id);
        $stmt->execute();
        $row =  $stmt->fetch();
        return $row['count'];

    }

    public static function getCategoriesByUser($course_id, $user_id){
        $stmt = DBManager::get()->prepare("SELECT *, COUNT(flexquiz_questions.question_id) as count FROM `flexquiz_questions`, `flexquiz_supercategories`
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND `seminar_id` = :course_id
                                            GROUP BY flexquiz_questions.supercategory_id");
        $stmt->bindValue(':course_id', $course_id);
        $stmt->execute();
        $entries = array();
        $entries = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($entries as $key => $entry){
            $status =  DBManager::get()->prepare("SELECT flexquiz_quiz.question_id FROM `flexquiz_questions`,`flexquiz_supercategories`, `flexquiz_quiz`
                                                    WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
						                            AND flexquiz_quiz.question_id = flexquiz_questions.question_id
                                                    AND flexquiz_questions.supercategory_id = :supercategory
						                            AND flexquiz_quiz.user_id = :user
                                                    GROUP BY flexquiz_questions.supercategory_id, flexquiz_quiz.question_id");
            $status->bindValue(':supercategory', $entry['supercategory_id']);
            $status->bindValue(':user', $user_id);
            $status->execute();
            $row =  $status->fetchAll(PDO::FETCH_ASSOC);

            $i = 0;
            if($row != null || !empty($row)){
                foreach($row as $row){

                    $i++;
                }
            } else {
                $i = 0;
            }

            $entries[$key]['answered'] = $i;
            $entries[$key]['left'] = $entry['count'] - $i;

        }

        return $entries;
    }

    public static function timeAllocation($category, $release_date, $response_date, $release_date_daily){

        if( $category == 'flexible') {
            /* Date zuweisen */
            $currentYear = date("Y");
            $currentMonth = date("m");

            if($release_date == null || $release_date == ''){
                $release_date =  date("Y-m-d H:i:s");
            }

            if($response_date == null || $response_date == ''){
                if(($currentMonth > "03") && ($currentMonth < "10")){
                    /* SoSe */
                    $response_date = $currentYear."-04-01 00:00:00";

                    } else {
                    $response_date = $currentYear."-03-31 00:00:00";
                }
            }
        } else {
            $release_date = $release_date_daily;
            $response_date = date('Y-m-d h:i:s', strtotime($release_date_daily. ' + 1 days'));
        }

        return array(
            'release_date' =>  $release_date,
            'response_date' =>  $response_date
        );
    }

    public static function removeIMG($question_id, $path){
        $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_questions WHERE question_id = :question_id");
        $stmt->bindValue(':question_id', $question_id);
        $stmt->execute();
        $res =  $stmt->fetch();
        unlink($path.'/'.$res['file']);

        $stmt = DBManager::get()->prepare("UPDATE flexquiz_questions
                    SET file = ?
                    WHERE question_id = ?");
        $stmt->execute(['', $question_id]);
    }
        public static function updateIMG($id, $qid, $extension)
        {
            $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_answers WHERE question_id = :question_id");
            $stmt->bindValue(':question_id', $qid);
            $stmt->execute();

            foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $key => $answers){
                if($answers['answer_id'] == $id){
                    $path = "antwort_".$qid."_".$key.".".$extension;
                    $stmt = DBManager::get()->prepare("UPDATE flexquiz_answers
                    SET file = ?
                    WHERE answer_id = ?");
                    $stmt->execute([$path, $id]);
                }
            }
            return $path;
        }

        public static function getDailyQuestions($cid, $id){
            $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_questions
            WHERE seminar_id = :seminar_id
            AND flexquiz_questions.category = 'daily'
            AND NOT EXISTS
                (SELECT *
                FROM flexquiz_quiz
                WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                AND flexquiz_quiz.user_id = :user_id)
            AND DATE(NOW()) >= DATE(flexquiz_questions.release_date)
            AND DATE(NOW()) <= DATE(flexquiz_questions.response_date)
            ");

            $stmt->bindValue(':seminar_id', $cid);
            $stmt->bindValue(':user_id', $id);
            $stmt->execute();

        return $stmt;
    }

    public static function getFlexibleQuestions($cid, $id){
        $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_questions
            WHERE seminar_id = :seminar_id
            AND flexquiz_questions.category = 'flexible'
            AND NOT EXISTS
                (SELECT *
                FROM flexquiz_quiz
                WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                AND flexquiz_quiz.user_id = :user_id)
            ");

        $stmt->bindValue(':seminar_id', $cid);
        $stmt->bindValue(':user_id', $id);
        $stmt->execute();

        return $stmt;
    }

    public static function getFirstOpenQuestionByCategory($cid, $id, $category){
            $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_questions
            WHERE seminar_id = :seminar_id
            AND flexquiz_questions.category = :category
            AND NOT EXISTS
                (SELECT *
                FROM flexquiz_quiz
                WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                AND flexquiz_quiz.user_id = :user_id)
            limit 1
            ");

            $stmt->bindValue(':seminar_id', $cid);
            $stmt->bindValue(':category',$category);
            $stmt->bindValue(':user_id', $id);
            $stmt->execute();
            return  $stmt->fetch();
    }
}

?>
