<div class='row' style='width:99%'>
    <div class='col-sm-12 m-4'>
        <h2 class='mb-3'>Verlauf beantworteter Fragen</h2>

        <? if($hcount == 0){ ?>
            <div class='box-info'>
            <i class="icon icon-info fas fa-question"></i>
                <div class='my-3'>
                    Es wurden bisher <b>keine</b> Quiz-Fragen beantwortet.
                </div>
            </div>
        <? } else { ?>
        <table class="default sortable-table" data-sortlist="[[1,0]]">
            <colgroup>
                <col width="30"></col>
                <col width="300"></col>
            </colgroup>
            <thead>
            <tr class="sortable">
                <th data-sort="text">#</th>
                <th data-sort="text">Titel</th>
                <th data-sort="text">Schwierigkeit</th>
                <th data-sort="text">Beantwortet am</th>
                <th data-sort="text">Ergebnis</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($history as $key => $history){ ?>
                <tr>
                    <td> <?php echo $key+1?> </td>
                    <td> <?php echo $history['title'] ?> </td>
                    <td> <?php echo $history['type'] ?> </td>
                    <td> <?php echo date("d.m.Y, H:i", strtotime($history['response_date'])); ?></td>
                    <td> <?php echo ($history['result'] == '1' ? 'Richtig' : 'Falsch' ) ?> </td>
                    <td>
                        <a class='btn button m-0' href='<?= PluginEngine::getLink('flexquiz/student/details', array('quiz' => $history['quiz_id'])) ?>'>
                            Betrachten
                        </a>
                    </td>
                </tr>
            <?php } ?>
            <tfoot></tfoot>
        </table>
        <? } ?>
    </div>
</div>

<?= $this->sidebar ?>
