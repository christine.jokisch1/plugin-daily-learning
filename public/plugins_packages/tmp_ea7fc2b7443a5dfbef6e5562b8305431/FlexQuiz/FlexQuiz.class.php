<?php
/*
 *  Copyright (c) 2012-2019  Rasmus Fuhse <fuhse@data-quest.de>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 */

/**
 * Class Blubber - the Blubber-plugin
 * This is only used to manage blubber within a course.
 */
require_once 'models/FlexQuizQuestions.php';
StudipAutoloader::addAutoloadPath(__DIR__ . '/models');

class FlexQuiz extends StudipPlugin implements StandardPlugin
{

     /**
     * Returns a navigation for the tab displayed in the course.
     * @param string $course_id of the course
     * @return \Navigation
     */

    public function __construct()
    {
        $GLOBALS['plugin_path'] = $this->getPluginPath().'core/flexquiz/files';
        parent::__construct();

    }

    /**
     * This method dispatches all actions.
     *
     * @param string $unconsumed_path  part of the dispatch path that was not consumed
     */

    public function perform($unconsumed_path)
    {
        $this->addStylesheet('stylesheets/style.less');
        $this->addStylesheet('stylesheets/all.css');
        $this->addStylesheet('stylesheets/bootstrap.css');
        $this->addScript('javascript/wysiwyg.js');
        $this->addScript('javascript/admin.js');
        $this->addScript('javascript/student.js');
        $this->addScript('javascript/bootstrap.bundle.min.js');
        parent::perform($unconsumed_path);
    }

    public function getTabNavigation($course_id)
    {
        //NavigationForLecturers
        if ($GLOBALS['perm']->have_perm('tutor') || $GLOBALS['perm']->have_perm('dozent')) {
            $tab = new Navigation(
                _('FlexQuiz'),
                PluginEngine::getURL($this, [], 'database/index')
            );

            $tab->setImage(Icon::create('flexquiz', Icon::ROLE_INFO_ALT));
            Navigation::addItem('course/flexquiz', $tab);

            $test2nav = new Navigation('Datenbestand', 'plugins.php/flexquiz/database/index');
            $test3nav = new Navigation('Quiz hinzufügen', 'plugins.php/flexquiz/database/create');

            Navigation::addItem('course/flexquiz/database', $test2nav);
            Navigation::addItem('course/flexquiz/create', $test3nav);

        } else {
            $tab = new Navigation(
                _('FlexQuiz'),
                PluginEngine::getURL($this, [], 'student/index')
            );

            $tab->setImage(Icon::create('flexquiz', Icon::ROLE_INFO_ALT));
            Navigation::addItem('course/flexquiz', $tab);
            $nav1 = new Navigation('Übersicht', 'plugins.php/flexquiz/student/index');
            Navigation::addItem('course/flexquiz/student', $nav1);
          //  $fragen = new Navigation('Fragen', 'plugins.php/flexquiz/student/fragen');
          //  Navigation::addItem('course/flexquiz/fragen',  $fragen);
        }
    }

    /**
     * Returns a navigation-object with the grey/red icon for displaying in the
     * my_courses.php page.
     * @param string  $course_id
     * @param int $last_visit
     * @param string|null  $user_id
     * @return \Navigation
     */

    public function getIconNavigation($course_id, $last_visit, $user_id = NULL)
    {
        $user_id || $user_id = $GLOBALS['user']->id;

        $icon = new Navigation(
            _('flexquiz'),
            PluginEngine::getURL($this, [], 'index')
        );
    }
    /**
     * Returns no template, because this plugin doesn't want to insert an
     * info-template in the course-overview.
     * @param string $course_id
     * @return null
     */
    public function getInfoTemplate($course_id)
    {
        return null;
    }

}
