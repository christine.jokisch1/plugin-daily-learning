<?php
/*
 *  Copyright (c) 2012-2019  Rasmus Fuhse <fuhse@data-quest.de>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 */

/**
 * Class Blubber - the Blubber-plugin
 * This is only used to manage blubber within a course.
 */
class DailyLearning extends StudIPPlugin implements StandardPlugin
{

     /**
     * Returns a navigation for the tab displayed in the course.
     * @param string $course_id of the course
     * @return \Navigation
     */

    
    public function __construct()
    {
        parent::__construct();
        $demonav  = new Navigation('Daily', 'board/course');
        $test1nav = new Navigation('Test 1', 'board/course', array('test1' => 1));   // URL= new Navigation('Test 1', 'dispatch.php/demo/index', array('test' => 1));   // URL
        $test2nav = new Navigation('Test 2', 'board/course', array('test2' => 2));   // URL= new Navigation('Test 1', 'dispatch.php/demo/index', array('test' => 1));   // URL
        $test3nav = new Navigation('Test 3', 'board/course', array('test3' => 3));   // URL= new Navigation('Test 1', 'dispatch.php/demo/index', array('test' => 1));   // URL

        Navigation::addItem('/course/demo', $demonav);
        $demonav->addSubNavigation('test1', $test1nav);
        $demonav->addSubNavigation('test2', $test2nav);
        $demonav->addSubNavigation('test3', $test3nav);
        Navigation::activateItem('course/demo');
    }

    public function getTabNavigation($course_id)
    {   
        $tab = new Navigation(
            _('Daily'),
            PluginEngine::getURL($this, [], 'board/course') // einfach nur course.php ist auch möglich
        );
        $tab->setImage(Icon::create('dailylearning', Icon::ROLE_INFO_ALT));
        return ['dailylearning' => $tab];
    }
        /** 
     * Returns a navigation-object with the grey/red icon for displaying in the
     * my_courses.php page.
     * @param string  $course_id
     * @param int $last_visit
     * @param string|null  $user_id
     * @return \Navigation
     */

    public function getIconNavigation($course_id, $last_visit, $user_id = null)
    {
        $icon = new Navigation(
            _('dailylearning'),
            PluginEngine::getURL($this, [], 'index')
        );
    }
    /**
     * Returns no template, because this plugin doesn't want to insert an
     * info-template in the course-overview.
     * @param string $course_id
     * @return null
     */
    public function getInfoTemplate($course_id)
    {
        return null;
    }

}
