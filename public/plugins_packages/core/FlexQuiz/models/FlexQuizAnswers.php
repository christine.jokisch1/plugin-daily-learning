
    <?php

    class FlexQuizAnswers {

    /**
     * Returns the id of the currently selected seminar or false, if no seminar
     * is selected
     *
     * @return mixed  seminar_id or false
     */
    
    public static function getAnswers($question_id)
    {
        $stmt = DBManager::get()->prepare("SELECT * FROM `flexquiz_answers` WHERE question_id = :question_id");
        $stmt->bindValue(':question_id', $question_id);
        $stmt->execute();
        return $stmt; 
    }

    public static function getCountAnswers($question_id){
        $stmt = DBManager::get()->prepare("SELECT count(*) as count FROM `flexquiz_answers` WHERE question_id = :question_id");
        $stmt->bindValue(':question_id', $question_id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result['count']; 
    }

    public static function getTextAnswer($question_id)
    {
        $stmt = DBManager::get()->prepare("SELECT * FROM `flexquiz_answers` WHERE question_id = :question_id");
        $stmt->bindValue(':question_id', $question_id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function removeAnswerIMG($answer_id, $path){
        $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_answers WHERE answer_id = :answer_id");
        $stmt->bindValue(':answer_id', $answer_id);
        $stmt->execute();
        $res =  $stmt->fetch();
        unlink($path.'/'.$res['file']);

        $stmt = DBManager::get()->prepare("UPDATE flexquiz_answers
                    SET file = ?
                    WHERE answer_id = ?");
        $stmt->execute(['', $answer_id]);
    }

      public static function editAnswer($answer_id, $value){

          $stmt = DBManager::get()->prepare(
              "UPDATE flexquiz_answers
                SET option_content = ?
                WHERE answer_id = ?");
          $stmt->execute([$value, $answer_id]);
      }


}
?>
