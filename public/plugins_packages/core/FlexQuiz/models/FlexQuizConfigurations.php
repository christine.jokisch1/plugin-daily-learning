<?php

class FlexQuizConfigurations {

    public static function editconfigcheck($cid)
    {

        $stmt = DBManager::get()->prepare(
            "SELECT * FROM flexquiz_configurations
           WHERE flexquiz_configurations.seminar_id = :seminar_id
           ");
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function insertconfig($cid, $pluginname, $welcome_dozent, $welcome_student, $hint_questions){

        $stmt = DBManager::get()->prepare(
            "INSERT INTO flexquiz_configurations (seminar_id, plugin_name, welcome_dozent, welcome_student, hint_questions)
            VALUES (?,?,?,?, ?)");

            $stmt->execute([
                $cid,
                $pluginname,
                $welcome_dozent, 
                $welcome_student,
                $hint_questions
            ]);
            return;
    }

    public static function editconfig($cid, $pluginname, $welcome_dozent, $welcome_student, $hint_questions){

        $stmt = DBManager::get()->prepare(
            "UPDATE flexquiz_configurations
                 SET plugin_name = ?, welcome_dozent = ?, welcome_student= ?, hint_questions = ?
                 WHERE seminar_id = ?");

            $stmt->execute([
                $pluginname,
                $welcome_dozent, 
                $welcome_student,
                $hint_questions,
                $cid
            ]);

            return;
    }

    public static function getconfig($cid){
        $stmt = DBManager::get()->prepare(
            "SELECT * FROM flexquiz_configurations
           WHERE flexquiz_configurations.seminar_id = :seminar_id
           ");
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->execute();
        return $stmt; 
    }

    public static function getHintQuestions($cid){
        $stmt = DBManager::get()->prepare(
            "SELECT hint_questions FROM flexquiz_configurations
           WHERE flexquiz_configurations.seminar_id = :seminar_id
           ");
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->execute();
        $res = $stmt->fetch(); 

        if($res['hint_questions']){
            return $hint_questions = $res['hint_questions']; 
        } else {
            return $hint_questions = '2'; 
        }

        return $hint_questions; 
    }

    public static function getPluginname($config){
        $pluginname_query = $config->fetch(); 

        if($pluginname_query['plugin_name']){
            return $plugin_name = $pluginname_query['plugin_name']; 
        } else {
            return $plugin_name = 'FlexQuiz'; 
        }
    }
}
?>
