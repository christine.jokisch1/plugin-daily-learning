
    <?php

    class FlexQuizBase {

    /**
     * Returns the id of the currently selected seminar or false, if no seminar
     * is selected
     *
     * @return mixed
     */

    public static function getQuiz($quiz_id)
    {
        $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_quiz 
                                            WHERE quiz_id = :quiz_id");  
        $stmt->bindValue(':quiz_id', $quiz_id);
        $stmt->execute();
        $result = $stmt->fetch(); 
       
        return $result;  
    }

    public static function getFirstResult($results)
    {
        foreach ($results as $item){
            $res = 0;
            if ($item['option_is_right'] == 1 && $item['chosen'] == '0' ){
                 $res = 0;
                break;
            } else if($item['option_is_right'] == 0 && $item['chosen'] == '1'){
                $res = 0;
                break; 
            } else {
                $res = 1;
            }
        }
        return $res; 
    }

    public static function auswertung($boolean, $options)
    {  
        $results = array();
        foreach((object) $options as $key => $option){

            if( ($boolean[$key] == $option['is_right']) && ($option['is_right'] == '1') ){ //wenn die option korrekt ausgewählt wurde
                $results[] = array('is_right' => 1, 'option_is_right' => $option['is_right'], 'chosen' => $boolean[$key], 'color' => 'ans-green', 'disabled' => 'disabled');
           
            } else if($boolean[$key] != $option['is_right'] && $option['is_right'] == '0'){
                $results[] = array('is_right' => 0, 'option_is_right' => $option['is_right'], 'chosen' => $boolean[$key], 'color' => 'ans-red', 'disabled' => 'disabled');
           
            } else {
                $results[] = array('is_right' => 0,'option_is_right' => $option['is_right'], 'chosen' => $boolean[$key], 'color' => 'ans-neutral', 'disabled' => 'false');
        
            }   
            
        } 

     // print_r($results); 
/*
        foreach((object) $options as $key => $option){

            if($boolean[$key] == $option['is_right'] && $option['is_right'] == '1'){ //wenn die option korrekt ausgewählt wurde
                $results[] = array('is_right' => 1, 'color' => 'ans-green', 'disabled' => true);
            } else if($boolean[$key] == $option['is_right'] && $option['is_right'] == '0'){ //wenn die korrekte antwort nicht ausgewählt wurde
                $results[] = array('is_right' => 1, 'color' => 'ans-neutral', 'disabled' => false);
            } else if($boolean[$key] != $option['is_right']){
                $results[] = array('is_right' => 0, 'color' => 'ans-red', 'disabled' => true);
           } 
        }
*/
        return $results;
    }


    public static function insertQuiz($user_id, $question_id, $answers, $boolean, $results, $pageState, $prog)
    {

        if($pageState == '1'){
            $zwres = DBManager::get()->prepare("SELECT tries, MAX(quiz_id) as lastquizid
                                                FROM flexquiz_quiz 
                                                WHERE user_id = :user_id 
                                                AND question_id = :question_id ");  
            $zwres->bindValue(':user_id', $user_id);
            $zwres->bindValue(':question_id', $question_id);
            $zwres->execute();
            $resultat = $zwres->fetch(); 
            $lastquizid = $resultat['lastquizid']; 
        } 
        $res = 0;
        foreach ($results as $item){
            
            if ($item['is_right'] == '0' && $item['option_is_right'] == '1'){
                $res = 0;
                break;
            } else if($item['chosen'] == '1' && $item['option_is_right'] == '0') {
                $res = 0;
                break;
            } else if($item['is_right'] == '1' && $item['option_is_right'] == '1') {
                $res = 1;
            } 
            
        }

        if($pageState == '0'){
            $stmt = DBManager::get()->prepare(
                "INSERT INTO flexquiz_quiz (user_id, question_id, result, tries, final) VALUES (?,?,?,?,?)");
            $stmt->execute([$user_id,$question_id,$res,'1',$prog]);
            
            /* QuizID für die Eintragung der Antworten (nur erster Versucht) */
            $stmt1 = DBManager::get()->prepare("SELECT max(quiz_id) as quizid FROM flexquiz_quiz
            WHERE user_id = :user_id AND question_id = :question_id
                        ");
            $stmt1->bindValue(':question_id', $question_id);
            $stmt1->bindValue(':user_id', $user_id);
            $stmt1->execute();
            $id = $stmt1->fetch();

            /* Eintragung der Antworten (nur erster Versucht) */
            foreach((object) $boolean as $key => $bool){
                $stmt2 = DBManager::get()->prepare(
                    "INSERT INTO flexquiz_quiz_results (quiz_id, answer_id, is_right) VALUES (?,?,?)");
                $stmt2->execute([$id['quizid'],$answers[$key], $bool]);
         }
        /* Für weitere Versuche keine Eintragung der Antworten, sondern Aktualisierung der Versuche */
        } else {
            // get the quiz 
            $stmt1 = DBManager::get()->prepare("SELECT * FROM flexquiz_quiz
            WHERE quiz_id = :quiz_id
            ");
            $stmt1->bindValue(':quiz_id', $lastquizid);
            $stmt1->execute();
            $data = $stmt1->fetch();
            $tries = $data['tries'] + 1; 
             // update the quiz
            $stmt = DBManager::get()->prepare("UPDATE flexquiz_quiz
            SET tries = ?, final = ?
            WHERE quiz_id = ?");
            $stmt->execute([$tries, $prog, $lastquizid]);
        }

        return;
    }

    public static function getLastQuizidByUser($user_id, $question_id){
        $zwres = DBManager::get()->prepare("SELECT MAX(quiz_id) as lastquizid
        FROM flexquiz_quiz 
        WHERE user_id = :user_id 
        AND question_id = :question_id ");  
        $zwres->bindValue(':user_id', $user_id);
        $zwres->bindValue(':question_id', $question_id);
        $zwres->execute();
        $resultat = $zwres->fetch(); 
        $lastquizid = $resultat['lastquizid']; 
        return $lastquizid;
    }

    public static function getUserTriesByQuizID($lastquizid){
        $stmt1 = DBManager::get()->prepare("SELECT * FROM flexquiz_quiz WHERE quiz_id = :quiz_id
        ");
        $stmt1->bindValue(':quiz_id', $lastquizid);
        $stmt1->execute();
        $data = $stmt1->fetch();
        return $data['tries']; 
    }

    public static function insertQuizText($user_id, $question_id, $user_answers, $question_answer, $answer_id, $result, $pageState, $prog)
    {

        if( $user_answers != $question_answer){
            $res = 0;
        } else {
            $res = 1;
        }

        if($pageState == '0'){
            $stmt = DBManager::get()->prepare(
                "INSERT INTO flexquiz_quiz (user_id, question_id, result, tries, final) VALUES (?,?,?,?,?)");
            $stmt->execute([$user_id,$question_id,$res,'1',$result]);
            
            $stmt1 = DBManager::get()->prepare("SELECT max(quiz_id) as quizid FROM flexquiz_quiz
            WHERE user_id = :user_id AND question_id = :question_id
            ");
            $stmt1->bindValue(':question_id', $question_id);
            $stmt1->bindValue(':user_id', $user_id);
            $stmt1->execute();
            $id = $stmt1->fetch();

            $stmt2 = DBManager::get()->prepare(
                "INSERT INTO flexquiz_quiz_results (quiz_id, answer_id, is_right) VALUES (?,?,?)");
            $stmt2->execute([$id['quizid'],$answer_id, $res]);
            
        } else {
            $zwres = DBManager::get()->prepare("SELECT tries, MAX(quiz_id) as lastquizid
                                                FROM flexquiz_quiz 
                                                WHERE user_id = :user_id 
                                                AND question_id = :question_id ");  
            $zwres->bindValue(':user_id', $user_id);
            $zwres->bindValue(':question_id', $question_id);
            $zwres->execute();
            $resultat = $zwres->fetch(); 
            $lastquizid = $resultat['lastquizid']; 

            // get the quiz 
            $stmt1 = DBManager::get()->prepare("SELECT * FROM flexquiz_quiz WHERE quiz_id = :quiz_id
            ");
            $stmt1->bindValue(':quiz_id', $lastquizid);
            $stmt1->execute();
            $data = $stmt1->fetch();
            $tries = $data['tries'] + 1; 
            // update the quiz
            $stmt = DBManager::get()->prepare("UPDATE flexquiz_quiz
            SET tries = ?, final = ?
            WHERE quiz_id = ?");
            $stmt->execute([$tries, $result, $lastquizid]);
        }

        return;
    }

    public static function history($cid, $user_id){

        $stmt = DBManager::get()->prepare("SELECT 
                                            flexquiz_questions.title, flexquiz_questions.type,
                                            flexquiz_quiz.response_date,
                                            flexquiz_quiz.result, flexquiz_quiz.quiz_id
                                            FROM flexquiz_quiz, flexquiz_questions
                                            WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                                            AND flexquiz_quiz.user_id = :user_id
                                            AND flexquiz_questions.seminar_id = :seminar_id
                                            ");
        $stmt->bindValue(':user_id', $user_id);
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->execute();

        if($stmt->rowCount() <= 0){
            $stmt = array();
        }
        
        return $stmt;
    }

    public function link_for($path, $params = [])
    {
        return htmlReady($this->url_for($path, $params));
    }


    public static function auswertungText($user_answer, $answer)
    {
        if($user_answer == $answer){
            $results[] = array('is_right' => 1, 'color' => 'ans-green');
        } else {
            $results[] = array('is_right' => 0, 'color' => 'ans-red');
        }
        return $results;
    }

    public static function getFirstTextResult($user_id, $question_id, $last_quiz_ID){
 
        $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_quiz 
                                            WHERE user_id = :user_id 
                                            AND quiz_id = :quiz_id
                                            ");
        $stmt->bindValue(':user_id', $user_id);
        $stmt->bindValue(':quiz_id', $last_quiz_ID);
        $stmt->execute();
        $res = $stmt->fetch();

        return $res['final']; 
    }

    public static function getCatInformation($cid,$id,$cat){

        $data = array();

        $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_supercategories
                                            WHERE flexquiz_supercategories.supercategory_id = :cat");
        $stmt->bindValue(':cat', $cat);
        $stmt->execute();
        $result = $stmt->fetch();

        $data['title'] = $result['title'];

        $stmt = DBManager::get()->prepare("SELECT flexquiz_quiz.question_id FROM flexquiz_questions, flexquiz_quiz
                                            WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                                            AND flexquiz_questions.supercategory_id = :cat
                                            AND flexquiz_questions.seminar_id = :seminar_id
                                            AND flexquiz_quiz.user_id = :user
                                            GROUP BY flexquiz_quiz.question_id
                                            ");
        $stmt->bindValue(':cat', $cat);
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->bindValue(':user', $id);
        $stmt->execute();

        $data['answered'] = $stmt->rowCount();

        $stmt = DBManager::get()->prepare("SELECT count(*) as count FROM flexquiz_supercategories, flexquiz_questions
                                            WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                                            AND flexquiz_supercategories.supercategory_id = :cat
                                            AND flexquiz_questions.category = 'flexible'
                                            ");
        $stmt->bindValue(':cat', $cat);
        $stmt->execute();
        $result = $stmt->fetch();
        $data['flexible_count'] = $result['count'];
        return $data;
    }

    public static function getCatName($cat){
        $stmt = DBManager::get()->prepare("SELECT * FROM flexquiz_supercategories
        WHERE flexquiz_supercategories.supercategory_id = :cat");
        $stmt->bindValue(':cat', $cat);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function getCountQuestions($cat){
        $stmt = DBManager::get()->prepare(
                "SELECT count(flexquiz_questions.question_id) as count
                FROM flexquiz_supercategories, flexquiz_questions
                WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                AND flexquiz_supercategories.supercategory_id = :cat
              /*  AND DATE(NOW()) >= DATE(flexquiz_questions.release_date)
                AND DATE(NOW()) <= DATE(flexquiz_questions.response_date)
                */
        ");
        $stmt->bindValue(':cat', $cat);
        $stmt->execute();
        $result =  $stmt->fetch();

        $data = array();
        $data['count'] = $result['count'];
        return $data;
    }

    public static function getCountQuestionsTimeRestricted($cat){
        $stmt = DBManager::get()->prepare(
                "SELECT count(flexquiz_questions.question_id) as count
                FROM flexquiz_supercategories, flexquiz_questions
                WHERE flexquiz_questions.supercategory_id = flexquiz_supercategories.supercategory_id
                AND flexquiz_supercategories.supercategory_id = :cat
                AND DATE(NOW()) >= DATE(flexquiz_questions.release_date)
                AND DATE(NOW()) <= DATE(flexquiz_questions.response_date)
                
        ");
        $stmt->bindValue(':cat', $cat);
        $stmt->execute();
        $result =  $stmt->fetch();

        $data = array();
        $data['count'] = $result['count'];
        return $data;
    }
}
?>
