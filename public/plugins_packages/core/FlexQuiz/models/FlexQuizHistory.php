
<?php

class FlexQuizHistory {

    public static function getQuestion($quiz_id, $user_id){
        $stmt = DBManager::get()->prepare(
            "SELECT * FROM flexquiz_quiz, flexquiz_questions
            WHERE flexquiz_quiz.question_id = flexquiz_questions.question_id
            AND flexquiz_quiz.quiz_id = :quiz_id
            AND flexquiz_quiz.user_id = :user_id
            ");
        $stmt->bindValue(':quiz_id', $quiz_id);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetch();
    }
     public static function getQuizResults($quiz_id){
         $stmt = DBManager::get()->prepare(
             "SELECT *, flexquiz_quiz_results.is_right as answered_right FROM flexquiz_quiz_results, flexquiz_answers
            WHERE flexquiz_quiz_results.answer_id = flexquiz_answers.answer_id
            AND flexquiz_quiz_results.quiz_id = :quiz_id
            ");
         $stmt->bindValue(':quiz_id', $quiz_id);
         $stmt->execute();
         return $stmt;
     }

     public static function getCountTries($user_id){
        $stmt = DBManager::get()->prepare(
            "SELECT count(*) FROM flexquiz_quiz_results
           WHERE flexquiz_quiz.user_id = :user_id
           ");
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt;
    }

    public static function countResults($cid,$userId)
    {
        $stmt = DBManager::get()->prepare("SELECT count(*) as count FROM  `flexquiz_quiz`,`flexquiz_questions`
        WHERE  flexquiz_quiz.question_id = flexquiz_questions.question_id
        AND flexquiz_questions.seminar_id = :seminar_id
        AND flexquiz_quiz.user_id = :user_id");
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->bindValue(':user_id', $userId);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res['count'];
    }

    public static function getSortValue($sortres)
    {
        if($sortres == 'Beantwortet am'){
            $filter = "[[0,'a']]";
        } else if($sortres == "Titel") {
            $filter = "[[1,'a']]";
        } else if($sortres == "Schwierigkeit"){
            $filter = "[[2,'a']]";
        } else if($sortres == "Ergebnis"){
            $filter = "[[3,'a']]";
        } else {
            $filter = "[[0,'a']]";
        }
        return $filter; 
    }

    public static function historyCount($cid, $userId)
    {
        $stmt = DBManager::get()->prepare("SELECT count(flexquiz_quiz.quiz_id) as count
        FROM flexquiz_quiz, flexquiz_questions
        WHERE flexquiz_quiz.question_id = flexquiz_questions.question_id
        AND flexquiz_quiz.user_id = :user_id
        AND flexquiz_questions.seminar_id = :seminar_id

        ");
    $stmt->bindValue(':user_id', $userId);
    $stmt->bindValue(':seminar_id', $cid);
    $stmt->execute();
    return $stmt; 

    }

    public static function historyPagingt($cid, $userId, $start_from, $per_page_record)
    {
        $stmt = DBManager::get()->prepare("SELECT 
        flexquiz_questions.title, flexquiz_questions.type,
        flexquiz_quiz.response_date,
        flexquiz_quiz.result, flexquiz_quiz.quiz_id
        FROM flexquiz_quiz, flexquiz_questions
        WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
        AND flexquiz_quiz.user_id = :user_id
        AND flexquiz_questions.seminar_id = :seminar_id
        LIMIT :a, :b
        ");
    $stmt->bindValue(':user_id', $userId);
    $stmt->bindValue(':seminar_id', $cid);
    $stmt->bindValue(':a', $start_from);
    $stmt->bindValue(':b', $per_page_record);
    $stmt->execute();
    return $stmt; 

    }

    public static function historyPaging($cid, $userId, $limit, $offset, $sortres)
    {

        $stmt = DBManager::get()->prepare("SELECT 
                                            flexquiz_questions.title, flexquiz_questions.type,
                                            flexquiz_quiz.response_date,
                                            flexquiz_quiz.result, flexquiz_quiz.quiz_id
                                            FROM flexquiz_quiz, flexquiz_questions
                                            WHERE flexquiz_questions.question_id = flexquiz_quiz.question_id
                                            AND flexquiz_quiz.user_id = :user_id
                                            AND flexquiz_questions.seminar_id = :seminar_id
                                            LIMIT :limit 
                                            OFFSET :offset
                                            ");
        $stmt->bindValue(':user_id', $userId);
        $stmt->bindValue(':seminar_id', $cid);
        $stmt->bindValue(':limit', $limit);
        $stmt->bindValue(':offset', $offset);
        $stmt->execute();

        if($stmt->rowCount() <= 0){
            $stmt = array();
        }
        
        return $stmt;
    }
}
?>
