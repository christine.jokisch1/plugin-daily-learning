<?php

class IndexController extends PluginController {

    /**
     * Returns a navigation-object with the grey/red icon for displaying in the
     * my_courses.php page.
     * @param string  $course_id
     * @param int $last_visit
     * @param string|null  $user_id
     * @return \Navigation
     */

    public function before_filter(&$action, &$args)
    {
        parent::before_filter($action, $args);
        $this->assets_url = $this->plugin->getPluginURL() . '/assets/';
        PageLayout::setBodyElementId('FlexQuiz-index');
    }

	public function index_action() {
		PageLayout::setTitle(_("FlexQuiz"));
 
        //NavigationForLecturers
        if ($GLOBALS['perm']->have_perm('tutor') || $GLOBALS['perm']->have_perm('dozent') || $GLOBALS['perm']->have_perm('root')) {
            if (Navigation::hasItem('course/flexquiz/index')) {
                Navigation::activateItem('course/flexquiz/index');
            }
            $this->buildSidebarDozent();
        } else {
            if (Navigation::hasItem('course/flexquiz/student')) {
                Navigation::activateItem('course/flexquiz/student');
            }
            $this->buildSidebarStudent();
        }
	}

    public function database_action() {
        $this->set_layout(null);
		PageLayout::setTitle(_("FlexQuiz"));
        Navigation::activateItem('database');
	}

    protected function buildSidebarDozent()
    {
        $cid = Request::get('cid');
        $cats = FlexQuizQuestions::getCategories($cid);
        $sidebar = Sidebar::Get();

        # load flexi lib
        $path_to_the_templates = dirname(__FILE__) . '/../templates';
        # we need a template factory
        $factory = new Flexi_TemplateFactory($path_to_the_templates);
        $template = $factory->open('sidebar_categories');
        $template->set_attribute('cats', $cats);
        echo $template->render();
        $template = $factory->open('sidebar_views');
        echo $template->render();
    }

    protected function buildSidebarStudent()
    {
        print_r('student');
    }
}
