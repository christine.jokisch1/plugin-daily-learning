<?php

class StudentController extends PluginController {
    /**
     * Returns a navigation-object with the grey/red icon for displaying in the
     * my_courses.php page.
     * @param string  $course_id
     * @param int $last_visit
     * @param string|null  $user_id
     * @return \Navigation
     */

    public function before_filter(&$action, &$args)
    {
        parent::before_filter($action, $args);
        $this->assets_url = $this->plugin->getPluginURL() . '/assets/';
        PageLayout::setBodyElementId('FlexQuiz-index');
        $this->Nav();
    }

    public function Nav()
    {  
        $cid = URLHelper::getLinkParams()['cid'];
        $config = FlexQuizConfigurations::getconfig($cid); 
        $plugin_name = FlexQuizConfigurations::getPluginname($config); 

        PageLayout::setTitle(_($plugin_name));
        if (!$user_id) {
            $this->$user_id = $GLOBALS['user']->id;
        }

        if($GLOBALS['user']->perms == 'dozent' || $GLOBALS['user']->perms == 'root' || $GLOBALS['user']->perms == 'tutor' || $GLOBALS['user']->perms == 'autor') {
            Navigation::removeItem('course/flexquiz/index');
            Navigation::removeItem('course/flexquiz/database');
            Navigation::removeItem('course/flexquiz/create');
            Navigation::removeItem('course/flexquiz/configurations');

            $nav1 = new Navigation('Übersicht', 'plugins.php/flexquiz/student/index');
            Navigation::addItem('course/flexquiz/student', $nav1);
            $nav3 = new Navigation('Verlauf', 'plugins.php/flexquiz/student/history');
            Navigation::addItem('course/flexquiz/history', $nav3);

            $this->buildSidebar();

        }
    }

    public function index_action() {
        $this->$userId = $GLOBALS['user']->id;
        $cid = URLHelper::getLinkParams()['cid'];
        URLHelper::bindLinkParam('cid', $cid);
        $categories = FlexQuizQuestions::getCategoriesByUser($cid,$this->$userId);
        $this->categories = $categories;
        $questions = FlexQuizQuestions::getQuestionsUp($cid,$GLOBALS['user']->id);
        $this->questions = $questions;
        $this->daily = FlexQuizQuestions::getDailyQuestions($cid,$GLOBALS['user']->id);
        $this->dcount = $this->daily->rowCount();

        $this->flexible = FlexQuizQuestions::getFlexibleQuestions($cid,$GLOBALS['user']->id);
        $this->fcount = $this->flexible->rowCount();
        $config = FlexQuizConfigurations::getconfig($cid); 
        $this->config = $config->fetch(); 

        Navigation::activateItem('course/flexquiz/student');
    }

    public function fragen_action() {
        $cid = URLHelper::getLinkParams()['cid'];
        URLHelper::bindLinkParam('cid', $cid);
        $categories = FlexQuizQuestions::getCategories($cid);
        $this->categories = $categories;
        $questions = FlexQuizQuestions::getQuestionsUp($cid,$GLOBALS['user']->id);
        $this->questions = $questions;

        Navigation::activateItem('course/flexquiz/fragen');
    }

    public function questions_action() {

        $this->$userId = $GLOBALS['user']->id;
        $cid = URLHelper::getLinkParams()['cid'];
        $cat = Request::get('cat');

     $this->questions = FlexQuizQuestions::getQuestionsUpByCat($cid,$GLOBALS['user']->id,$cat);
         // $questions = FlexQuizQuestions::getQuestionsUp($cid,$GLOBALS['user']->id)
       $this->info = FlexQuizBase::getCatInformation($cid,$GLOBALS['user']->id,$cat);

        $this->cat = FlexQuizBase::getCatname($cat);
        $this->countAll = FlexQuizBase::getCountQuestions($cat);
        $this->getCountTimeRestricted = FlexQuizBase::getCountQuestionsTimeRestricted($cat);
        Navigation::activateItem('course/flexquiz/student');
    }

    public function dailyOrFlexibleQuestions($cid,$userId,$category,$question_id){

        if(!empty($question_id)){
            $this->questionID = $question_id;
        } else {
            $first = FlexQuizQuestions::getFirstOpenQuestionByCategory($this->cid,$this->userId,$category);
            $this->questionID = $first['question_id'];
        }

        $this->question = FlexQuizQuestions::getQuestionByID($this->questionID);
        $this->open_question_next = FlexQuizQuestions::getNextOpenQuestionByID($userId,$cid,$this->questionID,$category);
        $this->open_question_previous = FlexQuizQuestions::getPreviousOpenQuestionByID($userId,$cid,$this->questionID,$category);

        $this->next_array = array('id' => $this->open_question_next['question_id'], 'type' => $category);
        $this->previous_array = array('id' => $this->open_question_previous['question_id'],'type' => $category);
        $this->subarray = array('id' => $this->questionID, 'type' => $category);
    }

    public function openQuestionsByCat($cid,$questionID,$userId,$cat){
         if(!empty($questionID)){
            $this->questionID = $questionID;
        } else{
            $first = FlexQuizQuestions::getFirstOpenQuestion($userId,$cid,$cat);
            $this->questionID = $first['question_id'];
        }
        $this->question = FlexQuizQuestions::getQuestion($this->questionID, $cat);
        $this->open_question_next = FlexQuizQuestions::getNextOpenQuestion($this->userId,$this->cid, $this->questionID, $cat);
        $this->open_question_previous = FlexQuizQuestions::getPreviousOpenQuestion($this->userId,$this->cid, $this->questionID, $cat);
        $this->next_array = array('id' => $this->open_question_next['question_id'], 'cat' => $cat);
        $this->previous_array = array('id' => $this->open_question_previous['question_id'], 'cat' => $cat);
        $this->subarray = array('id' => $this->questionID, 'cat' => $cat);
    }

    public function quiz_action() {
        $user_id = $this->getUserID();
        $this->cid = URLHelper::getLinkParams()['cid'];
        $this->userId = $GLOBALS['user']->id;
        $this->cat = Request::getInstance()['cat'];
        $this->type = Request::getInstance()['type'];
        if(!empty(Request::getInstance()['type']) && (Request::getInstance()['type'] == 'daily' || Request::getInstance()['type'] == 'flexible')) {
            $this->dailyOrFlexibleQuestions($this->cid,$this->userId,Request::getInstance()['type'],Request::getInstance()['id']);
        } else {
            $this->openQuestionsByCat($this->cid,Request::getInstance()['id'],$this->userId,$this->cat);
        }

        $this->options = FlexQuizAnswers::getAnswers($this->questionID);
        Navigation::activateItem('course/flexquiz/student');
        $this->results = [];
        $this->pageState = 0;
        $this->user_answer = '';
        $this->user_tries = 0; 
        $this->hint_questions = FlexQuizConfigurations::getHintQuestions($this->cid); 

        if($_SESSION["pageState"] == 1){
            $this->pageState = $_SESSION["pageState"];
             
        } else {
            $_SESSION["pageState"] = 0;
        }

        if(Request::submitted('answer')) {
            if(!empty(Request::get('answer-text'))){
                $this->user_answer = Request::get('answer-text');
                $result = FlexQuizAnswers::getTextAnswer($this->questionID);
                $this->question_answer =  $result['option_content'];
                $this->answer_id =  $result['answer_id'];
                $this->results = FlexQuizBase::auswertungText($this->user_answer,$this->question_answer);
                FlexQuizBase::insertQuizText($user_id, $this->questionID, $this->user_answer, $this->question_answer, $this->answer_id, $this->results[0]['is_right'], $this->pageState, $this->prog);
                $this->last_quiz_ID = FlexQuizbase::getLastQuizidByUser($user_id, $this->questionID); 
                $this->prog = FlexQuizBase::getFirstTextResult($user_id, $this->questionID, $this->last_quiz_ID); 
                $this->pageState = 1;
                $_SESSION["pageState"] = 1; 
                $this->user_tries = FlexQuizbase::getUserTriesByQuizID($this->last_quiz_ID); 
            } else {
                $this->selected = Request::getArray('boolean');
                $this->results = FlexQuizBase::auswertung(Request::getArray('boolean'),FlexQuizAnswers::getAnswers($this->questionID));
                $this->options = FlexQuizAnswers::getAnswers($this->questionID); 
                $this->prog = FlexQuizBase::getFirstResult($this->results); 
                FlexQuizBase::insertQuiz($user_id, $this->questionID, Request::getArray('answers'), Request::getArray('boolean'),$this->results, $this->pageState, $this->prog);
                $this->pageState = 1;
                $_SESSION["pageState"] = 1; 
                $this->last_quiz_ID = FlexQuizbase::getLastQuizidByUser($user_id, $this->questionID); 
                $this->user_tries = FlexQuizbase::getUserTriesByQuizID($this->last_quiz_ID); 
            }
          } else {
            $_SESSION["pageState"] = 0; 
            $this->pageState = $_SESSION["pageState"]; 
          }
          $this->buildSidebar();

    }
    public function getQuestionLink($url,$cid) {
        return '../../../../data/upload_doc/flexquiz/'.$cid.'/fragen/'.$url;
    }

    public function getOptionLink($url,$cid) {
        return '../../../../data/upload_doc/flexquiz/'.$cid.'/antworten/'.$url;
    }

    protected function getUserID(){
        if (!$user_id) {
            $this->$user_id = $GLOBALS['user']->id;
        }
        return $this->$user_id;
    }

    protected function buildSidebar()
    {
        $this->$user_id = $GLOBALS['user']->id;
        $this->role = $GLOBALS['user']->perms;

        $cid = Request::get('cid');
        $cats = FlexQuizQuestions::getCategoriesByUser($cid,$this->$userId);
        $sidebar = Sidebar::Get();

        $path_to_the_templates = dirname(__FILE__).'/../templates';
        $factory = new Flexi_TemplateFactory($path_to_the_templates);

        $this->cats = $factory->open('sidebar_categories_student');
        $this->cats->set_attribute('cats', $cats);
        $this->template = $factory->open('sidebar_views');

         if($this->role == "autor"){
            $this->sidebar = $this->cats->render();
         } else {
            $this->sidebar = $this->template->render() .''. $this->cats->render();
         }
    }

    public function history_action(){
        $cid = Request::get('cid');
        $userId = $GLOBALS['user']->id;

        $this->per_page_record = 20;
        if(Request::get('page')) {
            $this->page  = Request::get('page');    
        } else {
            $this->page = 1;  
        }

        $this->start_from = ($this->page-1) * $this->per_page_record;     
        $this->total_records = FlexQuizHistory::countResults($cid,$userId);  
        $this->total_pages = ceil($this->total_records / $this->per_page_record);    
        $this->sortres = "[['0', 'a']]";
        
        if(Request::get('sort')){
            $this->sort = Request::get('sort');
            $this->sortres = FlexQuizHistory::getSortValue($this->sort); 
        } else {
            $this->sort = ""; 
        }
  
          $this->history  = FlexQuizHistory::historyPagingt($cid, $userId, $this->start_from, $this->per_page_record); 
          $res = FlexQuizHistory::historyCount($cid, $userId)->fetch();
          $this->hcount = $res['count'];

        Navigation::activateItem('course/flexquiz/history');
        return;
    }

    public function details_action(){
        $userId = $GLOBALS['user']->id;
        $this->quiz_id = Request::getInstance()['quiz'];
        $this->quiz = FlexQuizBase::getQuiz($this->quiz_id);
        $this->question = FlexQuizHistory::getQuestion($this->quiz_id,$userId);
        $this->options = FlexQuizHistory::getQuizResults($this->quiz_id);

        Navigation::activateItem('course/flexquiz/history');
        return;
    }
}
?>
