<?php
class databaseController extends PluginController {

     	/**
     * Returns a navigation-object with the grey/red icon for displaying in the
     * my_courses.php page.
     * @param string  $course_id
     * @param int $last_visit
     * @param string|null  $user_id
     * @return \Navigation
     */

    public function before_filter(&$action, &$args)
    {
        parent::before_filter($action, $args);
        $this->assets_url = $this->plugin->getPluginURL() . '/assets/';
        PageLayout::setBodyElementId('FlexQuiz-database');
    }

	public function index_action() {
        $cid = URLHelper::getLinkParams()['cid'];
        URLHelper::bindLinkParam('cid', $cid);
        $questions = FlexQuizQuestions::getQuestions($cid);
        $this->questions = $questions;
        $categories = FlexQuizQuestions::getCategories($cid);
        $this->categories = $categories;
        $this->catcount = FlexQuizQuestions::getCategoriesCount($cid);
		PageLayout::setTitle(_("Datenbestand"));
        Navigation::activateItem('course/flexquiz/database');
        $this->buildSidebar();

        $action = Request::getInstance()['action'];
        $cat = Request::getInstance()['cat'];

        $config = FlexQuizConfigurations::getconfig($cid); 
        $this->config = $config->fetch(); 

        switch ($action) {
            case 'edit':
                $this->editCat_action($cat);
                break;
            case 'delete':
                $this->deleteCat_action($cat);
                break;
            default:
        }

	}

    public function editcat_action(){;
       $this->cid = Request::get('cid');
       $this->catid = Request::get('cat');
       $this->category = FlexQuizSupercategories::getSupercategory($this->catid);

       if(Request::get('action') == 'editsend'){
        FlexQuizSupercategories::editCat([
             'supercategory_id'     => Request::get('catid'),
             'title'                => Request::get('bezeichnung'),
             'icon'                 => Request::get('icon'),
         ]);
       }
    }

    public function deleteCat_action($supercategory_id){
       FlexQuizSupercategories::delete($supercategory_id);
       $this->redirect(PluginEngine::getLink('flexquiz/database/index'));
    }

    public function create_action() {
		PageLayout::setTitle(_("Quiz hinzufügen"));
        Navigation::activateItem('course/flexquiz/create');
        $cid = URLHelper::getLinkParams()['cid'];
        $this->categories = FlexQuizQuestions::getCategories($cid);
        $this->buildSidebar();
	}

    public function edit_action(){
        $this->cid = Request::get('cid');
        $this->questionID = Request::get('qid');
        $this->cat = Request::get('cat');
        $this->count_answers = FlexQuizAnswers::getCountAnswers($this->questionID); 
        $path_question = $GLOBALS['UPLOAD_PATH'].'/flexquiz/'.$this->cid.'/fragen';
        $path_answers = $GLOBALS['UPLOAD_PATH'].'/flexquiz/'.$this->cid.'/antworten';
        if(Request::get('action') == 'removeIMG'){
           FlexQuizQuestions::removeIMG(Request::get('id'), $path_question);
        } elseif(Request::get('action') == 'removeOptionIMG') {
            FlexQuizAnswers::removeAnswerIMG(Request::get('id'), $path_answers);
        } elseif(Request::get('action') == 'editAnswer') {
            FlexQuizAnswers::editAnswer(Request::get('id'), Request::get('value'));
        } elseif($_POST["action"] == "editAnswerUpload"){

            $upload = $_FILES['file'];
            $tempDir = $GLOBALS['UPLOAD_PATH'].'/flexquiz/'. $this->cid.'/antworten';
            $array = explode('.', $upload['name']);
            $extension = end($array);

            $path = FlexQuizQuestions::updateIMG($_POST["id"],$_POST["qid"],$extension);
            move_uploaded_file($_FILES['file']['tmp_name'], "$tempDir/$path");
        } else {
            if (Request::submitted('edit_question')) {
                $upload = $_FILES['import_file'];
                $tempDir = $GLOBALS['UPLOAD_PATH'].'/flexquiz/'. $this->cid.'/fragen';
                $array = explode('.', $upload['name']);
                $extension = end($array);

                /* question img */
                if(!empty($_FILES['import_file']['tmp_name'])){
                    if (is_uploaded_file($_FILES['import_file']['tmp_name'])) {
                        $name = "frage_".$this->questionID.".".$extension;
                        move_uploaded_file($_FILES['import_file']['tmp_name'], "$tempDir/$name");
                    } else {
                        PageLayout::postError(sprintf('Datei konnte nicht hochgeladen werden'));
                    }
                }

             $cat =  FlexQuizQuestions::edit([
                   'questionID'         => $this->questionID,
                   'title'              => Request::get('title'),
                   'category'           => Request::get('category'),
                   'supercategory'      => Request::get('supercategory'),
                   'question_type'      => Request::get('question_type'),
                   'release_date'       => date("Y-m-d H:i:s", strtotime(Request::get('release_date'))),
                   'response_date'      => date("Y-m-d H:i:s", strtotime(Request::get('response_date'))),
                   'subcategory'        => Request::get('subcategory'),
                   'content'            => Request::get('question_content'),
                   'type'               => Request::get('type'),
                   'hint'               => Request::get('hint'),
                   'file'               => $name,
                   'extension'          => $extension,
                   'tempDir'            => $tempDir,
                   'content_ans'            => Request::getArray('content'),
                   'boolean'            => Request::getArray('boolean'),
                ]);
                $this->redirect(PluginEngine::getURL('flexquiz/database/edit', array('cat' => $cat, 'qid' => $this->questionID)));
            }
            $this->question = FlexQuizQuestions::getQuestion($this->questionID, $this->cat);
            $this->answers = FlexQuizAnswers::getAnswers($this->questionID);
        }

        PageLayout::setTitle(_("FlexQuiz"));
        Navigation::activateItem('course/flexquiz/database');
        $this->buildSidebar();
    }

    public function add_action(){
        $cid = URLHelper::getLinkParams()['cid'];

        if (Request::submitted('add_question')) {

            /* Es muss erst der Unterordner Flexquiz erstellt werden */
            if (!file_exists($GLOBALS['UPLOAD_PATH'].'\flexquiz')) {
                mkdir($GLOBALS['UPLOAD_PATH'].'\flexquiz', 0777, true);
            } 

            if (!file_exists($GLOBALS['UPLOAD_PATH'].'\flexquiz\\'.$cid.'')) {
                mkdir($GLOBALS['UPLOAD_PATH'].'\flexquiz\\'.$cid.'',0777, true);
            } 
                /* Fragen upload file */
                $upload = $_FILES['import_file'];
                chmod($GLOBALS['UPLOAD_PATH'],0777); 
                $tempDir = $GLOBALS['UPLOAD_PATH'].'/flexquiz/'.$cid.'/fragen';
                $tempDirAnswers = $GLOBALS['UPLOAD_PATH'].'/flexquiz/'.$cid.'/antworten';
                $array = explode('.', $upload['name']);
                $extension = end($array);  

                 /* Erstellung des Fragen-Ordners */
                if (!file_exists($tempDir)) {
                    mkdir($tempDir,0777, true);
                }     

                /* Erstellung des Antworten-Ordners */
                if (!file_exists($tempDirAnswers)) {
                    mkdir($tempDirAnswers,0777, true);
                }

                /* Time allocation */
                $time = FlexQuizQuestions::timeAllocation(Request::get('category'), Request::get("release_date"), Request::get('response_date'), Request::get('release_date_daily'));
 
                /* Antworten upload file */
                $upload_file_answer = $_FILES['answer_file'] ? : ['name' => []];
                $uploads_file_answers = [];
                for ($i = 0; $i < count($upload_file_answer['name']); ++$i) {
                    foreach ($upload_file_answer as $key => $value) {
                        $upload_file_answers[$i][$key] = $value[$i];
                        $array_answers = explode('.', $upload_file_answers[$i]['name']);
                        $upload_file_answers[$i]['extension'] = end($array_answers);
                    }
                }

            $tit = $upload_file_answers[0]['extension'];
            $insert = FlexQuizQuestions::insert([
             'title'            => Request::get('title'),
             'seminar_id'       => $cid,
             'category'         => Request::get('category'),
             'type'             => Request::get('type'),
             'question_type'    => Request::get('question_type'),
             'supercategory'    => Request::get('supercategory'),
             'subcategory'      => Request::get('subcategory'),
             'release_date'     => date("Y-m-d H:i:s", strtotime($time['release_date'])),
             'response_date'    => date("Y-m-d H:i:s", strtotime($time['response_date'])),
             'content'          => Request::getArray('content'),
             'boolean'          => Request::getArray('boolean'),
             'question_content' => Request::get('question_content'),
             'file'             => $upload['name'],
             'extension'        => $extension,
             'tempDir'          => $tempDir,
             'file_answers'     => $upload_file_answers,
             'tempDirAnswers'   => $tempDirAnswers
         ]);

        /* Antworten upload file */
        $upload_file_answer = $_FILES['answer_file'] ?: ['name' => []];
        $uploads_file_answers = [];
        for ($i = 0; $i < count($upload_file_answer['name']); ++$i) {
            foreach ($upload_file_answer as $key => $value) {
                $upload_file_answer[$i][$key] = $value[$i];
                $array_answers = explode('.', $upload_file_answer[$i]['name']);
                $upload_file_answer[$i]['extension'] = end($array_answers);
            }

            if(!empty($upload_file_answer[$i]['tmp_name'])){
                if (is_uploaded_file($upload_file_answer[$i]['tmp_name'])) {
                    $name = "antwort_".$insert."_".$i.".".$upload_file_answer[$i]['extension'];
                    move_uploaded_file($upload_file_answer[$i]['tmp_name'], "$tempDirAnswers/$name");
                } else {
                    PageLayout::postError(sprintf('Datei konnte nicht hochgeladen werden'));
                }
            }
        }

         if(!empty($_FILES['import_file']['tmp_name'])){
            if (is_uploaded_file($_FILES['import_file']['tmp_name'])) {
                $name = "frage_".$insert.".".$extension;
                move_uploaded_file($_FILES['import_file']['tmp_name'], "$tempDir/$name");
            } else {
                PageLayout::postError(sprintf('Datei konnte nicht hochgeladen werden'));
            }
        }
     }

         if($insert){
            $this->redirect(PluginEngine::getLink('flexquiz/database/index'));
            PageLayout::postSuccess('Die Frage wurde erfolgreich angelegt.');
         }
    }

    public function questions_action(){
        $cid = URLHelper::getLinkParams()['cid'];
        $config = FlexQuizConfigurations::getconfig($cid); 
        $plugin_name = FlexQuizConfigurations::getPluginname($config); 
        
        PageLayout::setTitle(_($plugin_name));
        Navigation::activateItem('course/flexquiz/database');

        $this->supercategory_id = Request::getInstance()['cat'];
        $this->questions = FlexQuizQuestions::getQuestionsByCat($this->supercategory_id);
        $this->category = FlexQuizSupercategories::getCategoryTitle(Request::getInstance()['cat']);
        $this->count = FlexQuizSupercategories::getCountQuestionsByCategory(Request::getInstance()['cat']);
        $this->buildSidebar();
    }

    public function getQuestionLink($url,$cid) {
      
       // return 'file:///'.$GLOBALS['UPLOAD_PATH'].'/flexquiz/'.$cid.'/fragen/'.$url;
        return '../../../../data/upload_doc/flexquiz/'.$cid.'/fragen/'.$url;
    }

    public function getAnswerLink($url,$cid) {
       
        return '../../../../data/upload_doc/flexquiz/'.$cid.'/antworten/'.$url;
     //   return $GLOBALS['UPLOAD_PATH'].'/flexquiz/'.$cid.'/antworten/'.$url;
    }

    public function configurations_action(){
        PageLayout::setTitle(_("Konfigurationen"));
        Navigation::activateItem('course/flexquiz/configurations');
        $cid = URLHelper::getLinkParams()['cid'];

        if(Request::submitted('configurations') == 'edit_question'){
            $pluginname = Request::get('pluginname');
            $welcome_dozent = Request::get('welcome_dozent');
            $welcome_student = Request::get('welcome_student');
            $hint_questions = Request::get('hint_questions');

            $check = FlexQuizConfigurations::editconfigcheck($cid); 
            if($check){
                $edit = FlexQuizConfigurations::editconfig($cid, $pluginname, $welcome_dozent, $welcome_student, $hint_questions); 
            } else {
                $insert = FlexQuizConfigurations::insertconfig($cid, $pluginname, $welcome_dozent, $welcome_student, $hint_questions); 
            }
        }

        $config = FlexQuizConfigurations::getconfig($cid); 
        $this->config = $config->fetch(); 

        //$navigation = Navigation::getItem('database/index');
   
        $this->buildSidebar();
    }

    protected function buildSidebar()
    {
        $cid = Request::get('cid');
        $cats = FlexQuizQuestions::getCategories($cid);
        $sidebar = Sidebar::Get();
        # load flexi lib
        $path_to_the_templates = dirname(__FILE__) . '/../templates';
        # we need a template factory
        $factory = new Flexi_TemplateFactory($path_to_the_templates);
        $this->catcount = FlexQuizQuestions::getCategoriesCount($cid);
        $this->s_cats = $factory->open('sidebar_categories');
        $this->s_cats->set_attribute('cats', $cats);
        $this->s_views = $factory->open('sidebar_views');
       // echo $template->render();
    }

}
