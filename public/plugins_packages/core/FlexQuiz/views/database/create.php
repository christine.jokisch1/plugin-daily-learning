
<form class="default" id="create" autocomplete="off" name="add" method="post" action="<?= PluginEngine::getLink('flexquiz/database/add') ?>" enctype="multipart/form-data">
 <?= CSRFProtection::tokenTag() ?>
      <fieldset class='pt-0'>
          <legend>Pflichtfelder</legend>
            <div class="mb-3">
                <div class="row">
                    <div class="col mt-3">
                        <label for="title" class="form-label"><b>Name</b></label>
                            <input type="text" name="title" class="" placeholder="Name" required>
                    </div>
                    <div class="col mt-3">
                        <label for="Fragenkategorie" class="form-label"><b>Fragenkategorie</b></label>
                        <select name='category' class="" aria-label="category" id='category' required>
                            <option value='flexible' selected>Flexibel</option>
                            <option value='daily'>Täglich</option>
                        </select>
                    </div>
                </div>
            </div>

        <!-- Kategorie -->
        <div class="mb-3">
            <div class="row">
                <div class="col">
                    <label for="category1" class="form-label"><b>Oberkategorie</b> </label>
                        <input list="categories" type="text" name="supercategory" class="" required/>
                        <datalist id="categories" required>
                            <?php foreach($categories as $category){ ?>
                                <option value="<?php echo $category['title'] ?>">
                            <?php } ?>
                        </datalist>
                    </div>

                <div class="col">
                    <label for="type" class="form-label"><b>Fragen-Typ</b></label>
                    <select class="" name='question_type' id="question_type" required>
                        <option>Single-Choice</option>
                        <option>Multiple-Choice</option>
                        <option>Text</option>
                    </select>
                </div>
            </div>
        </div>

        <!-- Daily: Erscheinungsdatum -->
        <div id='erscheinung-daily' style='display:none'>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                    <label for="frage_start_daily" class="form-label"><b>Erscheinungsdatum</b></label>
                        <input type="text" name="release_date_daily" id="question_start_daily" class="datetimepicker qs" placeholder="Erscheinung">
                    </div>
                </div>
            </div>
        </div>

      </fieldset>
      <fieldset class="collapsable collapsed pt-0">
          <legend>Optionale Felder - Inhalt</legend>
        <!-- Datum von bis -->
         <!-- Daily: Erscheinungsdatum -->
         <div id='erscheinung-flexible'>
            <div class="mb-3">
                <div class="row">
                    <div class="col mt-3">
                    <label for="frage_start" class="form-label"><b>Fragen-Erscheinung</b></label>
                        <input type="text" name="release_date" id="question_start" class="datetimepicker qs" placeholder="Erscheinung">
                    </div>
                    <div class="col mt-3">
                        <label for="quiz_ende" class="form-label"><b>Beantwortungszeit</b></label>
                        <input type="text" name="response_date" id="question_ende" class="datetimepicker qe" placeholder="Fristende">
                    </div>
                </div>
            </div>
        </div>
        <!-- Kategorie -->
        <div class="mb-3">
            <div class="row">
                <div class="col">
                    <label for="subcategory" class="form-label"><b>Unterkategorie</b></label>
                    <input type="text" name="subcategory" class="" placeholder="Unterkategorie">
                </div>
                <div class="col">
                    <label for="difficulty" class="form-label"><b>Schwierigkeitsgrad</b></label>
                    <select class="" name='type' id="type">
                        <option>Leicht</option>
                        <option>Mittel</option>
                        <option>Schwer</option>
                    </select>
                </div>
            </div>
            <!-- File Uploads -->
            <div class="mb-3">
                <div class="row mt-3">
                    <div class="col">
                        <label for="subcategory" class="form-label"><b>Hinweistext</b></label>
                        <textarea type="text" class="" name="hint"></textarea>
                    </div>
                    <div class="col">
                    <label for="difficulty" class="form-label"><b>Bild zur Fragestellung</b></label>
                        <input type="hidden" name="subcmd" value="upload">
                        <input class="attach p-0" type="file" name="import_file" accept=".png, .jpg, .jpeg">
                    </div>

                </div>
            </div>
      </fieldset>
      <fieldset class='pt-0'>
          <legend>Fragestellung</legend> <br>
                <div class="col-12">
                    <div class="mb-3 mt-4">
                        <textarea class="add_toolbar wysiwyg" data-textarea="new_entry" name="question_content" required tabindex="1"></textarea>
                    </div>
                </div>

                <div class='col'>
                    <div class="text-answer" style='display:none'>
                        <div class="mb-3">
                        <label for="text-answer" class="form-label mb-0"><b>Korrekte Antwort</b></label>
                            <div class='row frame'>
                                <div class='col-sm-12 col-md-12 col-lg-10'>
                                    <div id="wrapper">
                                        <div id="first" class="frame-box mx-3 p-2">
                                            <input type='hidden' name='text-boolean[]' value='1' id='bool' checked>
                                            <input class="form-check-input input-box" id="boolean" type="checkbox" checked onclick="return false;" value='1'/>
                                        </div>
                                        <div id="second">
                                            <input type="text" class="form-control mw-100" name="content[]">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="answers">
                        <div class="mb-3">
                            <b>Antwortmöglichkeiten</b>
                            <!-- Single Choice -->
                            <div id='single'>
                                <div class='row frame'>
                                    <div class='col-sm-12 col-md-12 col-lg-4'>
                                        <div id="wrapper">
                                            <div id="first" class="frame-box mx-3 p-2">
                                                <input type='hidden' name='boolean[]' value='0' id='bool'>
                                                <input class="form-check-input input-box" id="boolean" type="checkbox" onclick="$(this).parent().find('#bool').attr('value', this.checked ? '1' : '0')">
                                            </div>
                                            <div id="second">
                                                <input type="text" class="form-control" name="content[]" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-12 col-md-12 col-lg-4'>
                                        <input type="hidden" name="subcmd" value="upload">
                                        <input class="attach p-0" type="file" name="answer_file[]" accept=".png,.jpg,.jpeg">
                                    </div> 
                                </div>
                            </div>

                            <div id='single'>
                                <div class='row frame'>
                                    <div class='col-sm-12 col-md-12 col-lg-4'>
                                        <div id="wrapper">
                                            <div id="first" class="frame-box mx-3 p-2">
                                                <input type='hidden' name='boolean[]' value='0' id='bool'>
                                                <input class="form-check-input input-box" id="boolean" type="checkbox" onclick="$(this).parent().find('#bool').attr('value', this.checked ? '1' : '0')">
                                            </div>
                                            <div id="second">
                                                <input type="text" class="" name="content[]" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-12 col-md-12 col-lg-4'>
                                        <input type="hidden" name="subcmd" value="upload">
                                        <input class="attach p-0" type="file" name="answer_file[]" accept=".png,.jpg,.jpeg">
                                    </div> 
                                    <div class="col-sm-1">
                                        <button type='button' class='btn button addbtn' id='addbtn'><span aria-hidden='true'><i class='far fa-plus-square'></i></span></button>       
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="messagebox messagebox_error m-2 error-show hidden">
                            <div class="messagebox_buttons">
                                <a class="close" href="#" title="Nachrichtenbox schließen">
                                    <span>Nachrichtenbox schließen</span>
                                </a>
                            </div>
                            <span class='error-result'> </span>
                        </div>
                    </div>
                </div>
        </fieldset>
      <button type="submit" id='create_question' name="add_question" class="button">Frage erstellen</button>
    </form>

<?= $this->s_cats->render() ?>
<?= $this->s_views->render() ?>

<script>$('.datetimepicker').datetimepicker();</script>

