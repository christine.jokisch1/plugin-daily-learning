<form class="default" id="create" autocomplete="off" name="add" method="post" action="" enctype="multipart/form-data">
 <?= CSRFProtection::tokenTag() ?>
    <fieldset class='pt-0'>
        <legend class="mb-3">Plugin Einstellungen</legend>
            <div class="row">
                <div class="col m-auto">
                    <label for="pluginname" class="form-label m-auto">Plugin-Name (Max. 20 Zeichen):</label>
                </div>
                <div class="col-sm-10 m-auto">
                    <input type="text" name="pluginname" class="form-control m-2 mw-100" value="<?php echo $config['plugin_name'] != '' ? $config['plugin_name'] : '' ?>" placeholder="<?php echo $config['plugin_name'] != '' ? $config['plugin_name'] : 'FlexQuiz' ?>">
                </div>
            </div>
            <div class="row">
                <div class="col m-auto">
                    <label for="pluginname" class="form-label m-auto">Hinweis Anzeige:<br> 
                    <small>Anzahl an Versuchen bis der Hinweistext angezeigt werden soll.</small> 
                </label>
                </div>
                <div class="col-sm-10 m-auto">
                    <input type="text" name="hint_questions" class="form-control m-2 mw-100" 
                    value="<?php echo $config['hint_questions'] != '' ? $config['hint_questions'] : '2' ?>" 
                    placeholder="<?php echo $config['hint_questions'] != '' ? $config['hint_questions'] : '2' ?>">
                </div>
            </div>
            </fieldset>
            <fieldset class='pt-0'>
                <legend class="mb-3">Dozierenden</legend>

                <div class="row">
                    <div class="col m-auto">
                        <label for="pluginname" class="form-label m-auto">Einleitung (Dozierenden):</label>
                    </div>
                    <div class="col-sm-10 m-auto">
                        <textarea class="form-control m-2 mw-100 add_toolbar wysiwyg" data-textarea="new_entry" name="welcome_dozent" required tabindex="1">
                        <?php
                            if($config['welcome_dozent'] != ''){
                            echo $config['welcome_dozent']; 
                            
                            } else {
                                echo ' 
                                <h3>Administration von FlexQuiz</h3><br>
                                Mithilfe von FlexQuiz können Sie flexible <b>Wissensfragen</b> erstellen.<br>
                                Dabei können sowohl mehrere Fragen innerhalb eines Zeitraumes beantwortet werden oder als tägliche Wissensabfrage fungieren. <br>
                            <ul>
                                <li>Fragen mit der Kategorie <i>flexibel</i> sind beliebig wiederholbar. <br></li>
                                <li>Tägliche Fragen können nur einmal an einem festgelegten Tag beantwortet werden.</li>
                            </ul>
                            '; 
                            }
                        ?>  
                        </textarea>
                    </div>
                </div>
            </fieldset>
            <fieldset class='pt-0'>
            <legend class="mb-3">Studierenden</legend>
                <div class="row">
                    <div class="col m-auto">
                        <label for="pluginname" class="form-label m-auto">Einleitung (Studierenden):</label>
                    </div>
                    <div class="col-sm-10 m-auto">
                        <textarea class="form-control m-2 mw-100 add_toolbar wysiwyg" data-textarea="new_entry" name="welcome_student" required tabindex="1">
                            <?php
                                if($config['welcome_student'] != ''){
                                    echo $config['welcome_student']; 
                                } else {
                                    echo ' 
                                    <h3>FlexQuiz</h3><br>
                                        In diesem Lernmodul werden Fragen spezifisch zur Lehrveranstaltung gestellt.<br>
                                        Die Fragen werden zwischen flexiblen und täglichen Fragen unterschieden.
                                    <ul>
                                        <li>Fragen mit der Kategorie <i>flexibel</i> sind beliebig wiederholbar. <br></li>
                                        <li>Tägliche Fragen können nur einmal an einem festgelegten Tag beantwortet werden.</li>
                                    </ul>
                                '; 
                                }
                            ?>  
                        </textarea>
                    </div>
                    
                </div>
            </fieldset>
    <button type="submit" id='create_question' name="configurations" class="button" >Frage erstellen</button>
</form>
 
<?= $this->s_cats->render() ?>
<?= $this->s_views->render() ?>
