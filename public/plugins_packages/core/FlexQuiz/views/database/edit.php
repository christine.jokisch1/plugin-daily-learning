
<form class="default" id="create" autocomplete="off" name="add" method="post" enctype="multipart/form-data">
 <?= CSRFProtection::tokenTag() ?>
        <? foreach ($question as $question) : ?>
            <fieldset class='pt-0'>
        <legend>Pflichtfelder</legend>
            <div class="my-3">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="title" class="form-label"><b>Name</b></label>
                        <input type="text" name="title" class="" value="<?php echo $question['qtitle']; ?>" required>
                    </div>
                    <div class="col-sm-6">
                        <label for="Fragenkategorie" class="form-label"><b>Fragenkategorie</b></label>
                        <select name='category' class="" aria-label="category" id='category' required>
                            <option value='flexible' <?php echo ($question['category'] === 'flexible' ? 'selected' : '') ?>>Flexibel</option>
                            <option value='daily' <?php echo ($question['category'] === 'daily' ? 'selected' : '') ?>>Täglich</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <div class="row">
                    <div class="col">
                    <label for="category1" class="form-label"><b>Oberkategorie</b></label>
                        <input list="categories" type="text" name="supercategory" class="" value="<?php echo $question['stitle']; ?>" required>
                    </div>
                    <datalist id="categories" required>
                        <?php foreach($categories as $category){ ?>
                            <option value="<?php echo $category['title'] ?>">
                        <?php } ?>
                    </datalist>
                    <div class="col">
                    <label for="type" class="form-label"><b>Fragentyp</b></label>
                        <select class="" name='question_type' id="question_type" required>
                            <option value='Single-Choice' <?php echo ($question['question_type'] === 'Single-Choice' ? 'selected' : '') ?>>Single-Choice</option>
                            <option value='Multiple-Choice' <?php echo ($question['question_type'] === 'Multiple-Choice' ? 'selected' : '') ?>>Multiple-Choice</option>
                            <option value='Text' <?php echo ($question['question_type'] === 'Text' ? 'selected' : '') ?>>Text</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                    <label for="frage_start" class="form-label"><b>Fragen-Erscheinung</b></label>
                        <input type="text" name="release_date" id="question_start"
                            class="<?php echo ( date("Y.m.d H:i", strtotime($question["release_date"])) < date("Y.m.d H:i")  ? '' : 'datetimepicker') ?> qs"
                            value="<?php echo date("d.m.Y, H:i", strtotime($question['release_date'])); ?>"
                            <?php echo ( date("Y.m.d H:i", strtotime($question["release_date"])) < date("Y.m.d H:i")  ? 'readonly' : '') ?>
                            >
                    </div>

                    <div class="col">
                        <label for="quiz_ende" class="form-label"><b>Beantwortungszeit</b></label>
                        <input type="text" name="response_date" id="question_ende" class="<?php echo ( date("Y.m.d H:i") > date("Y.m.d H:i", strtotime($question["response_date"]))  ? '' : 'datetimepicker') ?> qe"
                        <?php echo ( date("Y.m.d H:i") > date("Y.m.d H:i", strtotime($question["response_date"]))  ? 'readonly' : '') ?>
                        value="<?php echo date("d.m.Y, H:i", strtotime($question['response_date'])); ?>">
                    </div>
                </div>
            </div>
        </fieldset>
    <? endforeach ?>
    <fieldset class='pt-0'>
      <legend>Optionale Felder</legend> <br>

        <div class="my-3">
                <div class="row">
                    <div class="col">
                        <label for="subcategory" class="form-label"><b>Unterkategorie</b></label>
                        <input type="text" name="subcategory" class="" value="<?php echo $question['subcategory']; ?>">
                    </div>
                    <div class="col">
                        <label for="difficulty" class="form-label"><b>Schwierigkeitsgrad</b></label>
                        <select class="" name='type' id="type">
                            <option <?php echo ($question['type'] === 'Leicht' ? 'selected' : '') ?>>Leicht</option>
                            <option <?php echo ($question['type'] === 'Mittel' ? 'selected' : '') ?>>Mittel</option>
                            <option <?php echo ($question['type'] === 'Schwer' ? 'selected' : '') ?>>Schwer</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                        <label for="hinweis" class="form-label"><b>Hinweistext</b></label>
                        <textarea class="" name="hint"><?php echo $question['hint']; ?></textarea>
                    </div>
                    <div class="col">
                    <label for="difficulty" class="form-label"><b>Bild zur Fragestellung</b></label>
                        <input type="hidden" name="subcmd" value="upload">
                        <input class="attach p-0" type="file" name="import_file" accept=".png">

                        <?php if($question['file']) { ?>
                            <div id='img-view' class="card-header border-0 py-3 justify-content-between align-items-center mt-1">

                                <span style='float:left' class="card-title mb-0 d-inline align-middle">
                                    <?php echo $question['file']; ?>
                                </span>

                                <span style='float:left' class="mx-3 badge rounded-pill b_flexible pointer" data-id="<?php echo $question['question_id']; ?>" id="remove">Entfernen</span>

                                <div class='show-img' id="show">
                                    <span src="<?php echo $controller->getQuestionLink($question['file'], $cid); ?>" alt="Bild zur Fragestellung: <?php echo $question['title'] ?>" class="mr-3 badge rounded-pill b_flexible pop">
                                        Öffnen
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
    </fieldset>

    <fieldset class='pt-0'>
        <legend>Fragestellung</legend> <br>

            <div class="col-12">
                <div class="mb-3 mt-4">
                    <textarea class="form-control add_toolbar wysiwyg" data-textarea="new_entry" name="question_content" required tabindex="1">
                        <?php echo $question['content']; ?>
                    </textarea>
                </div>
            </div>

            <div class="mb-3">
                <b>Antwortmöglichkeiten</b>
            </div>

            <?php foreach($answers as $key => $answer){ ?>
                <div id='single'>

                    <div class='row frame'>
                        <div class='col-sm-12 col-md-12 col-lg-4'>
                            <div id="wrapper">
                                <div id="first" class="frame-box mx-3 p-2">
                                    <input type='hidden' name='boolean[]' value='<?php echo ($answer['is_right'] === '1' ? '1' : '0') ?>' id='bool'>
                                    <input <?php echo ($answer['is_right'] === '1' ? 'checked' : '') ?> class="form-check-input input-box" id="boolean" type="checkbox" onclick="$(this).parent().find('#bool').attr('value', this.checked ? '1' : '0')">
                                </div>
                                <div id="second">
                                    <input type="text" class="option" name="content[]" data-id="<?php echo $answer['answer_id']; ?>" value="<?php echo $answer['option_content']?>">
                                </div>
                            </div>
                        </div>

                        <div class='col-sm-12 col-md-12 col-lg-4'>
                            <input type="hidden" name="subcmd" value="upload">
                            <input class="attach p-0" type="file" name="answer_file[]" data-id="<?php echo $answer['answer_id']; ?>" accept=".png,.jpg,.jpeg">
                        </div>
                        <?php if($answer['file']) { ?>
                        <div class='col-sm-12 col-md-12 col-lg-3'>
                            <div id='img-view' class="p-3 border-0 py-3 justify-content-between align-items-center ml-2">
                             
                                <span style='float:left' class="removeOptionIMG mx-3 badge rounded-pill b_flexible pointer" data-id="<?php echo $answer['answer_id']; ?>">
                                    <i class="fas fa-image"></i> Entfernen
                                </span>
                                <div class='show-img' id="show" style='width:200px'>
                                    <span src="<?php echo $controller->getAnswerLink($answer['file'], $cid); ?>" alt="" class="mr-3 badge rounded-pill b_flexible pop">
                                        <i class="fas fa-image"></i> Öffnen 
                                    </span>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-sm-1">
                            <?php if($key == 1 && $question['question_type'] == 'Single-Choice'){ ?>
                                <button type='button' class='btn button addbtn' id='addbtn'><span aria-hidden='true'><i class='far fa-plus-square'></i></span></button>
                            <?php }else if($key == 2 && $question['question_type'] == 'Multiple-Choice') {  ?>
                                <button type='button' class='btn button addbtn' id='addbtn'><span aria-hidden='true'><i class='far fa-plus-square'></i></span></button>
                            <?php } ?>
                             <?php if($key == 2 && $question['question_type'] == 'Single-Choice'){ ?>
                                <button type='button' class='btn button remove'><span aria-hidden='true'><i class='far fa-trash-alt'></i></span></button>
                            <?php } else if($key == 3 && $question['question_type'] == 'Multiple-Choice') { ?>
                                <button type='button' class='btn button remove'><span aria-hidden='true'><i class='far fa-trash-alt'></i></span></button>
                                <button type='button' class='btn button addbtn' id='addbtn'><span aria-hidden='true'><i class='far fa-plus-square'></i></span></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
    </fieldset>
    <button type="submit" id='edit_question' name="edit_question" class="button">Frage erstellen</button>
</form>

<!-- Modal -->
<div id="myModal" class="modal">
    <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>


<script>$('.datetimepicker').datetimepicker();</script>
<script>
    $(document).on("click", '.show-img', function(event) {
        var modal = document.getElementById("myModal");
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        modal.style.display = "block";

        var new_img = $(this).find('.pop').attr('src');
        var new_caption = $(this).find('.pop').attr('alt');
        modalImg.src = new_img;
        captionText.innerHTML = new_caption;
    });

    $('.attach').change(function(ev) {
        let id = $(this).data("id");
        let searchParams = new URLSearchParams(window.location.search);
        let cid = searchParams.get('cid');
        let qid = searchParams.get('qid');
        let cat = searchParams.get('cat');
        let formData = new FormData();

        var element = $(this);
        var file = this.files[0]
        formData.append("file", file);
        formData.append("action", 'editAnswerUpload');
        formData.append("id", id);
        formData.append("cid", cid);
        formData.append("qid", qid);

        $.ajax({
            type: 'POST',
            url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/edit",
            data: formData,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                element.parent('div').next().html(`
                <div id='img-view' class="card-header border-0 py-3 justify-content-between align-items-center">
                    Image hochgeladen!
                </div>
                `);
            }
        });

        // your code
    });

    $('.option').on('keyup', function() {
        let id = $(this).data("id");
        let searchParams = new URLSearchParams(window.location.search);
        let cid = searchParams.get('cid');
        let value = $(this).val();

        $.ajax({
            type: 'GET',
            url: STUDIP.ABSOLUTE_URI_STUDIP + "plugins.php/flexquiz/database/edit",
            dataType: "html",
            data: {id: id, action:'editAnswer', cid:cid, value:value},
            success: function(response) {

            }
        });
    });

</script>
<?= $this->s_cats->render() ?>
<?= $this->s_views->render() ?>
