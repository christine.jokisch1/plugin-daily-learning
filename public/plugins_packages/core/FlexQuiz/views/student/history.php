<div class="m-4">
  <div class='row' style='width:99%; margin-left:0px !important; margin-right:0px !important'>
      <div class='col-sm-12'>

    <? if($hcount == 0){ ?>
     
        <div class='box-info'>
        <i class="icon icon-info fas fa-question"></i>
            <div class='my-3'>
                Es wurden bisher <b>keine</b> Quiz-Fragen beantwortet.
            </div>
        </div>
    <? } else { ?>
      <div class='box-info mb-4'>
      <i class="icon icon-info fas fa-question"></i>
        <h3 class='mb-2 p-0'>
           History
        </h3>

        Es wurden bereits <b><?php echo $hcount ?> Quiz-Fragen</b> beantwortet. <br>
        Sie können Ihre Quiz unter <i>Betrachten</i> erneut einsehen.
    </div>

      <table class="default sortable-table" data-sortlist="<?php echo $sortres ?>">
          <thead>
          <tr class="sortable">
              <th data-sort="text">Beantwortet am</th>
              <th data-sort="text">Titel</th>
              <th data-sort="text">Schwierigkeit</th>
              <th class="sortInitialOrder-asc" data-sort="text">Ergebnis</th>
              <th></th>
          </tr>
          </thead>
          <tbody>

          <?php foreach($history as $key => $history){ ?>
              <tr>
                  <td> <?php echo date("d.m.Y, H:i", strtotime($history['response_date'])); ?> Uhr </td>
                  <td> <?php echo $history['title'] ?> </td>
                  <td> <?php echo $history['type'] ?> </td>
                  <td> <?php echo ($history['result'] == '1' ? 'Richtig' : 'Falsch' ) ?> </td>
                  <td>
                      <a class='btn button m-0' href='<?= PluginEngine::getLink('flexquiz/student/details', array('quiz' => $history['quiz_id'])) ?>'>
                          Betrachten
                      </a>
                  </td>
              </tr>
          <?php } ?>
          <tfoot></tfoot>
      </table>
      <? } ?>

      <?php 
      
        if($page>=2){   
          echo '<a class="pageto" id="paging" href="'.URLHelper::getLink('', array('page' => $page-1)).'"> Zurück </a>';   
        }       
      
        for ($i=1; $i<=$total_pages; $i++) {   
          if ($i == $page) {   
              $pagLink .= '<a class="pageto" id="paging" href="'.URLHelper::getLink('', array('page' => $i)).'">'.$i.'</a>';   
          }               
          else  {  
            $pagLink .= '<a class="pageto" id="paging" href="'.URLHelper::getLink('', array('page' => $i)).'">'.$i.'</a>';  

          }   
        };     
        echo $pagLink;   

        if($page<$total_pages){   
            echo '<a class="pageto" id="paging" href="'.URLHelper::getLink('', array('page' => $page+1)).'"> Nächste </a>';  
        }   

      ?>  

    </div>
  </div>
</div>

<?= $this->sidebar ?>

<script>
$(document).on("click", '.tablesorter-header', function(event) {
  var url = document.location.href;
  var urlParams = new URLSearchParams(window.location.search); //get all parameters
  var sortstring = urlParams.get('sort'); //extract the foo parameter - this will return NULL if foo isn't a parameter

  if($('.tablesorter-active-class').hasClass('tablesorter-active-class') || sortstring){
   
      $('.tablesorter-active-class').removeClass('tablesorter-active-class'); 
      var sort = $(this).text(); 
      $(this).addClass('tablesorter-active-class');

      if(sortstring) { 
        var queryParams = new URLSearchParams(window.location.search);
        queryParams.set("sort", sort);
        history.replaceState(null, null, "?"+queryParams.toString());
        
      }else{
        var newUrl = document.location.href+"&sort="+sort;
      }
  } else {
    var sort = $(this).text(); 
    $(this).addClass('tablesorter-active-class'); 
    var url = document.location.href+"&sort="+sort;
    window.location.href=url;
  }
}); 

$(document).on("click", '#paging', function(e) {
   e.preventDefault();
  var url = new URL(window.location.href); 
  var urlParams = new URLSearchParams(window.location.search); //get all parameters
  var urlParam = decodeURIComponent(urlParams);
  var hrefParams = $(this).attr('href'); 
  var sortstring = urlParams.get('sort');  

  if(sortstring) { //check if foo parameter is set to anything
    if(urlParams.get('page')){
      window.location.href = $(this).attr('href')+"&sort="+sortstring; 
    } else {
      window.location.href = $(this).attr('href')+"&sort="+sortstring; 
    }

  }else{
    window.location.href = $(this).attr('href');
  }

}); 

 </script>
