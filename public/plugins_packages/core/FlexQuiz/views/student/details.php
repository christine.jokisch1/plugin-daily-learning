
<div class='row justify-content-center'>
    <div class='col-sm-10'>
        <h2 class='mb-3 mt-4'>Quiz: <?php echo $question['title'] ?></h2>
        <div class='flexquiz-quiz-question'>
            <div class="card-title question-head">
                <div class='question-title'>Fragestellung</div>
            </div>
            <div class='flexquiz-quiz-body'>
                <?php if(!empty($question['file'])){ ?>
                    <a href="#" class="pop">
                        <!-- Trigger the Modal -->
                        <img class='question-preview' id="myImg" src="<?php echo $controller->getQuestionLink($question['file'], $cid); ?>" alt="Bild zur Fragestellung: <?php echo $question['title'] ?>" width="250">
                    </a>
                <?php } ?>
                <?php echo $question['content'] ?>
            </div>
        </div>
            <?php foreach($options as $option){ ?>
                <div class='flexquiz-quiz-answers 
                <?php echo ($option['answered_right'] == '1' && $option['is_right'] == '1') ? 'ans-green' : ''; ?>
                <?php echo ($option['is_right'] == '0' && $option['answered_right'] == '1' ) ? 'ans-red' : ''; ?>
                <?php echo ($option['is_right'] == '1' && $option['answered_right'] == '0' ) ? 'ans-green' : ''; ?>
                '>
                    <div class='row'>
                        <div class='col-sm-10'>
                            <? if($option['file']){ ?>
                                <div class="img-box-options">
                                    <div class='show-img'>
                                        <a href="#" class="pop" alt="<?php echo $option['option_content'] ?>" src='<?php echo $controller->getOptionLink( $option['file'], $cid); ?>'>
                                            <i class="far fa-file-image"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="img-box-options box-none"></div>
                            <?php } ?>
                            <label style='display:block' class='mb-0'>
                                <input type='hidden' name='answers[]' value='<?php echo $option['answer_id']; ?>'>
                                <input type='hidden' name='boolean[]' value='' id='bool'>
                                <input <?php echo ($option['answered_right'] == '1') ? 'disabled="disabled" checked' : 'disabled="disabled"'; ?>
                                    class="form-check-input mt-0 option mr-4 mb-0" name='answer_option' id="boolean-option" type="checkbox" />
                                <?php echo $option['option_content'] ?>
                            </label>
                        </div>
                    </div>
                </div>
            <?php } ?>

        <?php if($quiz['result'] == '1') { ?>
            <div class="quiz-success">
                Die Frage haben Sie richtig beantwortet.
            </div>
        <?php } else { ?>
            <div class="quiz-failure">
            Die Frage haben Sie falsch beantwortet.
            </div>
        <?php } ?>

        <div class='question-bottom'>
            <a id="show-hint" class="button quiz-button-bottom m-2">
                Hinweis anzeigen
            </a>
        </div>

            <div id="hint" style="display:none;">
                <div class='row'>
                    <div class='col-sm-12'>
                        <div class='hint-inner'>
                            <i class="far fa-lightbulb font-color-blue"></i> <b>Hinweistext zur Lösung</b><br>
                            <hr>
                            <?php echo $question['hint']; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


<!-- Modal -->
<div id="myModal" class="modal">
    <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>

<?= $this->sidebar ?>

<script>
$(document).on("click", '#show-hint', function(event) {
    if ($('#hint').is(':visible')){
        $('#hint').slideUp();
        $(this).text('Hinweistext einblenden');
    }else {
        $('#hint').slideDown();
        $(this).text('Hinweistext ausblenden');
    }
});
</script>
